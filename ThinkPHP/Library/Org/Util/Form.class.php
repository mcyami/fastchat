<?php
/**
 * 
 * Form.php (模型表单生成)
 *
 */
namespace Org\Util;
class Form {
	public $data = array (), $isadmin = 1, $doThumb = 1, $doAttach = 1, $sid, $sysConfig;
	
	public function __construct($data = array()) {
		$this->data = $data;
		$this->sid = get_current_siteid ();
		$this->sites = F('Sitelist');
		$this->siteData = $this->sites[$this->sid];
		$this->sysConfig = string2array($this->siteData['config']);
	}
	
	/**
	 * 是否链接转向
	 * @param array $info 
	 * @param string $value
	 */
	public function islink($info, $value) {
		$validate = getvalidate ( $info );
		$id = $field = $info ['field'];
		if (ACTION_NAME == 'add') {
			$value = $value ? $value : $info ['setup'] ['default'];
		} else {
			$value = $value ? $value : $this->data [$field];
		}
		
		$info ['field'] = 'islink';
		$info ['setup'] ['labelwidth'] = 40;
		$info ['setup'] ['onclick'] = 'enablelink(this.value)';
		$info ['options'] = array (0 => L ( 'no' ), 1 => L ( 'yes' ) );
		$radiostr = $this->radio ( $info, $value ) . '<span id="append"><span>';
		if ($value) {
			$url = $this->data ['url'];
			return $radiostr . '&nbsp;<input type="text" class="put ' . $info ['class'] . '" placeholder="请填写正确的URL地址" name="linkurl" id="linkurl" value="' . $url . '"  ' . $validate . '/>';
		} else {
			return $radiostr;
		}
	}
	/**
	 * url  规则调用
	 * @param array $info 
	 * @param string $value
	 */
	public static function urlrule($info, $value) {
		$module = $info ['module'];
		$ishtml = $info ['ishtml'];
		$file = $info ['file'];
		$field = $info ['field'];
		if (ACTION_NAME == 'add') {
			$value = $value ? $value : $info ['setup'] ['default'];
		} else {
			$value = $value ? $value : $this->data [$field];
		}
		if (! $module){
			$module = 'Content';
		}
		$urlrules = F ( "Urlrule" );
		foreach ( $urlrules as $k => $rules ) {
			if ($rules ['module'] == $module && $rules ['file'] == $file && $rules ['ishtml'] == $ishtml){
				$array [$rules ['id']] = $rules ['example'];
			}
		}
		$info ['options'] = $array;
		return self::select ( $info, $value );
	}
	
	/**
	 * 来源选择
	 * @access public
	 * @param array $info 
	 * @param string $value
	 */
	public function copyfrom($info, $value) {
		$validate = getvalidate ( $info );
		$id = $field = $info ['field'];
		// 取值
		$value = $value ? $value : $this->data[$field];
		$copyarr = explode('|', $value);
		$copyfromid = $copyarr[1];
		$copyfromlink = $copyarr[0];
		$str = '<input type="text" class="put ' . $info ['class'] . '" name="' . $field . '" id="' . $id . '" value="' . stripcslashes ( $copyfromlink ) . '"  ' . $validate . '/>';
		/* 所有来源列表 */
		$data = D ( 'Copyfrom' )->order ( 'listorder desc' )->select ();
		$valueData = D ( 'Copyfrom' )->where('id ='.$copyfromid)->find ();
		$valueName = $valueData['sitename'];
		$copyfrom [''] = "≡请选择≡";
		foreach ( $data as $v ) {
			$copyfrom [$v ['id']] = $v ['sitename'];
		}
		$info ['options'] = $copyfrom;
		$info ['option_key'] = 'siteurl, sitename';
		$info ['field'] = $field.'_data';
		$selectstr = $this->select ( $info, $copyfromid );
		if (empty ( $data )){
			$selectstr = '';
		}
		
		return $str.'&nbsp;&nbsp;'.$selectstr;
	}
	/**
	 * 阅读收费
	 * @param array $info 
	 * @param string $value
	 */
	public function readpoint($info, $value) {
		$validate = getvalidate ( $info );
		$id = $field = $info ['field'];
		$value = $value ? $value : $this->data [$field];
		$str = '<input type="text"   class="put2 ' . $info ['class'] . '" name="' . $field . '"  id="' . $id . '" value="' . stripcslashes ( $value ) . '"  ' . $validate . '/>';
		$data ['options'] = L ( 'readpoints' );
		$data ['field'] = 'paytype';
		$radiostr = $this->radio ( $data, $value );
		$str = $str . '&nbsp;' . $radiostr;
		return $str;
	}
	/**
	 * 栏目选择
	 * @param array $info
	 * @param string $value
	 */
	public function catid($info, $value) {
		$validate = getvalidate ( $info );
		$Category = F ( 'Category_' . get_current_sid () );
		$id = $field = $info ['field'];
		$value = $value ? $value : $this->data [$field];
		$modelid = $info ['modelid'];
		foreach ( $Category as $r ) {
			//			$postgroup = explode ( ',', $r ['postgroup'] );
			//			if (($this->isadmin && $_SESSION ['roleid'] != 1 && ! in_array ( $_SESSION ['roleid'], $postgroup )) || (empty ( $this->isadmin ) && ! in_array ( cookie ( 'roleid' ), $postgroup )))
			//				continue;
			//			
			if ($r ['type'] == 1 && $r ['type'] == 2){
				continue;
			}
				
			
		//			$arr = explode ( ",", $r ['arrchildid'] );
			//			$show = 0;
			//			foreach ( ( array ) $arr as $rr ) {
			//				if ($Category [$rr] ['modelid'] == $modelid)
			//					$show = 1;
			//			}
			//			if (empty ( $show ))
			//				continue;
			$r ['disabled'] = $r ['child'] ? ' disabled' : '';
			$array [] = $r;
		}
		$str = "<option value='\$id' \$disabled \$selected>\$spacer \$catname</option>";
		$tree = new Tree ( $array );
		$parseStr .= '<select  id="' . $id . '" name="' . $field . '" class=" ' . $info ['class'] . '"  ' . $validate . '>';
		if (! $info ['chose']) {
			$parseStr .= '<option value="">' . L ( 'please_chose' ) . '</option>';
		}
		$parseStr .= $tree->get_tree ( 0, $str, $value );
		$parseStr .= '</select>';
		return $parseStr;
	}
	/**
	 * 栏目选择
	 * @param array $info
	 * @param string $value
	 */
	public function cat($info, $value) {
		$validate = getvalidate ( $info );
		
		$Category = F ( 'Cat_content_' . get_current_sid () . '_' . LANG_NAME );
		
		$id = $field = $info ['field'];
		$value = $value ? $value : $this->data [$field];
		$modelid = $info ['modelid'];
		foreach ( $Category as $r ) {
			if ($r ['type'] == 1)
				continue;
			$array [] = $r;
		}
		import ( 'Tree' );
		$str = "<option value='\$id' \$selected>\$spacer \$catname</option>";
		$tree = new Tree ( $array );
		$parseStr .= '<select  id="' . $id . '" name="' . $field . '" class=" ' . $info ['class'] . '"  ' . $validate . '>';
		if (! $info ['chose']) {
			$parseStr .= '<option value="">' . L ( 'please_chose' ) . '</option>';
		}
		$parseStr .= $tree->get_tree ( 0, $str, $value );
		$parseStr .= '</select>';
		return $parseStr;
	}
	/**
	 * 内容标题（文本框）内置标题样式设置
	 * @param array $info
	 * @param string $value
	 */
	public function title($info, $value) {
		$info ['setup'] = is_array ( $info ['setup'] ) ? $info ['setup'] : string2array ( $info ['setup'] );
		$thumb = $info ['setup'] ['thumb'];
		$style = $info ['setup'] ['style'];
		$id = $field = $info ['field'];
		$validate = getvalidate ( $info );
		$value = $value ? $value : $this->data [$field];
		
		$title_style = explode ( ';', $this->data ['title_style'] );
		$style_color = explode ( ':', $title_style [0] );
		$style_color = $style_color [1];
		$style_bold = explode ( ':', $title_style [1] );
		$style_bold = $style_bold [1];
		
		if (empty ( $info ['setup'] ['upload_maxsize'] )) {
			$info ['setup'] ['upload_maxsize'] = intval ( byte_format ( $this->sysConfig ['attach_maxsize'] ) );
		}
		$windcms_auth_key = sys_md5 ( C ( 'ENCRYPT' ) . $_SERVER ['HTTP_USER_AGENT'] );
		$windcms_auth = auth_code ( $this->isadmin . '-1-1-1-jpeg,jpg,png,gif-' . $info ['setup'] ['upload_maxsize'] . '-' . $info ['modelid'], 'ENCODE', $windcms_auth_key );
		
		$thumb_ico = $this->data ['thumb'] ? $this->data ['thumb'] : '/Public/Img/admin_upload_thumb.png';
		$boldchecked = $style_bold == 'bold' ? 'checked' : '';
		$thumbstr = '<div class="thumb_box"  id="thumb_box"><div id="thumb_aid_box"></div>
				<a href="javascript:swfupload(\'thumb_uploadfile\',\'thumb\',\'' . L ( 'uploadfiles' ) . '\',' . $this->isadmin . ',1,1,1,\'jpeg,jpg,png,gif\',' . $info ['setup'] ['upload_maxsize'] . ',' . $info ['modelid'] . ',\'' . $windcms_auth . '\',yesdo,nodo)"><img src="' . $thumb_ico . '" id="thumb_pic" ></a><br /> <a href="javascript:;" onclick="javascript:clean_thumb(\'thumb\');">' . L ( 'clean_thumb' ) . '</a>
				<input type="hidden"  id="thumb" name="thumb"  value="' . $this->data ['thumb'] . '" /></div>';
		
		$parseStr = '<input type="text" style="float: left;" class="put input-title f_l" name="' . $field . '"  id="' . $id . '" value="' . $value . '" size="' . $info ['setup'] ['size'] . '"  ' . $validate . '  />&nbsp;';
		
		$stylestr = '<div id="' . $id . '_colorimg" class="colorimg" style="background-color:' . $style_color . '"><img src="/Public/Img/admin_color_arrow.gif"></div><input type="hidden" id="' . $id . '_style_color" name="style_color" value="' . $style_color . '" /><input type="checkbox" class="style_bold" id="style_bold" name="style_bold" value="bold" ' . $boldchecked . ' /><b><label for="style_bold">' . L ( 'style_bold' ) . '</label></b><script>$.showcolor("' . $id . '_colorimg","' . $id . '_style_color");</script>';
		if ($style)
			$parseStr = $parseStr . $stylestr;
		
		// if ($thumb && $this->doThumb)
		if ($thumb){
			$parseStr = $parseStr . $thumbstr;
		}
		return $parseStr;
	}
	/**
	 * 文本框
	 * @param array $info
	 * @param string $value
	 */
	public function text($info, $value) {
		$info ['setup'] = is_array ( $info ['setup'] ) ? $info ['setup'] : string2array ( $info ['setup'] );
		$id = $field = $info ['field'];
		$validate = getvalidate ( $info );
		$info ['setup'] ['ispassword'] ? $inputtext = 'password' : $inputtext = 'text';
		if (ACTION_NAME == 'add') {
			$value = $value ? $value : $info ['setup'] ['default'];
		} else {
			$value = $value ? $value : $this->data [$field];
		}
		$parseStr = '<input type="' . $inputtext . '"   class="put ' . $info ['class'] . '" name="' . $field . '"  id="' . $id . '" value="' . stripcslashes ( $value ) . '" size="' . $info ['setup'] ['size'] . '"  ' . $validate . '/> ';
		return $parseStr;
	}
	/**
	 * 关键词
	 * @param array $info
	 * @param string $value
	 */
	public function keywords($info, $value) {
		$info ['setup'] = is_array ( $info ['setup'] ) ? $info ['setup'] : string2array ( $info ['setup'] );
		$id = $field = $info ['field'];
		$validate = getvalidate ( $info );
		$value = $value ? $value : $this->data [$field];
		$parseStr = '<input title="请以逗号隔开关键词"  class="put ' . $info ['class'] . '" name="' . $field . '"  id="' . $id . '" value="' . $value . '" size="' . $info ['setup'] ['size'] . '"  ' . $validate . ' />标签:<span id="tagCheck" onclick="getAllTagsByAjax()" title="查看已有标签"></span>|<span onclick="hideTagTip()" id="hideTag" title="隐藏标签库"></span><div id="light" style="background-color: #fff;text-align:center;display:none;position: absolute;z-index: 9;border: 2px solid #104E8B;padding: 5px;width:650px;height:auto;overflow:hidden;"></div>';
		
		//$parseStr = '<div class="tag-list"><ul id="post-taglist" style="zoom:1"></ul><input type="text" name="post-tag-input" id="post-tag-input" class="ipt" /></div>';
		return $parseStr;
	}
	/**
	 * 验证码
	 * @param array $info
	 * @param string $value
	 */
	public function verify($info, $value) {
		$info ['setup'] = is_array ( $info ['setup'] ) ? $info ['setup'] : string2array ( $info ['setup'] );
		$id = $field = $info ['field'];
		$validate = getvalidate ( $info );
		$parseStr = '<input class="put ' . $info ['class'] . '" name="' . $field . '"  id="' . $id . '" value="" size="' . $info ['setup'] ['size'] . '"  ' . $validate . ' /><img src="' . URL ( 'Content-Index/verify' ) . '" onclick="javascript:resetVerifyCode();" class="checkcode" align="absmiddle"  title="点击刷新验证码" id="verifyImage"/>';
		return $parseStr;
	}
	
	public function number($info, $value) {
		$info ['setup'] = is_array ( $info ['setup'] ) ? $info ['setup'] : string2array ( $info ['setup'] );
		$id = $field = $info ['field'];
		$validate = getvalidate ( $info );
		$info ['setup'] ['ispassowrd'] ? $inputtext = 'passowrd' : $inputtext = 'text';
		if (ACTION_NAME == 'add') {
			$value = $value ? $value : $info ['setup'] ['default'];
		} else {
			$value = $value ? $value : $this->data [$field];
		}
		$parseStr = '<input type="' . $inputtext . '"   class="put ' . $info ['class'] . '" name="' . $field . '"  id="' . $id . '" value="' . $value . '" size="' . $info ['setup'] ['size'] . '"  ' . $validate . '/> ';
		return $parseStr;
	}
	
	public function textarea($info, $value) {
		$info ['setup'] = is_array ( $info ['setup'] ) ? $info ['setup'] : string2array ( $info ['setup'] );
		$id = $field = $info ['field'];
		$validate = getvalidate ( $info );
		if (ACTION_NAME == 'add') {
			$value = $value ? $value : $info ['setup'] ['default'];
		} else {
			$value = $value ? $value : $this->data [$field];
		}
		
		$parseStr = '<textarea  class="' . $info ['class'] . '" name="' . $field . '"  rows="' . $info ['setup'] ['rows'] . '" cols="' . $info ['setup'] ['cols'] . '"  id="' . $id . '"   ' . $validate . '/>' . stripcslashes ( $value ) . '</textarea>';
		return $parseStr;
	}
	
	/**
	 * 下拉选择框
	 * @param array $info
	 * @param string $value
	 */
	public function select($info, $value) {
		$info ['setup'] = is_array ( $info ['setup'] ) ? $info ['setup'] : string2array ( $info ['setup'] );
		$id = $info ['id'];
		$field = $info ['field'];
		$validate = getvalidate ( $info );
		if (ACTION_NAME == 'add') {
			$value = $this->data [$field];
		} else {
			$value = $value ? $value : $this->data [$field];
		}
		if ($value != ''){
			$value = strpos ( $value, ',' ) ? explode ( ',', $value ) : $value;
		}
		if (is_array ( $info ['options'] )) {
			if ($info ['options_key']) {
				$options_key = explode ( ',', $info ['options_key'] );
				foreach ( ( array ) $info ['options'] as $key => $res ) {
					if ($options_key [0] == 'key') {
						$optionsarr [$key] = $res [$options_key [1]];
					} else {
						$optionsarr [$res [$options_key [0]]] = $res [$options_key [1]];
					}
				}
			} else {
				$optionsarr = $info ['options'];
			}
		} else {
			$options = $info ['setup'] ['options'];
			$options = explode ( "\n", $info ['setup'] ['options'] );
			foreach ( $options as $r ) {
				$v = explode ( "|", $r );
				$k = trim ( $v [1] );
				$optionsarr [$k] = $v [0];
			}
		}
		
		if (! empty ( $info ['setup'] ['multiple'] )) {
			$parseStr = '<select id="' . $id . '" name="' . $field . '"  onchange="' . $info ['setup'] ['onchange'] . '" class="' . $info ['class'] . '"  ' . $validate . ' size="' . $info ['setup'] ['size'] . '" multiple="multiple" >';
		} else {
			$parseStr = '<select id="' . $id . '" name="' . $field . '" onchange="' . $info ['setup'] ['onchange'] . '"  class="' . $info ['class'] . '" ' . $validate . '>';
		}
		if (is_array ( $optionsarr )) {
			foreach ( $optionsarr as $key => $val ) {
				if (! empty ( $value )) {
					$selected = '';
					if ($value == $key || in_array ( $key, $value )){
						$selected = ' selected="selected"';
					}
					$parseStr .= '<option ' . $selected . ' value="' . $key . '">' . $val . '</option>';
				} else {
					$parseStr .= '<option value="' . $key . '">' . $val . '</option>';
				}
			}
		}
		$parseStr .= '</select>';
		return $parseStr;
	}
	
	/**
	 * 复选框
	 * @param array $info
	 * @param string $value
	 */
	public function checkbox($info, $value) {
		$info ['setup'] = is_array ( $info ['setup'] ) ? $info ['setup'] : string2array ( htmlspecialchars_decode($info ['setup']) );
		$id = $field = $info ['field'];
		$validate = getvalidate ( $info );
		if (ACTION_NAME == 'add') {
			$value = $value ? $value : $info ['setup'] ['default'];
		} else {
			$value = $value ? $value : $this->data [$field];
		}
		$labelwidth = $info ['setup'] ['labelwidth'];
		if (is_array ( $info ['options'] )) {
			if ($info ['options_key']) {
				$options_key = explode ( ',', $info ['options_key'] );
				foreach ( ( array ) $info ['options'] as $key => $res ) {
					if ($options_key [0] == 'key') {
						$optionsarr [$key] = $res [$options_key [1]];
					} else {
						$optionsarr [$res [$options_key [0]]] = $res [$options_key [1]];
					}
				}
			} else {
				$optionsarr = $info ['options'];
			}
		} else {
			$options = $info ['setup'] ['options'];
			$options = explode ( "\n", $info ['setup'] ['options'] );
			foreach ( $options as $r ) {
				$v = explode ( "|", $r );
				$k = trim ( $v [1] );
				$optionsarr [$k] = $v [0];
			}
		}
		if ($value != ''){
			$value = (strpos ( $value, ',' ) && ! is_array ( $value )) ? explode ( ',', $value ) : $value;
		}
		$value = is_array ( $value ) ? $value : array ($value );
		$i = 1;
		$onclick = $info ['setup'] ['onclick'] ? ' onclick="' . $info ['setup'] ['onclick'] . '" ' : '';
		
		foreach ( $optionsarr as $key => $r ) {
			$key = trim ( $key );
			if ($i > 1){
				$validate = '';
			}
			$checked = ($value && in_array ( $key, $value )) ? 'checked' : '';
			if ($labelwidth){
				$parseStr .= '<label style="float:left;width:' . $labelwidth . 'px" class="checkbox_' . $id . '" >';
			}
			$parseStr .= '<input type="checkbox" class="input_checkbox ' . $info ['class'] . '" name="' . $field . '[]" id="' . $id . '_' . $i . '" ' . $checked . $onclick . ' value="' . htmlspecialchars ( $key ) . '"  ' . $validate . '> ' . htmlspecialchars ( $r );
			if ($labelwidth){
				$parseStr .= '</label>';
			}
			$i ++;
		}
		return $parseStr;
	}
	/**
	 * 推荐位复选框(规则的布局)
	 * @param array $info
	 * @param string $value
	 */
	public function checkbox2($info, $value) {
		$info ['setup'] = is_array ( $info ['setup'] ) ? $info ['setup'] : string2array ($info ['setup']);
		$id = $field = $info ['field'];
		$validate = getvalidate ( $info );
		if (ACTION_NAME == 'add') {
			$value = $value ? $value : $info ['setup'] ['default'];
		} else {
			$value = $value ? $value : $this->data [$field];
		}
		$labelwidth = $info ['setup'] ['labelwidth'];
		/* 推荐位按站点取出 */
		if (is_array ( $info ['options'] )) {
			foreach ($info ['options'] as $k => $v) {
				$optionsarr [$k] = $v;
			}
		} else {
			$options = $info ['setup'] ['options'];
			$options = explode ( "\n", $info ['setup'] ['options'] );
			foreach ( $options as $r ) {
				$v = explode ( "|", $r );
				$k = trim ( $v [1] );
				$optionsarr [$k] = $v [0];
			}
		}
		if ($value != ''){
			$value = (strpos ( $value, ',' ) && ! is_array ( $value )) ? explode ( ',', $value ) : $value;
		}
		$value = is_array ( $value ) ? $value : array ($value );
		$i = 1;
		$onclick = $info ['setup'] ['onclick'] ? ' onclick="' . $info ['setup'] ['onclick'] . '" ' : '';
		/* 构建推荐位 */
		foreach ( $optionsarr as $site => $option ) {
			$parseStr .= '<table style="border:1px solid #ccc"><td style="width:70px;">'.$site.'</td><td>';
			foreach ( $option as $key => $r ) {
				$key = trim ( $key );
				if ($i > 1){
					$validate = '';
				}
				$checked = ($value && in_array ( $key, $value )) ? 'checked' : '';
				if ($labelwidth){
					$parseStr .= '<label style="float:left;width:' . $labelwidth . 'px" class="checkbox_' . $id . '" >';
				}
				$parseStr .= '<input type="checkbox" class="input_checkbox ' . $info ['class'] . '" name="' . $field . '[]" id="' . $id . '_' . $i . '" ' . $checked . $onclick . ' value="' . htmlspecialchars ( $key ) . '"  ' . $validate . '> ' . htmlspecialchars ( $r );
				if ($labelwidth){
					$parseStr .= '</label>';
				}
				$i ++;
			}
			$parseStr .= '</td></table>';
		}
		return $parseStr;
	}
	/**
	 * 单选按钮
	 * @param array $info
	 * @param string $value
	 */
	public function radio($info, $value) {
		$info ['setup'] = is_array ( $info ['setup'] ) ? $info ['setup'] : string2array ( $info ['setup'] );
		$id = $field = $info ['field'];
		$validate = getvalidate ( $info );
		if (ACTION_NAME == 'add') {
			$value = $value ? $value : $info ['setup'] ['default'];
		} else {
			$value = $value ? $value : $this->data [$field];
		}
		$labelwidth = $info ['setup'] ['labelwidth'];
		if (is_array ( $info ['options'] )) {
			if ($info ['options_key']) {
				$options_key = explode ( ',', $info ['options_key'] );
				foreach ( ( array ) $info ['options'] as $key => $res ) {
					if ($options_key [0] == 'key') {
						$optionsarr [$key] = $res [$options_key [1]];
					} else {
						$optionsarr [$res [$options_key [0]]] = $res [$options_key [1]];
					}
				}
			} else {
				$optionsarr = $info ['options'];
			}
		} else {
			$options = $info ['setup'] ['options'];
			$options = explode ( "\n", $info ['setup'] ['options'] );
			foreach ( $options as $r ) {
				$v = explode ( "|", $r );
				$k = trim ( $v [1] );
				$optionsarr [$k] = $v [0];
			}
		}
		$onclick = $info ['setup'] ['onclick'] ? ' onclick="' . $info ['setup'] ['onclick'] . '" ' : '';
		$i = 1;
		foreach ( $optionsarr as $key => $r ) {
			if ($i > 1)
				$validate = '';
			$checked = trim ( $value ) == trim ( $key ) ? 'checked' : '';
			if (empty ( $value ) && empty ( $key ))
				$checked = 'checked';
			if ($labelwidth)
				$parseStr .= '<label style="float:left;width:' . $labelwidth . 'px" class="checkbox_' . $id . '" >';
			$parseStr .= '<input type="radio" class="input_radio ' . $info ['class'] . '" name="' . $field . '" id="' . $id . '_' . $i . '" ' . $checked . $onclick . ' value="' . $key . '" ' . $validate . '> ' . $r;
			if ($labelwidth)
				$parseStr .= '</label>';
			$i ++;
		}
		return $parseStr;
	}
	/**
	 * 编辑器
	 * @param array $info
	 * @param array $value
	 */
	public function editor($info, $value) {
		$this->isadmin = 1;
		$info ['setup'] = is_array ( $info ['setup'] ) ? $info ['setup'] : string2array ( $info ['setup'] );
		$id = $field = $info ['field'];
		$validate = getvalidate ( $info );
		if (ACTION_NAME == 'add') {
			$value = $value ? $value : $info ['setup'] ['default'];
		} else {
			$value = $value ? $value : $this->data [$field];
		}
		//$value = stripslashes(htmlspecialchars_decode($value));
		$textareaid = $field;
		$toolbar = $info ['setup'] ['toolbar'];
		$modelid = $info ['modelid'];
		$height = $info ['setup'] ['height'] ? $info ['setup'] ['height'] : 300;
		$flashupload = $info ['setup'] ['flashupload'] == 1 ? 1 : '';
		$alowuploadexts = $info ['setup'] ['alowuploadexts'] ? $info ['setup'] ['alowuploadexts'] : 'jpg,jpeg,gif,png';
		$alowuploadsize = $info ['setup'] ['alowuploadsize'] ? $info ['setup'] ['alowuploadsize'] : 3;
		$alowuploadlimit = $info ['setup'] ['alowuploadlimit'] ? $info ['setup'] ['alowuploadlimit'] : 1;
		$show_page = $info ['setup'] ['showpage'];
		
		$attach_allowext = $this->sysConfig ['attach_allowext'] ? $this->sysConfig ['attach_allowext'] : $alowuploadexts;
		
		$file_size = intval ( byte_format ( $this->sysConfig ['attach_maxsize'] ) );
		$file_size = $file_size ? $file_size : $alowuploadsize;
		$windcms_auth_key = sys_md5 ( C ( 'ENCRYPT' ) . $_SERVER ['HTTP_USER_AGENT'] );
		$attach_auth = auth_code ( "$this->isadmin-1-0-$alowuploadlimit-$attach_allowext-$file_size-$modelid", 'ENCODE', $windcms_auth_key );
		$windcms_auth = auth_code ( "$this->isadmin-1-0-$alowuploadlimit-$alowuploadexts-$file_size-$modelid", 'ENCODE', $windcms_auth_key );
		
		$str = '';
		$str .= '<div class="editor_box"><div style="display:none;" id="' . $field . '_aid_box"></div><textarea name="' . $field . '" class="' . $info ['class'] . '"  id="' . $id . '"  boxid="' . $id . '" ' . $validate . '  style="width:99%;height:' . $height . 'px;visibility:hidden;">' . $value . '</textarea>';
		
		$show_page = $show_page ? 1 : 0;
		//$info['setup']['edittype']='kindeditor';
		if ($info ['setup'] ['edittype'] == 'ckeditor') {
			if ($toolbar == 'basic') {
				$toolbar = "['Bold', 'Italic', '-', 'NumberedList', 'BulletedList', '-', 'Link', 'Unlink' ]\r\n";
			} elseif ($toolbar == 'full') {
				$toolbar = "['Source',";
				$toolbar .= "'-','Templates'],
				['Cut','Copy','Paste','PasteText','PasteFromWord','-','Print'],
				['Undo','Redo','-','Find','Replace','-','SelectAll','RemoveFormat'],['ShowBlocks'],['Image','Flash'],['Maximize'],
				'/',
				['Bold','Italic','Underline','Strike','-'],
				['NumberedList','BulletedList','-','Outdent','Indent','Blockquote'],
				['JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock'],
				['Link','Unlink','Anchor'],
				['Table','HorizontalRule','Smiley','SpecialChar','PageBreak'],
				'/',
				['Styles','Format','Font','FontSize'],
				['TextColor','BGColor'],\r\n";
			} elseif ($toolbar == 'desc') {
				$toolbar = "['Bold', 'Italic', '-', 'NumberedList', 'BulletedList', '-', 'Link', 'Unlink', '-', 'Image', '-','Source'],\r\n";
			} else {
				$toolbar = '';
			}
			
			$str .= "<script type=\"text/javascript\">\r\n";
			$str .= "CKEDITOR.replace( '$textareaid',{";
			$str .= "height:{$height},isadmin:$this->isadmin,pages:$show_page,flashupload:$flashupload,textareaid:'" . $textareaid . "',iframeid:'swfupload',file_limit:$alowuploadlimit,file_types:'" . $alowuploadexts . "',modelid:$modelid,auth:'" . $windcms_auth . "',file_size:$file_size,yesdo:'insert2editor',nodo:'nodo',\r\n";
			$str .= "toolbar :\r\n";
			$str .= "[\r\n";
			$str .= $toolbar;
			$str .= "]\r\n";
			$str .= "});\r\n";
			$str .= '</script>';
			$str .= '<div class=\'editor_bottom\'>';
			if ($info ['setup'] ['show_add_description'])
				$str .= '<input type="checkbox" name="add_description" value="1" checked /> ' . L ( 'add_description' ) . ' 
				<input type="text" name="description_length" value="200" style="width:24px;" size="3" />' . L ( 'description_length' );
			if ($info ['setup'] ['show_auto_thumb'])
				$str .= '<input type="checkbox" name="auto_thumb" value="1" checked /> ' . L ( 'auto_thumb' ) . ' 
				<input type="text" name="auto_thumb_no" value="1" size="1" />' . L ( 'auto_thumb_no' );
			$str .= '</div></div>';
		} elseif ($info ['setup'] ['edittype'] == 'Xheditor') {
			
			if ($toolbar == 'basic') {
				$modtools = 'simple';
			} elseif ($toolbar == 'full') {
				$modtools = $this->isadmin ? 'full' : 'mfull';
			} elseif ($toolbar == 'desc') {
				$modtools = 'mini';
			} else {
				$modtools = '';
			}
			
			$str .= "<script type=\"text/javascript\" src=\"/Public/Xheditor/xheditor-zh-cn.min.js\"></script>";
			$str .= "<script type=\"text/javascript\"> \r\n";
			$str .= "var editor_" . $id . "=$('#" . $id . "').xheditor({ ";
			$str .= "plugins:plugins,tools:'" . $modtools . "',loadCSS:'<style>pre{margin:12px;background:#EFEFEF;border:1px solid #ddd;border-left:3px solid #6CE26C;padding:10px;padding-top: 1px;}</style>',shortcuts:{'ctrl+enter':submitForm},width:\"100%\",height:\"$height\"";
			
			if ($flashupload) {
				$str .= ",upLinkUrl:\"!/index.php?m=Admin&c=Attach&a=index&isadmin=$this->isadmin&more=1&isthumb=0&file_limit=$alowuploadlimit&file_types=$attach_allowext&file_size=$file_size&modelid=$modelid&auth=$attach_auth&editorid=$id&immediate=1\"";
				$str .= ",upImgUrl:\"!/index.php?m=Admin&c=Attach&a=index&isadmin=$this->isadmin&more=1&isthumb=0&file_limit=$alowuploadlimit&file_types=$alowuploadexts&file_size=$file_size&modelid=$modelid&auth=$windcms_auth&editorid=$id&immediate=1\"";
				
				$windcms_auth = auth_code ( "$this->isadmin-1-0-$alowuploadlimit-swf-$file_size-$modelid", 'ENCODE', $windcms_auth_key );
				$str .= ",upFlashUrl:\"!/index.php?m=Admin&c=Attach&a=index&isadmin=$this->isadmin&more=1&isthumb=0&file_limit=$alowuploadlimit&file_types=swf&file_size=$file_size&modelid=$modelid&auth=$windcms_auth&editorid=$id&immediate=1\"";
				
				$windcms_auth = auth_code ( "$this->isadmin-1-0-$alowuploadlimit-mpg,wmv,avi,wma,mp3,mid,asf,rm-$file_size-$modelid", 'ENCODE', $windcms_auth_key );
				$str .= ",upMediaUrl:\"!/index.php?m=Admin&c=Attach&a=index&isadmin=$this->isadmin&more=1&isthumb=0&file_limit=$alowuploadlimit&file_types=mpg,wmv,avi,wma,mp3,mid,asf,rm&file_size=$file_size&modelid=$modelid&auth=$windcms_auth&editorid=$id&immediate=1\"";
				
				$str .= ",onUpload:upokis,modalWidth:\"600\",modalHeight:\"455\"";
			}
			
			$str .= "});</script>";
			
			$str .= '<div class=\'editor_bottom1\'>';
			
			if ($show_page)
				$str .= '<a href="javascript:void(0);"  onclick="editor_' . $id . '.pasteText(\'[page]\');return false;">' . L ( 'page_break' ) . '</a>';
			if ($info ['setup'] ['show_add_description'])
				$str .= '<input type="checkbox" class="input_radio" name="add_description" value="1" checked /> ' . L ( 'add_description' ) . ' <input type="text"  name="description_length" value="200" style="width:24px;" size="3" />' . L ( 'description_length' );
			if ($info ['setup'] ['show_auto_thumb'])
				$str .= '<input type="checkbox" class="input_radio" name="auto_thumb" value="1" checked /> ' . L ( 'auto_thumb' ) . '<input   type="text" name="auto_thumb_no" value="1" size="1" />' . L ( 'auto_thumb_no' );
			$str .= '</div></div>';
		
		} else {
			
			$upurl = "/index.php?m=Admin&c=Attach&a=index&isadmin=$this->isadmin&more=1&isthumb=0&file_limit=$alowuploadlimit&file_types=$attach_allowext&file_size=$file_size&modelid=$modelid&auth=$attach_auth";
			
			$windcms_auth = auth_code ( "$this->isadmin-1-0-1-gif,jpg,jpeg,png,bmp-$file_size-$modelid", 'ENCODE', $windcms_auth_key );
			$upImgUrl = "/index.php?m=Admin&c=Attach&a=index&isadmin=$this->isadmin&more=1&isthumb=0&file_limit=1&file_types=gif,jpg,jpeg,png,bmp&file_size=$file_size&modelid=$modelid&auth=$windcms_auth";
			
			$windcms_auth = auth_code ( "$this->isadmin-1-0-1-swf,flv-$file_size-$modelid", 'ENCODE', $windcms_auth_key );
			$upFlashUrl = "/index.php?m=Admin&c=Attach&a=index&isadmin=$this->isadmin&more=1&isthumb=0&file_limit=1&file_types=swf,flv&file_size=$file_size&modelid=$modelid&auth=$windcms_auth";
			
			$windcms_auth = auth_code ( "$this->isadmin-1-0-1-mpg,wmv,avi,wma,mp3,mid,asf,rm,rmvb,wav,wma,mp4-$file_size-$modelid", 'ENCODE', $windcms_auth_key );
			$upMediaUrl = "/index.php?m=Admin&c=Attach&a=index&isadmin=$this->isadmin&more=1&isthumb=0&file_limit=1&file_types=mpg,wmv,avi,wma,mp3,mid,asf,rm,rmvb,wav,wma,mp4&file_size=$file_size&modelid=$modelid&auth=$windcms_auth";
			
			$str .= "<script type=\"text/javascript\" src=\"" . PUBLIC_PATH . "Kindeditor/kindeditor-min.js\"></script>";
			$str .= "<script type=\"text/javascript\">\r\n";
			$str .= "KindEditor.ready(function(K) {\r\n";
			$str .= "K.create('#" . $id . "', {\r\n";
			$str .= "cssPath : '" . PUBLIC_PATH . "Kindeditor/plugins/code/prettify.css',";
			//$str .= "uploadJson : '$upurl',";
			$str .= "fileManagerJson:'$upurl',";
			$str .= "editorid:'$id',";
			$str .= "upImgUrl:'$upImgUrl',";
			$str .= "upFlashUrl:'$upFlashUrl',";
			$str .= "upMediaUrl:'$upMediaUrl',";
			$str .= "allowFileManager : true\r\n";
			
			$str .= "});\r\n";
			$str .= "});\r\n";
			$str .= '</script>';
			$str .= '<div  class=\'editor_bottom2\'>';
			if ($info ['setup'] ['show_add_description'])
				$str .= '<input type="checkbox" name="add_description" value="1" checked /> ' . L ( 'add_description' ) . ' 
				<input type="text" name="description_length" value="200" style="width:24px;" size="3" />' . L ( 'description_length' );
			if ($info ['setup'] ['show_auto_thumb'])
				$str .= '<input type="checkbox" name="auto_thumb" value="1" checked /> ' . L ( 'auto_thumb' ) . ' 
				<input type="text" name="auto_thumb_no" value="1" size="1" />' . L ( 'auto_thumb_no' );
			$str .= '</div></div>';
		}
		return $str;
	}
	/**
	 * 日期选择
	 * @param array $info
	 * @param array $value
	 */
	public function datetime($info, $value) {
		$info ['setup'] = is_array ( $info ['setup'] ) ? $info ['setup'] : string2array ( $info ['setup'] );
		$id = $field = $info ['field'];
		$validate = getvalidate ( $info );
		if (ACTION_NAME == 'add') {
			$value = $value ? to_date ( $value, "Y-m-d H:i:s" ) : ($info ['setup'] ['default'] ? $info ['setup'] ['default'] : to_date ( time (), "Y-m-d H:i:s" ));
		} else {
			$value = $value ? to_date ( $value, "Y-m-d H:i:s" ) : to_date ( $this->data [$field], "Y-m-d H:i:s" );
		}
		//$value = $value ? to_date ( $value, "Y-m-d H:i:s" ) : to_date ( time (), "Y-m-d H:i:s" );
		

		$parseStr = '<input  class="Wdate put2  ' . $info ['class'] . '"  ' . $validate . '  name="' . $field . '" type="text" id="' . $id . '" size="25" onFocus="WdatePicker({dateFmt:\'yyyy-MM-dd HH:mm:ss\'
		})" value="' . $value . '" />';
		return $parseStr;
	}
	/**
	 * 分组选择复选框
	 * @param array $info
	 * @param string $value
	 */
	public function groupid($info, $value) {
		$newinfo = $info;
		$info ['setup'] = is_array ( $info ['setup'] ) ? $info ['setup'] : string2array ( $info ['setup'] );
		$groups = F ( 'Role' );
		$options = array ();
		foreach ( $groups as $key => $r ) {
			if ($r ['status']) {
				$options [$key] = $r ['name'];
			}
		}
		$newinfo ['options'] = $options;
		$fun = $info ['setup'] ['inputtype'];
		return $this->$fun ( $newinfo, $value );
	}
	
	/**
	 * 相关文章内容
	 * @param unknown_type $info
	 * @param unknown_type $value
	 */
	public function relation($info, $value){
		return '<b>asdfasdf</b>';
	}
	
	/**
	 * 推荐位选择
	 * @param array $info
	 * @param string $value
	 */
	public function posid($info, $value) {
		$newinfo = $info;
		$posids = F ( 'Position' );
		$sites = F ( 'Sitelist' );
		/* 根据站点显示推荐位 */
		foreach ($sites as $k => $v) {
			foreach ($posids as $key => $r){
				if ($r['siteid'] == $v['id']){
					$options[$v['name']] [$key] = $r ['name'];
				}
			}
		}
		$newinfo ['options'] = $options;
		$fun = $info ['setup'] ['inputtype'] ? $info ['setup'] ['inputtype'] : 'checkbox2';
		return $this->$fun ( $newinfo, $value );
	}
	
	public function typeid($info, $value) {
		$newinfo = $info;
		$validate = getvalidate ( $newinfo );
		$types = F ( 'Type' );
		
		$info ['setup'] = is_array ( $info ['setup'] ) ? $info ['setup'] : string2array ( $info ['setup'] );
		$id = $field = $info ['field'];
		$value = $value ? $value : $this->data [$field];
		$parentid = $info ['setup'] ['default'];
		//$keyid = $types [$parentid] ['keyid'];
		

		$options = array ();
		$options [''] = L ( 'please_chose' );
		foreach ( ( array ) $types as $key => $r ) {
			//			if ($r ['keyid'] != $keyid)
			//				continue;
			//			$r ['id'] = $r ['typeid'];
			if ($r ['siteid'] != cookie ( 'siteid' ))
				continue;
			$array [] = $r;
			$options [$key] = $r ['name'];
		}
		
		import ( 'Tree' );
		$str = "<option value='\$id' \$selected>\$spacer \$name</option>";
		$tree = new Tree ( $array );
		$tree->nbsp = '&nbsp;&nbsp;';
		$select_type = $tree->get_tree ( 0, $str, $value );
		
		$fun = $info ['setup'] ['inputtype'] ? $info ['setup'] ['inputtype'] : 'checkbox';
		//$append = $info['setup']['append'];
		if ($fun == 'select') {
			$str = '<select  id="' . $id . '" class="' . $info ['class'] . '" ' . $validate . '  name="' . $field . '"><option value="">' . L ( 'please_chose' ) . '</option>' . $select_type . '</select>';
			//			if($append){
			//				$str .= '&nbsp;<input type="text" name="type[name]" class="put2" placeholder="'.L('new_type').'" />';
			//			}
			return $str;
		} else {
			$newinfo ['options'] = $options;
			return self::$fun ( $newinfo, $value );
		}
	}
	/**
	 * 风格选择
	 * @param array $info
	 * @param array $value
	 */
	public function style($info, $value) {
		$newinfo = $info;
		$templates = temp_list ( get_current_sid () );
		$tpl [''] = L ( 'please_chose' );
		foreach ( $templates as $k => $v ) {
			$tpl [$v ['dirname']] = $v ['name'] ? $v ['name'] : $v ['dirname'];
		
		//unset($templates[$k]);
		}
		$newinfo ['options'] = $tpl;
		$newinfo ['setup'] = $info ['setup'];
		return Form::select ( $newinfo, $value );
	}
	/**
	 * 模板选择(栏目频道页|列表页|内容页)
	 * @param unknown_type $info
	 * @param unknown_type $value
	 */
	public function template($info, $value) {
		$newinfo = $info;
		$style = $info ['setup'] ['style'] ? $info ['setup'] ['style'] : $info ['style'];
		$module = $info ['setup'] ['module'] ? $info ['setup'] ['module'] : $info ['module'];
		$pre = $info ['setup'] ['pre'] ? $info ['setup'] ['pre'] : $info ['pre'];
		$cls = $info ['setup'] ['cls'] ? $info ['setup'] ['cls'] : $info ['cls'];
		$templatedir = TMPL_PATH . $style . '/' . $module . '/' . $cls . '/';
		$confing_path = TMPL_PATH . $style . '/' . 'config.php';
		$localdir = 'Tpl|' . $style . '|' . $module . '|' . $cls;
		$templates = glob ( $templatedir . $pre . '*.html' );
		if (empty ( $templates )) {
			$style = 'Default';
			$templatedir = TMPL_PATH . $style . '/' . $module . '/';
			$confing_path = TMPL_PATH . $style . '/config.php';
			$localdir = 'Tpl|' . $style . '|' . $module . '|' . $cls;
			$templates = glob ( $templatedir . $cls . '/' . $pre . '*.html' );
		}
		if (empty ( $templates )){
			return false;
		}
		$files = @array_map ( 'basename', $templates );
		if (file_exists ( $confing_path )) {
			$names = include $confing_path;
		}
		if (is_array ( $files )) {
			foreach ( $files as $file ) {
				$key = substr ( $file, 0, - 5 );
				$option [$key] = isset ( $names ['file_explan'] [$localdir] [$file] ) && ! empty ( $names ['file_explan'] [$localdir] [$file] ) ? $names ['file_explan'] [$localdir] [$file] . '(' . $file . ')' : $file;
			}
		}
		ksort ( $option );
		$newinfo ['options'] = $option;
		$newinfo ['field'] = $pre . '_template';
		return self::select ( $newinfo, $value );
	}
	/**
	 * 模板选择
	 * 
	 * @param $style  风格
	 * @param $module 模块
	 * @param $id 默认选中值
	 * @param $str 属性
	 * @param $pre 模板前缀
	 */
	public static function select_template($style, $module, $id = '', $str = '', $pre = '') {
		$tpl_root = TMPL_PATH;
		$templatedir = ROOT_PATH.$tpl_root.DIRECTORY_SEPARATOR.$style.DIRECTORY_SEPARATOR.$module.DIRECTORY_SEPARATOR;
		$confing_path = ROOT_PATH.$tpl_root.DIRECTORY_SEPARATOR.$style.DIRECTORY_SEPARATOR.'config.php';
		$localdir = str_replace(array('/', '\\'), '', $tpl_root).'|'.$style.'|'.$module;
		$templates = glob($templatedir.$pre.'*.html');
		if(empty($templates)) {
			$style = 'default';
			$templatedir = ROOT_PATH.$tpl_root.DIRECTORY_SEPARATOR.$style.DIRECTORY_SEPARATOR.$module.DIRECTORY_SEPARATOR;
			$confing_path = ROOT_PATH.$tpl_root.DIRECTORY_SEPARATOR.$style.DIRECTORY_SEPARATOR.'config.php';
			$localdir = str_replace(array('/', '\\'), '', $tpl_root).'|'.$style.'|'.$module;
			$templates = glob($templatedir.$pre.'*.html');
		}
		if(empty($templates)) return false;
		$files = @array_map('basename', $templates);
		$names = array();
		if(file_exists($confing_path)) {
			$names = include $confing_path;
		}
		$templates = array();
		if(is_array($files)) {
			foreach($files as $file) {
				$key = substr($file, 0, -5);
				$templates[$key] = isset($names['file_explan'][$localdir][$file]) && !empty($names['file_explan'][$localdir][$file]) ? $names['file_explan'][$localdir][$file].'('.$file.')' : $file;
			}
		}
		ksort($templates);
		return self::select($templates, $id, $str,L('please_select'));
	}
	/**
	 * 
	 * Enter description here ...
	 * @param unknown_type $info
	 * @param unknown_type $value
	 */
	public function image($info, $value) {
		$info ['setup'] = is_array ( $info ['setup'] ) ? $info ['setup'] : string2array ( $info ['setup'] );
		$id = $field = $info ['field'];
		$validate = getvalidate ( $info );
		if (ACTION_NAME == 'add') {
			$value = $value ? $value : $info ['setup'] ['default'];
		} else {
			$value = $value ? $value : $this->data [$field];
		}
		if (empty ( $info ['setup'] ['upload_maxsize'] )) {
			$Config = F ( 'Config' );
			$info ['setup'] ['upload_maxsize'] = intval ( byte_format ( $Config ['attach_maxsize'] ) );
		}
		
		$windcms_auth_key = sys_md5 ( C ( 'ENCRYPT' ) . $_SERVER ['HTTP_USER_AGENT'] );
		$windcms_auth = auth_code ( $this->isadmin . '-' . $info ['setup'] ['more'] . '-0-1-' . $info ['setup'] ['upload_allowext'] . '-' . $info ['setup'] ['upload_maxsize'] . '-' . $info ['modelid'], 'ENCODE', $windcms_auth_key );
		
		$parseStr = ' <div id="' . $field . '_aid_box"></div><input type="text"   class="put ' . $info ['class'] . '" name="' . $field . '"  id="' . $id . '" value="' . $value . '" size="' . $info ['setup'] ['size'] . '"  ' . $validate . '/> <input type="button" class="button" value="' . L ( 'upload_images' ) . '" onclick="javascript:swfupload(\'' . $field . '_uploadfile\',\'' . $field . '\',\'' . L ( 'uploadfiles' ) . '\',' . $this->isadmin . ',' . $info ['setup'] ['more'] . ',0,1,\'' . $info ['setup'] ['upload_allowext'] . '\',' . $info ['setup'] ['upload_maxsize'] . ',' . $info ['modelid'] . ',\'' . $windcms_auth . '\',up_image,nodo)"> ';
		return $parseStr;
	}
	
	public function images($info, $value) {
		$info ['setup'] = is_array ( $info ['setup'] ) ? $info ['setup'] : string2array ( $info ['setup'] );
		$id = $field = $info ['field'];
		$validate = getvalidate ( $info );
		if (ACTION_NAME == 'add') {
			$value = $value ? $value : $info ['setup'] ['default'];
		} else {
			$value = $value ? $value : $this->data [$field];
		}
		$data = '';
		$i = 0;
		if ($value) {
			$options = explode ( ":::", $value );
			if (is_array ( $options )) {
				foreach ( $options as $r ) {
					$v = explode ( "|", $r );
					$k = trim ( $v [1] );
					$optionsarr [$k] = $v [0];
					$data .= '<div id="uplistd_' . $i . '"><input type="text" class="put" name="' . $field . '[]" value="' . $v [0] . '"  /> <input type="text" class="put" name="' . $field . '_name[]" value="' . $v [1] . '" size="30" /> &nbsp;<a href="javascript:remove_this(\'uplistd_' . $i . '\');">' . L ( 'remove' ) . '</a> </div>';
					$i ++;
				}
			}
		}
		if (empty ( $info ['setup'] ['upload_maxsize'] )) {
			$Config = F ( 'Config' );
			$info ['setup'] ['upload_maxsize'] = intval ( byte_format ( $Config ['attach_maxsize'] ) );
		}
		$windcms_auth_key = sys_md5 ( C ( 'ENCRYPT' ) . $_SERVER ['HTTP_USER_AGENT'] );
		$windcms_auth = auth_code ( $this->isadmin . '-' . $info ['setup'] ['more'] . '-0-' . $info ['setup'] ['upload_maxnum'] . '-' . $info ['setup'] ['upload_allowext'] . '-' . $info ['setup'] ['upload_maxsize'] . '-' . $info ['modelid'], 'ENCODE', $windcms_auth_key );
		
		$parseStr = '
		<fieldset class="images_box">
        <legend>' . L ( 'upload_images' ) . '</legend><center><div>' . L ( 'upload_maxfiles' ) . ' <font color=\'red\'>' . $info ['setup'] ['upload_maxnum'] . '</font> ' . L ( 'zhang' ) . '</div></center>
		<div id="' . $field . '_images" class="imagesList"><input type="hidden"  name="' . $field . '[]" value=""/><input type="hidden"   name="' . $field . '_name[]" value="" />' . $data . '</div>
		</fieldset>
		<div class="c"></div>
		<a href="javascript:;" onclick="javascript:swfupload(\'' . $field . '_uploadfile\',\'' . $field . '\',\'' . L ( 'uploadfiles' ) . '\',' . $this->isadmin . ',' . $info ['setup'] ['more'] . ',0,' . $info ['setup'] ['upload_maxnum'] . ',\'' . $info ['setup'] ['upload_allowext'] . '\',' . $info ['setup'] ['upload_maxsize'] . ',' . $info ['modelid'] . ',\'' . $windcms_auth . '\',up_images,nodo)">' . L ( 'upload_images' ) . '</a>';
		
		return $parseStr;
	}
	public function file($info, $value) {
		$info ['setup'] = is_array ( $info ['setup'] ) ? $info ['setup'] : string2array ( $info ['setup'] );
		$id = $field = $info ['field'];
		$validate = getvalidate ( $info );
		if (ACTION_NAME == 'add') {
			$value = $value ? $value : $info ['setup'] ['default'];
		} else {
			$value = $value ? $value : $this->data [$field];
		}
		if (empty ( $info ['setup'] ['upload_maxsize'] )) {
			$Config = F ( 'Config' );
			$info ['setup'] ['upload_maxsize'] = intval ( byte_format ( $Config ['attach_maxsize'] ) );
		}
		$windcms_auth_key = sys_md5 ( C ( 'ENCRYPT' ) . $_SERVER ['HTTP_USER_AGENT'] );
		$windcms_auth = auth_code ( $this->isadmin . '-' . $info ['setup'] ['more'] . '-0-1-' . $info ['setup'] ['upload_allowext'] . '-' . $info ['setup'] ['upload_maxsize'] . '-' . $info ['modelid'], 'ENCODE', $windcms_auth_key );
		$parseStr = ' <div id="' . $field . '_aid_box"></div><input type="text"    class="put ' . $info ['class'] . '" name="' . $field . '"  id="' . $id . '" value="' . $value . '" size="' . $info ['setup'] ['size'] . '"  ' . $validate . '/> <input type="button" class="button" value="' . L ( 'uploadfiles' ) . '" onclick="javascript:swfupload(\'' . $field . '_uploadfile\',\'' . $field . '\',\'' . L ( 'uploadfiles' ) . '\',' . $this->isadmin . ',' . $info ['setup'] ['more'] . ',0,1,\'' . $info ['setup'] ['upload_allowext'] . '\',' . $info ['setup'] ['upload_maxsize'] . ',' . $info ['modelid'] . ',\'' . $windcms_auth . '\',up_image,nodo)"> ';
		return $parseStr;
	}
	
	public function files($info, $value) {
		$info ['setup'] = is_array ( $info ['setup'] ) ? $info ['setup'] : string2array ( $info ['setup'] );
		$id = $field = $info ['field'];
		$validate = getvalidate ( $info );
		if (ACTION_NAME == 'add') {
			$value = $value ? $value : $info ['setup'] ['default'];
		} else {
			$value = $value ? $value : $this->data [$field];
		}
		if (empty ( $info ['setup'] ['upload_maxsize'] )) {
			$Config = F ( 'Config' );
			$info ['setup'] ['upload_maxsize'] = intval ( byte_format ( $Config ['attach_maxsize'] ) );
		}
		$windcms_auth_key = sys_md5 ( C ( 'ENCRYPT' ) . $_SERVER ['HTTP_USER_AGENT'] );
		$windcms_auth = auth_code ( $this->isadmin . '-' . $info ['setup'] ['more'] . '-0-' . $info ['setup'] ['upload_maxnum'] . '-' . $info ['setup'] ['upload_allowext'] . '-' . $info ['setup'] ['upload_maxsize'] . '-' . $info ['modelid'], 'ENCODE', $windcms_auth_key );
		
		$parseStr = '<fieldset class="images_box">
        <legend>' . L ( 'upload_images' ) . '</legend><center><div>' . L ( 'upload_maxfiles' ) . ' <font color=\'red\'>' . $info ['setup'] ['upload_maxnum'] . '</font> ' . L ( 'zhang' ) . '</div></center>
		<div id="' . $field . '_images" class="imagesList"></div>
		</fieldset>
		<input type="button"  style="margin-left:5px;" class="button" value="' . L ( 'uploadfiles' ) . '" onclick="javascript:swfupload(\'' . $field . '_uploadfile\',\'' . $field . '\',\'' . L ( 'uploadfiles' ) . '\',' . $this->isadmin . ',' . $info ['setup'] ['more'] . ',0,' . $info ['setup'] ['upload_maxnum'] . ',\'' . $info ['setup'] ['upload_allowext'] . '\',' . $info ['setup'] ['upload_maxsize'] . ',' . $info ['modelid'] . ',\'' . $windcms_auth . '\',up_images,nodo)">  ';
		
		return $parseStr;
	}
}
?>