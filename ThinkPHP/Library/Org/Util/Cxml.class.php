<?php
namespace Org\Util;
class Cxml {
	
	public $root = 'rss';
	public $root_attributes = array ();
	public $charset = 'utf-8';
	public $NodeName = 'item';
	public $dom;
	
	public function __construct($data = null, $file = '') {
		if (! is_array ( $data ) || count ( $data ) == 0) {
			return false;
		}
		$this->dom = new \DOMDocument ( '1.0', $this->charset );
		// 添加DOM根元素
		$resultElement = $this->dom->createElement ( $this->root );
		// 设置DOM根元素属性
		$this->Attribute ( $this->root_attributes, $this->dom, $resultElement );
		// 将数组转换为xml添加到根元素
		$this->Array2Xml ( $this->dom, $data, $resultElement );
		// 加入DOM对象
		$this->dom->appendChild ( $resultElement );
		if ($file) {
			//生成xml文件
			$r = $this->dom->save ( $file );
			return $r;
		} else {
			//输出XML显示
			$this->outHeader ();
			return $this->dom->saveXML ();
		}
	}
	
	public function outHeader() {
		header ( "Expires: Mon, 26 Jul 1997 05:00:00 GMT" );
		header ( "Last-Modified: " . gmdate ( "D, d M Y H:i:s" ) . " GMT" );
		header ( "Cache-Control: no-cache" );
		header ( "Pragma: no-cache" );
		header ( "Content-type: text/xml; charset=" . $this->charset );
	}
	
	public function Xml2Array($file = '') {
		if (! is_file ( $file ))
			return false;
		
		//$dom = new DOMDocument('1.0',$this->charset);		
		//$array=$this->xml_to_array(simplexml_load_file($file));
		$array = $this->simplexml2array ( simplexml_load_file ( $file ) );
		return $array;
	}
	
	public function Array2Xml($dom, $data, $result = '') {
		if (is_array ( $data )) {
			foreach ( $data as $key => $value ) {
				if (is_numeric ( $key )) {
					$NodeName = $value ['NodeName'] ['value'];
					if ($value ['NodeName'] ['attributes'])
						$value ['attributes'] = $value ['NodeName'] ['attributes'];
					unset ( $value ['NodeName'] );
				} else {
					$NodeName = $key;
				}
				if (! isset ( $value ['value'] )) {
					$key_Element = $dom->createElement ( $NodeName );
					$result->appendChild ( $key_Element );
					if ($value ['attributes']) {
						$this->Attribute ( $value ['attributes'], $dom, $key_Element );
						unset ( $value ['attributes'] );
					}
					$this->Array2Xml ( $dom, $value, $key_Element );
				} else {
					$key_Element = $dom->createElement ( $NodeName );
					
					if ($value ['ishtml']) {
						$key_Element->appendChild ( $dom->createCDATASection ( $value ['value'] ) );
					} else {
						$key_Element->appendChild ( $dom->createTextNode ( $value ['value'] ) );
					}
					if ($value ['attributes']) {
						$this->Attribute ( $value ['attributes'], $dom, $key_Element );
					}
					$result->appendChild ( $key_Element );
				}
			}
			return $result;
		}
	}
	
	public function Attribute($att, $dom, $key_Element) {
		$attributes_element = '';
		foreach ( $att as $key => $rs ) {
			$attributes_element = $dom->createAttribute ( $key );
			$attributes_element->appendChild ( $dom->createTextNode ( $rs ) );
			$key_Element->appendChild ( $attributes_element );
		}
	}
	
	public function simplexml2array($xml) {
		$arXML = array ();
		$arXML ['name'] = trim ( $xml->getName () );
		$arXML ['value'] = trim ( ( string ) $xml );
		$t = array ();
		foreach ( $xml->attributes () as $name => $value ) {
			$t [$name] = trim ( $value );
		}
		$arXML ['attr'] = $t;
		$t = array ();
		foreach ( $xml->children () as $name => $xmlchild ) {
			
			if (! in_array ( $name, array_keys ( $t ) )) {
				$t [$name] = $this->simplexml2array ( $xmlchild );
			} else {
				if (! $t [$name] [0]) {
					$t [$name] = array ($t [$name] );
				}
				$t [$name] [] = $this->simplexml2array ( $xmlchild );
			}
		}
		$arXML ['children'] = $t;
		return ($arXML);
	}
}