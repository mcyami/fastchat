<?php
/**
 * 弹出警告
 * @author mcyami
 * @version 20150321
 */
namespace Org\Util;

class Notice {
	
	/** 
	 * js 弹窗并且跳转 
	 * @param string $_info 
	 * @param string $_url 
	 * @return js 
	 */
	static public function alertLocation($_info, $_url) {
		echo "<script type='text/javascript'>alert('$_info');location.href='$_url';</script>";
		exit ();
	}
	
	/** 
	 * js 弹窗返回 
	 * @param string $_info 
	 * @return js 
	 */
	static public function alertBack($_info) {
		echo "<script type='text/javascript'>alert('$_info');history.back();</script>";
		exit ();
	}
	
	/** 
	 * 页面跳转 
	 * @param string $url 
	 * @return js 
	 */
	static public function headerUrl($url) {
		echo "<script type='text/javascript'>location.href='{$url}';</script>";
		exit ();
	}
	
	/** 
	 * 弹窗关闭 
	 * @param string $_info 
	 * @return js 
	 */
	static public function alertClose($_info) {
		echo "<script type='text/javascript'>alert('$_info');close();</script>";
		exit ();
	}
	
	/** 
	 * 弹窗 
	 * @param string $_info 
	 * @return js 
	 */
	static public function alert($_info) {
		echo "<script type='text/javascript'>alert('$_info');</script>";
		exit ();
	}

}