<?php
/**
 * ucenter接口配置文件
 * ===============================================
 * @copyright 深圳市顶速网络科技有限公司  http://fasttop.top
 * ===============================================
 * @author: Ketity <970564173@qq.com>
 * @date: 2015-6-5
 */
//1.UC_CONNECT： 连接 UCenter 的方式: mysql/NULL, 默认为空时为 fscoketopen(). mysql 是直接连接的数据库, 为了效率, 建议采用 mysql
//2.UC_DBHOST： UCenter 数据库主机
//3.UC_DBUSER： UCenter 数据库用户名
//4.UC_DBPW： UCenter 数据库密码
//5.UC_DBNAME： UCenter 数据库名称
//6.UC_DBCHARSET： UCenter 数据库字符集
//7.UC_DBTABLEPRE： UCenter 数据库表前缀
//8.UC_DBCONNECT： UCenter 数据库持久连接 0=关闭, 1=打开
//9.UC_KEY： 与 UCenter 的通信密钥, 要与 UCenter 保持一致
//10.UC_API： UCenter 的 URL 地址, 在调用头像时依赖此常量
//11.UC_CHARSET： UCenter 的字符集
//12.UC_IP： UCenter 的 IP, 当 UC_CONNECT 为非 mysql 方式时, 并且当前应用服务器解析域名有问题时, 请设置此值
//13.UC_APPID： 当前应用的 ID
//14.UC_PPP： 分页
define ( 'UC_CONNECT', '' );
define ( 'UC_DBHOST', 'localhost' );
define ( 'UC_DBUSER', 'root' );
define ( 'UC_DBPW', 'root' );
define ( 'UC_DBNAME', 'ucenter' );
define ( 'UC_DBCHARSET', 'utf8' );
define ( 'UC_DBTABLEPRE', '`ucenter`.uc_' );
define ( 'UC_DBCONNECT', '0' );
define ( 'UC_KEY', '123456789' );
define ( 'UC_API', 'http://ucenter.cc/ucenter' );
define ( 'UC_CHARSET', 'utf-8' );
define ( 'UC_IP', '' );
define ( 'UC_APPID', '6' );
define ( 'UC_PPP', '20' );