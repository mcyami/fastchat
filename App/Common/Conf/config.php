<?php
return array(
	'MODULE_DENY_LIST' => array('Common','Api'),//设置禁止访问的模块列表
	'USER_ADMINISTRATOR' => 1, //超级管理员用户ID
	
	'LOAD_EXT_FILE' => 'uc', // 加载扩展函数库
	'LOAD_EXT_CONFIG' => 'ucenter', // 加载扩展函数库
	'SHOW_PAGE_TRACE' => true, //显示页面trace信息
	'ERROR_PAGE' => './404.html', //错误页面,部署模式下有效

	'SESSION_TYPE' => 'Db',
	'URL_MODEL' => 0,
	'LANG_SWITCH_ON' => true, // 开启语言包功能
	'LANG_AUTO_DETECT' => true, // 自动侦测语言 开启多语言功能后有效
	'LANG_LIST' => 'zh-cn,zh-tw,en-us', // 允许切换的语言列表 用逗号分隔
	'VAR_LANGUAGE' => 'l', // 默认语言切换变量
	'DEFAULT_LANG' => 'zh-cn',
	'DB_TYPE' => 'mysql', 
	'DB_HOST' => '127.0.0.1', 
	'DB_NAME' => 'chatlive', 
	'DB_USER' => 'root', 
	'DB_PWD' => 'root', 
	'DB_PORT' => '3306', 
	'DB_PREFIX' => 'ft_',
	
	'ADMIN_VERIFY' => false, // 是否开启后台验证码
	'MAX_LOGIN_FAILED_TIMES' => 8, // 登录失败最大次数
	'FAILED_LOCK_TIME' => 60, // 登录失败锁定分钟数

	'UC_CONNECT' => 'mysqll', // ucenter通信方式,mysql或http
);