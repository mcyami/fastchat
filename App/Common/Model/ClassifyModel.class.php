<?php
/**
 * 分类模型类
 * ===============================================
 * @copyright 深圳市顶速网络科技有限公司  http://fasttop.top
 * ===============================================
 * @author: Ketity <970564173@qq.com>
 * @date: 2015-6-9
 */
namespace Common\Model;
class ClassifyModel extends \Think\Model {
	
	protected $_validate = array ();
	protected $_auto = array ();
	
	public function _initialize() {
	
	}
	
	/**
	 * 根据条件获取分类数目
	 * @param array $where
	 */
	public function getCount($where = array()) {
		return $this->where ( $where )->count ();
	}
	
	/**
	 * 根据条件获取分类列表
	 * @param array $where
	 * @param string $order
	 * @param string $limit
	 */
	public function getList($where = array(), $order = 'id asc', $limit = '') {
		return $this->where ( $where )->order ( $order )->limit ( $limit )->select ();
	}
	
	/**
	 * 根据条件获取分类指定信息
	 * @param array $where
	 */
	public function getClassify($field, $where = array()) {
		return $this->where ( $where )->getField ( $field, true );
	}
}