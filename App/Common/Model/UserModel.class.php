<?php
/**
 * 用户模型类
 * ===============================================
 * @copyright 深圳市顶速网络科技有限公司  http://fasttop.top
 * ===============================================
 * @author: Ketity <970564173@qq.com>
 * @date: 2015-5-14
 */
namespace Common\Model;
class UserModel extends \Think\Model {
	
	/* 用户模型自动验证 */
	protected $_validate = array(
		/* 验证用户名 */
		array ('username', '2,30', - 1, self::EXISTS_VALIDATE, 'length' ), //用户名长度不合法
		array ('username', 'checkDenyUsername', - 2, self::EXISTS_VALIDATE, 'callback' ), //用户名禁止注册
		array ('username', '', - 3, self::EXISTS_VALIDATE, 'unique' ), //用户名被占用
		/* 验证密码 */
		array ('password', '6,32', - 4, self::EXISTS_VALIDATE, 'length' ), //密码长度不合法
		/* 验证邮箱 */
		array ('email', 'email', - 5, self::EXISTS_VALIDATE ), //邮箱格式不正确
		array ('email', '1,32', - 6, self::EXISTS_VALIDATE, 'length' ), //邮箱长度不合法
		array ('email', 'checkDenyEmail', - 7, self::EXISTS_VALIDATE, 'callback' ), //邮箱禁止注册
		array ('email', '', - 8, self::EXISTS_VALIDATE, 'unique' ), //邮箱被占用
		/* 验证手机号码 */
		array ('mobile', '//', - 9, self::EXISTS_VALIDATE ), //手机格式不正确 TODO:
		array ('mobile', 'checkDenyMobile', - 10, self::EXISTS_VALIDATE, 'callback' ), //手机禁止注册
		array ('mobile', '', - 11, self::EXISTS_VALIDATE, 'unique' ) ); //手机号被占用
	

	/* 用户模型自动完成 */
	protected $_auto = array (
		array ('reg_time', NOW_TIME, self::MODEL_INSERT ), 
		array ('reg_ip', 'get_client_ip', self::MODEL_INSERT, 'function' ), 
		array ('update_time', NOW_TIME ), 
		array ('status', 'getStatus', self::MODEL_BOTH, 'callback' ) 
	);
	
	public function _initialize() {
	
	}
	
	/**
	 * 检测用户名是不是被禁止注册
	 * @param  string $username 用户名
	 * @return boolean          ture - 未禁用，false - 禁止注册
	 */
	protected function checkDenyUsername($username) {
		return true; //TODO: 暂不限制，下一个版本完善
	}
	
	/**
	 * 检测邮箱是不是被禁止注册
	 * @param  string $email 邮箱
	 * @return boolean       ture - 未禁用，false - 禁止注册
	 */
	protected function checkDenyEmail($email) {
		return true; //TODO: 暂不限制，下一个版本完善
	}
	
	/**
	 * 检测手机是不是被禁止注册
	 * @param  string $mobile 手机
	 * @return boolean        ture - 未禁用，false - 禁止注册
	 */
	protected function checkDenyMobile($mobile) {
		return true; //TODO: 暂不限制，下一个版本完善
	}
	
	/**
	 * 根据配置指定用户状态
	 * @return integer 用户状态
	 */
	protected function getStatus() {
		return true; //TODO: 暂不限制，下一个版本完善
	}
	
	/**
	 * 增加一个用户
	 * @param string $username
	 * @param string $password
	 * @param string $email
	 * @param string $mobile
	 * @return integer 注册成功-用户信息，注册失败-错误编号
	 */
	public function addUser($username, $password, $email, $mobile, $info = array()) {
		$data = array ('username' => $username, 'email' => $email, 'mobile' => $mobile );
		if ($info) {
			$data = array_merge ( $data, $info );
		}
		// 验证手机
		if (empty ( $data ['mobile'] )) {
			unset ( $data ['mobile'] );
		}
		// 生成加密因子和密码
		$pass = \Org\Util\String::pwd ( $password );
		$data ['password'] = $pass ['password'];
		$data ['encrypt'] = $pass ['encrypt'];
		/* 添加用户 */
		if ($this->create ( $data )) {
			$uid = $this->add ();
			return $uid ? $uid : 0; //0-未知错误，大于0-注册成功
		} else {
			return $this->getError (); //错误详情见自动验证注释
		}
	
	}
	
	/**
	 * 更新用户信息
	 * @param int $uid 用户id
	 * @param string $password 密码，用来验证
	 * @param array $data 修改的字段数组
	 * @return true 修改成功，false 修改失败
	 */
	public function editUser($uid, $password, $data) {
		if (empty ( $uid ) || empty ( $data )) {
			$this->error = L ( 'error_login_0' );
			return false;
		}
		if ($password) {
			//更新前检查用户密码
			if (! $this->checkPassword ( $uid, $password )) {
				$this->error = '验证出错：密码不正确！';
				return false;
			}
		}
		// 更新用户信息
		$data = $this->create ( $data );
		if ($data) {
			return $this->where ( array ('uid' => $uid ) )->save ( $data );
		}
		return false;
	}
	
	/**
	 * 指定不同的字段返回用户信息
	 * @param unknown_type $field
	 * @param unknown_type $value
	 */
	public function getUser($field = 'username', $value) {
		$type_array = array ('username', 'email', 'mobile', 'uid' );
		if (in_array ( $field, $type_array )) {
			$where [$field] = $value;
			$user_info = $this->where ( $where )->find ();
			if (is_array ( $user_info )) {
				return $user_info;
			} else {
				return false; // 用户不存在
			}
		} else {
			// 不合法的获取类型
			return false;
		}
	}
	
	/**
	 * 通过用户ID删除用户，若是数组则进行批量删除
	 * @param unknown_type $uid
	 */
	public function deleteUser($uid) {
		if (is_array ( $uid )) {
			$ids = array ();
			foreach ( $uid as $k => $id ) {
				$ids [] = $this->deleteUserById ( $id );
			}
			return $ids;
		} else {
			return $this->deleteUserById ( $uid );
		}
	}
	
	/**
	 * 通过用户ID单个删除用户
	 * @param int $uid
	 */
	protected function deleteUserById($uid) {
		return $this->delete ( $uid );
	}
	
	/**
	 * 修改用户登录信息，用户最后登录的时间和登录IP
	 * @param int $uid
	 * @access public
	 */
	public function editLoginInfo($uid) {
		$where ['uid'] = $uid;
		$data ['last_login_time'] = NOW_TIME;
		$data ['last_login_ip'] = get_client_ip ();
		return $this->where ( $where )->save ( $data );
	}
	
	/**
	 * 根据指定条件获取用户数目
	 * @param array $where
	 */
	public function getUserCount($where = array()) {
		return $this->where ( $where )->count ();
	}
	
	/**
	 * 根据制定条件获取用户列表
	 * @param array $where
	 * @param string $order
	 * @param string $limit
	 */
	public function getUserList($where = array(), $order = 'uid asc', $limit = '') {
		return $this->where ( $where )->order ( $order )->limit ( $limit )->select ();
	}
	
	/**
	 * 检测是否登录
	 * @access public
	 */
	public function checkLogin() {
		$user = session ( 'user_auth' );
		if (empty ( $user )) {
			return 0;
		} else {
			return session ( 'user_auth_sign' ) == \Org\Util\String::authSign ( $user ) ? $user ['uid'] : 0;
		}
	}
	
	/**
	 * 检测是否是超级管理员
	 * @access public
	 * @param string $uid
	 */
	public function checkAdministrator($uid = NULL) {
		$uid = is_null ( $uid ) ? $this->checkLogin () : $uid;
		return $uid && (intval ( $uid ) === C ( 'USER_ADMINISTRATOR' ));
	}
	
	/**
	 * 检测验证码
	 * @param integer $code 验证码
	 * @param unknown_type $id
	 */
	public function checkVerify($code, $id = 1) {
		$verify = new \Think\Verify ();
		return $verify->check ( $code, $id );
	}
	
	/**
	 * 检测用户密码
	 * @param int $uid
	 * @param string $password_in
	 * @return true 验证成功，false 验证失败
	 */
	public function checkPassword($uid, $password_in) {
		$user = $this->getUser ( 'uid', $uid );
		if (\Org\Util\String::pwd ( $password_in, $user ['encrypt'] ) == $user ['password']) {
			return true;
		}
		return false;
	}

}