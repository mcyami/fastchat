<?php
/**
 * 用户角色组模型类
 * ===============================================
 * @copyright 深圳市顶速网络科技有限公司  http://fasttop.top
 * ===============================================
 * @author: Ketity <970564173@qq.com>
 * @date: 2015-6-6
 */
namespace Common\Model;
class AuthGroupAccessModel extends \Think\Model {
	
	protected $_validate = array ();
	protected $_auto = array ();
	public function _initialize() {
	
	}
	
	/**
	 * 添加用户对应角色组数据
	 * @param int $uid
	 * @param int $group_id
	 */
	public function addGroupAccess($uid, $group_id) {
		$data = array ('uid' => $uid, 'group_id' => $group_id );
		if ($this->create ( $data )) {
			$res = $this->add ();
			return $res ? $res : 0;
		} else {
			return $this->getError ();
		}
	}
	
	/**
	 * 编辑用户对应角色组信息
	 * @param int $uid
	 * @param int $group_id
	 */
	public function editGroupAccess($uid, $group_id) {
		if (empty ( $uid ) || empty ( $group_id )) {
			$this->error = L ( 'error_not_exists' );
			return false;
		}
		$data = array ('uid' => $uid, 'group_id' => $group_id );
		$data = $this->create ( $data );
		if ($data) {
			return $this->save ( $data );
		}
		return false;
	}
	
	/**
	 * 根据用户ID获取用户角色组ID
	 * @param int $uid
	 */
	public function getGrouopById($uid) {
		$where ['uid'] = $uid;
		return $this->where ( $where )->getField ( 'group_id' );
	}
	
	/**
	 * 
	 * 删除用户对应角色组
	 * @param int/array $uid
	 */
	public function deleteGroupAccess($uid) {
		if (is_array ( $uid )) {
			$where ['uid'] = array ('in', $uid );
		} else {
			$where ['uid'] = $uid;
		}
		return $this->where ( $where )->delete ();
	}
}