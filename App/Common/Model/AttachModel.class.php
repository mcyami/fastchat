<?php
/**
 * 附件模型类
 * ===============================================
 * @copyright 深圳市顶速网络科技有限公司  http://fasttop.top
 * ===============================================
 * @author: Ketity <970564173@qq.com>
 * @date: 2015-6-4
 */
namespace Common\Model;
class AttachModel extends \Think\Model {
	
	protected $_validate = array (
		array ('file_name', 'require', '附件名称不能为空', self::EXISTS_VALIDATE)
	);
	
	protected $_auto = array (
		array ('createtime', NOW_TIME, self::MODEL_INSERT),
		array ('upload_ip', 'get_client_ip', self::MODEL_INSERT, 'function')
	);
	
	public function _initialize() {
	
	}

}