<?php
/**
 * 菜单模型类
 * ===============================================
 * @copyright 深圳市顶速网络科技有限公司  http://fasttop.top
 * ===============================================
 * @author: Ketity <970564173@qq.com>
 * @date: 2015-5-20
 */
namespace Common\Model;
class MenuModel extends \Think\Model {
	
	protected $_validate = array (
		array ('title', 'require', '菜单名称不能为空' ), 
		array ('title', 'checkTitle', '菜单已存在', self::EXISTS_VALIDATE, 'callback' ), //菜单名称已存在
		array ('url', 'require', '链接URL不能为空' ) 
	);
	
	protected $_auto = array (
		array ('data', '' ) 
	);
	
	public function _initialize() {
	
	}
	
	/**
	 * 检测菜单是否已存在
	 * @param string $title
	 */
	protected function checkTitle($title) {
		$where ['title'] = $title;
		$detail = $this->where ( $where )->find ();
		if ($detail) {
			return false;
		}
		return true;
	}
	
	/**
	 * 根据条件获取菜单数目
	 * @param array $where
	 * @param string $order
	 */
	public function getCount($where = array(), $order = 'id asc') {
		return $this->where ( $where )->order ( $order )->count ();
	}
	
	/**
	 * 根据条件获取菜单列表
	 * @param array $where
	 * @param string $order
	 * @param string $limit
	 */
	public function getList($field = '*', $where = array(), $order = 'id asc', $limit = '') {
		return $this->field ( $field )->where ( $where )->order ( $order )->limit ( $limit )->select ();
	}
	
	/**
	 * 根据条件获取指定菜单字段信息
	 * @param string $field
	 * @param array $where
	 */
	public function getMenuField($field = '*', $where = array()) {
		return $this->where ( $where )->field ( $field )->find ();
	}
	
	/**
	 * 根据菜单ID获取菜单详细路径
	 * @param unknown_type $id
	 */
	public function getPathById($id) {
		$path = array ();
		$nav = $this->getMenuField ( 'id,parentid,title', array ('id' => $id ) );
		$path [] = $nav;
		if ($nav ['parentid'] >= 1) {
			$parent = $this->getPathById ( $nav ['parentid'] );
			$path = array_merge ( $parent, $path  );
		}
		return $path;
	}
}