<?php
/**
 * Ucenter 函数库
 * ===============================================
 * @copyright 深圳市顶速网络科技有限公司  http://fasttop.top
 * ===============================================
 * @author: Ketity <970564173@qq.com>
 * @date: 2015-6-5
 */

define ( 'UC_API_FUNC', UC_CONNECT == 'mysql' ? 'uc_api_mysql' : 'uc_api_post' ); //UCenter连接方式
define ( 'UC_ROOT', substr ( __FILE__, 0, - 10 ) ); //note 用户中心客户端的根目录 UC_CLIENTROOT
define ( 'UC_CLIENT_VERSION', 'fasttop 1.0' ); //note UCenter 版本标识
define ( 'UC_CLIENT_RELEASE', '20150517' );

/**
 * 
 * Enter description here ...
 * @param unknown_type $xml
 * @param unknown_type $isnormal
 */
function xml_unserialize(&$xml, $isnormal = FALSE) {
	$xml_parser = new \Org\Util\UcXml ( $isnormal );
	$data = $xml_parser->parse ( $xml );
	$xml_parser->destruct ();
	return $data;
}

/**
 * 
 * Enter description here ...
 * @param unknown_type $arr
 * @param unknown_type $htmlon
 * @param unknown_type $isnormal
 * @param unknown_type $level
 */
function xml_serialize($arr, $htmlon = FALSE, $isnormal = FALSE, $level = 1) {
	$s = $level == 1 ? "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>\r\n<root>\r\n" : '';
	$space = str_repeat ( "\t", $level );
	foreach ( $arr as $k => $v ) {
		if (! is_array ( $v )) {
			$s .= $space . "<item id=\"$k\">" . ($htmlon ? '<![CDATA[' : '') . $v . ($htmlon ? ']]>' : '') . "</item>\r\n";
		} else {
			$s .= $space . "<item id=\"$k\">\r\n" . xml_serialize ( $v, $htmlon, $isnormal, $level + 1 ) . $space . "</item>\r\n";
		}
	}
	$s = preg_replace ( "/([\x01-\x08\x0b-\x0c\x0e-\x1f])+/", ' ', $s );
	return $level == 1 ? $s . "</root>" : $s;
}

/**
 * 
 * 在执行sql语句时，在需要在某些字符前加上了反斜线，这些字符有单引号(')、双引号(")、反斜线(\)
 * @param string/array $string	如果$string 为字符串，则直接用转换，如果是数组的话，通过foreach循环遂个进行转换
 * @param bool $force	
 * @param bool $strip
 * @return string 
 * get_magic_quotes_gpc():本函式取得 PHP 环境设定的变数 magic_quotes_gpc (GPC, Get/Post/Cookie) 值。如果传回0为关闭，1为打开
 * string addslashes(string str) 在某些字符前加上了反斜线
 * string stripslashes (string str) 将用addslashes()函数处理后的字符串返回原样。
 */
function uc_addslashes($string, $force = 0, $strip = FALSE) {
	if (! MAGIC_QUOTES_GPC || $force) {
		if (is_array ( $string )) {
			foreach ( $string as $key => $val ) {
				$string [$key] = uc_addslashes ( $val, $force, $strip );
			}
		} else {
			$string = addslashes ( $strip ? stripslashes ( $string ) : $string );
		}
	}
	return $string;
}

//判断是否存在daddslashes函数，如果不存在则定义
if (! function_exists ( 'daddslashes' )) {
	/**
	 * 
	 * 在需要在某些字符前加上了反斜线
	 * @param string $string
	 * @param bool $force
	 * @return string
	 */
	function daddslashes($string, $force = 0) {
		return uc_addslashes ( $string, $force );
	}
}

/**
 * 将用addslashes()函数处理后的字符串返回原样
 * @param string $string
 * @return string
 */
function uc_stripslashes($string) {
	if (MAGIC_QUOTES_GPC) {
		return stripslashes ( $string );
	} else {
		return $string;
	}
}

/**
 * 使用post方式连接UCenter,取指定的模块和动作的数据
 * @param string $module	请求的模块
 * @param string $action	请求的动作
 * @param array $arg	参数（会加密的方式传送）
 */
function uc_api_post($module, $action, $arg = array()) {
	$s = $sep = '';
	foreach ( $arg as $k => $v ) {
		$k = urlencode ( $k );
		if (is_array ( $v )) {
			$s2 = $sep2 = '';
			foreach ( $v as $k2 => $v2 ) {
				$k2 = urlencode ( $k2 );
				$s2 .= "$sep2{$k}[$k2]=" . urlencode ( uc_stripslashes ( $v2 ) );
				$sep2 = '&';
			}
			$s .= $sep . $s2;
		} else {
			$s .= "$sep$k=" . urlencode ( uc_stripslashes ( $v ) );
		}
		$sep = '&';
	}
	$postdata = uc_api_requestdata ( $module, $action, $s );
	/*echo $postdata;exit ();*/
	return uc_fopen2 ( UC_API . '/index.php', 500000, $postdata, '', TRUE, UC_IP, 20 );
}

/**
 * 构造发送给用户中心的请求数据
 * @param string $module	请求的模块
 * @param string $action	请求的动作
 * @param string $arg	参数（会加密的方式传送）
 * @param string $extra	附加参数（传送时不加密）
 * @return string
 */
function uc_api_requestdata($module, $action, $arg = '', $extra = '') {
	$input = uc_api_input ( $arg );
	$post = "m=$module&a=$action&inajax=2&release=" . UC_CLIENT_RELEASE . "&input=$input&appid=" . UC_APPID . $extra;
	return $post;
}

function uc_api_url($module, $action, $arg = '', $extra = '') {
	$url = UC_API . '/index.php?' . uc_api_requestdata ( $module, $action, $arg, $extra );
	return $url;
}

function uc_api_input($data) {
	$s = urlencode ( uc_authcode ( $data . '&agent=' . md5 ( $_SERVER ['HTTP_USER_AGENT'] ) . "&time=" . time (), 'ENCODE', UC_KEY ) );
	return $s;
}

/**
 * 使用mysql方式连接UCenter,并取指定的模块和动作的数据
 * @param string $model	请求的模块
 * @param string $action	请求的动作
 * @param mixed $args	参数（会加密的方式传送）
 * @return	mixed
 * 
 */
function uc_api_mysql($model, $action, $args = array()) {
	global $uc_controls;
	if (empty ( $uc_controls [$model] )) {
		include_once UC_ROOT . './lib/db.class.php';
		include_once UC_ROOT . './model/base.php';
		include_once UC_ROOT . "./control/$model.php";
		//例如:  $uc_controls['app'] = new appcontrol();
		eval ( "\$uc_controls['$model'] = new {$model}control();" );
	}
	if ($action {0} != '_') {
		$args = uc_addslashes ( $args, 1, TRUE );
		$action = 'on' . $action;
		$uc_controls [$model]->input = $args;
		return $uc_controls [$model]->$action ( $args );
	} else {
		return '';
	}
}

/**
 * 
 * 字符串加密以及解密函数
 * @param string $string 原文或者密文
 * @param string $operation 操作(ENCODE | DECODE), 默认为 DECODE
 * @param string $key 密钥
 * @param int $expiry 密文有效期, 加密时候有效， 单位 秒，0 为永久有效
 * @return string 处理后的 原文或者 经过 base64_encode 处理后的密文
 * @example
		$a = authcode('abc', 'ENCODE', 'key');
		$b = authcode($a, 'DECODE', 'key'); // $b(abc)
		
		$a = authcode('abc', 'ENCODE', 'key', 3600);
		$b = authcode('abc', 'DECODE', 'key'); // 在一个小时内，$b(abc)，否则 $b 为空
 * 
 */
function uc_authcode($string, $operation = 'DECODE', $key = '', $expiry = 0) {
	//note 随机密钥长度 取值 0-32;
	//note 加入随机密钥，可以令密文无任何规律，即便是原文和密钥完全相同，加密结果也会每次不同，增大破解难度。
	//note 取值越大，密文变动规律越大，密文变化 = 16 的 $ckey_length 次方
	//note 当此值为 0 时，则不产生随机密钥
	$ckey_length = 4;
	$key = md5 ( $key ? $key : UC_KEY );
	$keya = md5 ( substr ( $key, 0, 16 ) );
	$keyb = md5 ( substr ( $key, 16, 16 ) );
	$keyc = $ckey_length ? ($operation == 'DECODE' ? substr ( $string, 0, $ckey_length ) : substr ( md5 ( microtime () ), - $ckey_length )) : '';
	
	$cryptkey = $keya . md5 ( $keya . $keyc );
	$key_length = strlen ( $cryptkey );
	
	$string = $operation == 'DECODE' ? base64_decode ( substr ( $string, $ckey_length ) ) : sprintf ( '%010d', $expiry ? $expiry + time () : 0 ) . substr ( md5 ( $string . $keyb ), 0, 16 ) . $string;
	$string_length = strlen ( $string );
	
	$result = '';
	$box = range ( 0, 255 );
	
	$rndkey = array ();
	for($i = 0; $i <= 255; $i ++) {
		$rndkey [$i] = ord ( $cryptkey [$i % $key_length] );
	}
	
	for($j = $i = 0; $i < 256; $i ++) {
		$j = ($j + $box [$i] + $rndkey [$i]) % 256;
		$tmp = $box [$i];
		$box [$i] = $box [$j];
		$box [$j] = $tmp;
	}
	
	for($a = $j = $i = 0; $i < $string_length; $i ++) {
		$a = ($a + 1) % 256;
		$j = ($j + $box [$a]) % 256;
		$tmp = $box [$a];
		$box [$a] = $box [$j];
		$box [$j] = $tmp;
		$result .= chr ( ord ( $string [$i] ) ^ ($box [($box [$a] + $box [$j]) % 256]) );
	}
	
	if ($operation == 'DECODE') {
		if ((substr ( $result, 0, 10 ) == 0 || substr ( $result, 0, 10 ) - time () > 0) && substr ( $result, 10, 16 ) == substr ( md5 ( substr ( $result, 26 ) . $keyb ), 0, 16 )) {
			return substr ( $result, 26 );
		} else {
			return '';
		}
	} else {
		return $keyc . str_replace ( '=', '', base64_encode ( $result ) );
	}
}

/**
 * 
 * 远程打开URL
 * @param string $url 打开的url，　如 http://www.baidu.com/123.htm
 * @param int $limit 取返回的数据的长度
 * @param string $post 要发送的 POST 数据，如uid=1&amp;amp;amp;password=1234
 * @param string $cookie 要模拟的 COOKIE 数据，如uid=123&amp;amp;amp;auth=a2323sd2323
 * @param bool $bysocket TRUE/FALSE 是否通过SOCKET打开
 * @param string $ip IP地址
 * @param int $timeout 连接超时时间
 * @param bool $block 是否为阻塞模式
 * @return 取到的字符串
 */
function uc_fopen2($url, $limit = 0, $post = '', $cookie = '', $bysocket = FALSE, $ip = '', $timeout = 15, $block = TRUE) {
	$__times__ = isset ( $_GET ['__times__'] ) ? intval ( $_GET ['__times__'] ) + 1 : 1;
	if ($__times__ > 2) {
		return '';
	}
	$url .= (strpos ( $url, '?' ) === FALSE ? '?' : '&') . "__times__=$__times__";
	return uc_fopen ( $url, $limit, $post, $cookie, $bysocket, $ip, $timeout, $block );
}

/**
 * 
 * 
 * 
 * 
 */
function uc_fopen($url, $limit = 0, $post = '', $cookie = '', $bysocket = FALSE, $ip = '', $timeout = 15, $block = TRUE) {
	$return = '';
	$matches = parse_url ( $url );
	! isset ( $matches ['host'] ) && $matches ['host'] = '';
	! isset ( $matches ['path'] ) && $matches ['path'] = '';
	! isset ( $matches ['query'] ) && $matches ['query'] = '';
	! isset ( $matches ['port'] ) && $matches ['port'] = '';
	$host = $matches ['host'];
	$path = $matches ['path'] ? $matches ['path'] . ($matches ['query'] ? '?' . $matches ['query'] : '') : '/';
	$port = ! empty ( $matches ['port'] ) ? $matches ['port'] : 80;
	
	//	请求测试url
	//	echo '$path='.$path.'<br />';
	//	echo '$port='.$port.'<br />';
	//	echo '$host='.$host.'<br />';
	//	echo '$post='.$post.'<br />';
	//	exit;
	if ($post) {
		$out = "POST $path HTTP/1.0\r\n";
		$out .= "Accept: */*\r\n";
		//$out .= "Referer: $boardurl\r\n";
		$out .= "Accept-Language: zh-cn\r\n";
		$out .= "Content-Type: application/x-www-form-urlencoded\r\n"; //
		$out .= "User-Agent: $_SERVER[HTTP_USER_AGENT]\r\n"; //客户端浏览器版本
		$out .= "Host: $host\r\n";
		$out .= 'Content-Length: ' . strlen ( $post ) . "\r\n";
		$out .= "Connection: Close\r\n";
		$out .= "Cache-Control: no-cache\r\n";
		$out .= "Cookie: $cookie\r\n\r\n";
		$out .= $post;
	} else {
		$out = "GET $path HTTP/1.0\r\n";
		$out .= "Accept: */*\r\n";
		//$out .= "Referer: $boardurl\r\n";
		$out .= "Accept-Language: zh-cn\r\n";
		$out .= "User-Agent: $_SERVER[HTTP_USER_AGENT]\r\n";
		$out .= "Host: $host\r\n";
		$out .= "Connection: Close\r\n";
		$out .= "Cookie: $cookie\r\n\r\n";
	}
	if (function_exists ( 'fsockopen' )) {
		$fp = @fsockopen ( ($ip ? $ip : $host), $port, $errno, $errstr, $timeout );
	} elseif (function_exists ( 'pfsockopen' )) {
		$fp = @pfsockopen ( ($ip ? $ip : $host), $port, $errno, $errstr, $timeout );
	} else {
		$fp = false;
	}
	if (! $fp) {
		return '';
	} else {
		stream_set_blocking ( $fp, $block );
		stream_set_timeout ( $fp, $timeout );
		@fwrite ( $fp, $out );
		$status = stream_get_meta_data ( $fp );
		if (! $status ['timed_out']) {
			while ( ! feof ( $fp ) ) {
				if (($header = @fgets ( $fp )) && ($header == "\r\n" || $header == "\n")) {
					break;
				}
			}
			$stop = false;
			while ( ! feof ( $fp ) && ! $stop ) {
				if ($limit == 0 || $limit > 8192) {
					$limit = 8192;
				} else {
					$limit = $limit;
				}
				$data = fread ( $fp, $limit );
				$return .= $data;
				if ($limit) {
					$limit -= strlen ( $data );
					$stop = $limit <= 0;
				}
			}
		}
		return $return;
	}
}

/**
 * 
 * 获取UCenter中所有应用的列表数据
 * @return array 应用列表数据
 */
function uc_app_ls() {
	$return = call_user_func ( UC_API_FUNC, 'app', 'ls', array () );
	return UC_CONNECT == 'mysql' ? $return : xml_unserialize ( $return );
}

/**
 * 
 * 用于向UCenter Home 添加事件,如果正确则返回事件的ID
 * @param string $icon	图标类型,如thread、post、video、goods、reward、debate、blog、album、comment、wall、friend
 * @param integer $uid	用户 ID
 * @param string $username	用户名
 * @param string $title_template	标题模板
 * @param string $title_data	标题数据数组
 * @param string $body_template	内容模板
 * @param string $body_data	模板数据
 * @param string $body_general	相同事件合并时用到的数据：特定的数组，只有两项：name、link，保留
 * @param string $target_ids	保留
 * @param array $images	相关图片的 URL 和链接地址
 * @return integer	事件的ID
 */
function uc_feed_add($icon, $uid, $username, $title_template = '', $title_data = '', $body_template = '', $body_data = '', $body_general = '', $target_ids = '', $images = array()) {
	return call_user_func ( UC_API_FUNC, 'feed', 'add', array ('icon' => $icon, 'appid' => UC_APPID, 'uid' => $uid, 'username' => $username, 'title_template' => $title_template, 'title_data' => $title_data, 'body_template' => $body_template, 'body_data' => $body_data, 'body_general' => $body_general, 'target_ids' => $target_ids, 'image_1' => $images [0] ['url'], 'image_1_link' => $images [0] ['link'], 'image_2' => $images [1] ['url'], 'image_2_link' => $images [1] ['link'], 'image_3' => $images [2] ['url'], 'image_3_link' => $images [2] ['link'], 'image_4' => $images [3] ['url'], 'image_4_link' => $images [3] ['link'] ) );
}

/**
 * 
 * 提取事件,如果正确则返回事件列表数组
 * @param integer $limit	取事件条数,默认为100条
 * @param bool $delete
 * @return array 事件列表数据,详见下面
 * 
 * 
		key					类型			value
		feedid	 		integer	 	事件的 ID
		appid	 		integer		 所在应用的 ID
		icon	 		string	 	事件的图标 thread、poll、reward 等
		uid	 			integer	 	事件的发起人的用户 ID
		username	 	string	 	发起人的用户名
		dateline	 	integer	 	时间，UNIX 时间戳格式
		hash_template	 string	 	模板的 Hash 值，用来相同类型事件的合并，32位字符串，如:c95dbd9aa75862c841b627e1e9598fd5
		hash_data	 	string	 	数据的 Hash 值，用来相同类型事件的合并，32位字符串，如:c95dbd9aa75862c841b627e1e9598fd5
		title_template	 string		 标题模板
		title_data	 	string	 	标题数据
		body_template	 string	 	内容模板
		body_data	 	string	 	事件内容 HTML 格式，用 {xxx} 格式字符表示变量，如 {username}
		body_general	 string	 	保留
		image_1	 		string		 第一张图片的 URL
		image_1_link	 string	 	第一张图片链接的 URL
		image_2	 		string	 	第二张图片的 URL
		image_2_link	 string	 	第二张图片链接的 URL
		image_3	 		string	 	第三张图片的 URL
		image_3_link	 string	 	第三张图片链接的 URL
		image_4	 		string	 	第四张图片的 URL
		image_4_link	 string	 	第四张图片链接的 URL
 * 
 * 
 */
function uc_feed_get($limit = 100, $delete = TRUE) {
	$return = call_user_func ( UC_API_FUNC, 'feed', 'get', array ('limit' => $limit, 'delete' => $delete ) );
	return UC_CONNECT == 'mysql' ? $return : xml_unserialize ( $return );
}

/**
 * 
 * 把friendid添加为uid的好友
 * @param integer $uid	用户ID
 * @param integer $friendid	好友用户ID
 * @param string $comment	备注,可为空
 * @return bool 1:成功    0:失败
 */
function uc_friend_add($uid, $friendid, $comment = '') {
	return call_user_func ( UC_API_FUNC, 'friend', 'add', array ('uid' => $uid, 'friendid' => $friendid, 'comment' => $comment ) );
}

/**
 * 
 * 删除指定用户的好友
 * @param integer $uid	用户ID
 * @param array $friendids	好友用户ID数组
 * @return integer 被删除的好友数
 */
function uc_friend_delete($uid, $friendids) {
	return call_user_func ( UC_API_FUNC, 'friend', 'delete', array ('uid' => $uid, 'friendids' => $friendids ) );
}

/**
 * 
 * 获取指定用户的好友数
 * @param integer $uid	用户ID
 * @param integer $direction	0:(默认值)指定用户的全部好友	1:正向,指定用户添加的好友,但没被对方添加	2:反向,指定用户被哪些用户添加为好友,但没有添加对方	3:双向,互相添加为好友
 * @return integer 好友数目
 */
function uc_friend_totalnum($uid, $direction = 0) {
	return call_user_func ( UC_API_FUNC, 'friend', 'totalnum', array ('uid' => $uid, 'direction' => $direction ) );
}

/**
 * 
 * 获取指定用户的好友列表
 * @param integer $uid	用户ID
 * @param integer $page	当前页码,默认值是1
 * @param integer $pagesize	每页最大条目数,默认值 10
 * @param integer $totalnum	好友总数,默认值10
 * @param integer $direction	0:(默认值)指定用户的全部好友    1:正向    2:反向    3:双向
 * @return array 返回好友列表数据
 */
function uc_friend_ls($uid, $page = 1, $pagesize = 10, $totalnum = 10, $direction = 0) {
	$return = call_user_func ( UC_API_FUNC, 'friend', 'ls', array ('uid' => $uid, 'page' => $page, 'pagesize' => $pagesize, 'totalnum' => $totalnum, 'direction' => $direction ) );
	return UC_CONNECT == 'mysql' ? $return : xml_unserialize ( $return );
}

/**
 * 
 * 用户注册接口
 * @param string $username	用户名
 * @param sString $password	密码
 * @param string $email	邮箱
 * @param int $questionid	安全问题索引
 * @param string $answer	安全问题答案
 * @param string $regip	注册ip
 * @return integer	
 * 大于0	:	返回用户id,表示用户注册成功
 * -1	:	用户名不合法
 * -2	:	包含不允许注册的词语
 * -3	:	用户名已经存在
 * -4	:	E-mail格式有误
 * -5	:	E-mail不允许注册
 * -6	:	E-mail已被注册
 * 
 */
function uc_user_register($username, $password, $email, $questionid = '', $answer = '', $regip = '') {
	return call_user_func ( UC_API_FUNC, 'user', 'register', array ('username' => $username, 'password' => $password, 'email' => $email, 'questionid' => $questionid, 'answer' => $answer, 'regip' => $regip ) );
}

/**
 * 
 * 用户登陆接口
 * @param string $username	用户名
 * @param string $password	密码
 * @param bool $isuid	是否使用用户ID登陆	0:(默认值)使用用户名登陆	1:使用用户ID登陆
 * @param bool $checkques	是否验证安全提问	0:(默认值)不验证安全提问	1:验证安全提问
 * @param int $questionid	安全提问索引
 * @param string $answer	安全提问答案
 * @return array
 * int[0]		大于0:返回用户ID,表示用户登陆成功    -1:用户不存在或者被删除    -2:密码错误    3:安全提问错误
 * string[1]	用户名
 * string[2]	密码
 * string[3]	E-mail
 * bool[4]		用户名是否重名
 * 
 */
function uc_user_login($username, $password, $isuid = 0, $checkques = 0, $questionid = '', $answer = '') {
	$isuid = intval ( $isuid );
	$return = call_user_func ( UC_API_FUNC, 'user', 'login', array ('username' => $username, 'password' => $password, 'isuid' => $isuid, 'checkques' => $checkques, 'questionid' => $questionid, 'answer' => $answer ) );
	return UC_CONNECT == 'mysql' ? $return : xml_unserialize ( $return );
}

/**
 * 
 * 同步登陆接口
 * @param integer $uid	用户id
 * @return string	同步登陆的HTML代码
 * 
 */
function uc_user_synlogin($uid) {
	$uid = intval ( $uid );
	$sso = new SSOAPI ();
	$userInfo = $sso->info ( $uid );
	$domain = M ( 'Agentsite' )->where ( 'Agentid = ' . $userInfo ['siteid'] )->getfield ( 'domain' );
	
	if (@include UC_ROOT . './data/cache/apps.php') {
		//var_dump($_CACHE['apps']);exit;
		if (count ( $_CACHE ['apps'] ) > 1) {
			$return = uc_api_post ( 'user', 'synlogin', array ('uid' => $uid ) );
		
		//var_dump($return);exit;
		} else {
			$return = '';
		}
	
	}
	return $return;
}

/**
 * 
 * 同步退出接口
 * @return string 同步退出的HTML代码
 */
function uc_user_synlogout() {
	if (@include UC_ROOT . './data/cache/apps.php') {
		if (count ( $_CACHE ['apps'] ) > 1) {
			$return = uc_api_post ( 'user', 'synlogout', array () );
		} else {
			$return = '';
		}
	}
	return $return;
}

/**
 * 
 * 更新用户资料接口
 * @param string $username	用户名
 * @param string $oldpw	旧密码
 * @param string $newpw	新密码,如不修改为空
 * @param string $email	如不修改为空
 * @param bool $ignoreoldpw	是否忽略旧密码
 * @param integer $questionid	安全提问索引
 * @param string $answer	安全提问答案
 * @return integer
 * 1:更新成功
 * 0:没有做任何修改
 * -1:E-mail格式有误
 * -5:E-mail不允许注册
 * -6:该E-mail已经被注册
 * -7:没有做任何修改
 * -8:该用户受保护,无权限修改
 * 
 */
function uc_user_edit($username, $oldpw, $newpw, $email, $ignoreoldpw = 0, $questionid = '', $answer = '') {
	return call_user_func ( UC_API_FUNC, 'user', 'edit', array ('username' => $username, 'oldpw' => $oldpw, 'newpw' => $newpw, 'email' => $email, 'ignoreoldpw' => $ignoreoldpw, 'questionid' => $questionid, 'answer' => $answer ) );
}

/**
 * 
 * 删除用户接口
 * @param integer $uid	用户id
 * @return integer	1:删除成功		2:删除失败
 */
function uc_user_delete($uid) {
	return call_user_func ( UC_API_FUNC, 'user', 'delete', array ('uid' => $uid ) );
}

/**
 * 
 * 删除用户头像接口
 * @param integer $uid	用户ID
 * 
 */
function uc_user_deleteavatar($uid) {
	uc_api_post ( 'user', 'deleteavatar', array ('uid' => $uid ) );
}

/**
 * 
 * 检查用户名
 * @param string $username	用户名
 * @return integer 
 * 1:成功
 * -1:用户名不合法
 * -2:包含不允许注册的词语
 * -3:用户名已经存在
 */
function uc_user_checkname($username) {
	return call_user_func ( UC_API_FUNC, 'user', 'check_username', array ('username' => $username ) );
}

/**
 * 
 * 检查E-mail地址
 * @param string $email	电子邮箱
 * @return integer
 * 1:成功
 * -4:Email格式有误
 * -5:Email不允许注册
 * -6:该Email已经被注册
 * 
 */
function uc_user_checkemail($email) {
	return call_user_func ( UC_API_FUNC, 'user', 'check_email', array ('email' => $email ) );
}

/**
 * 
 * 添加保护用户
 * @param string/array $username	保护用户名
 * @param string $admin	操作的管理员
 * @return integer 1:成功  -1:失败
 * 
 */
function uc_user_addprotected($username, $admin = '') {
	return call_user_func ( UC_API_FUNC, 'user', 'addprotected', array ('username' => $username, 'admin' => $admin ) );
}

/**
 * 
 * 删除保护用户
 * @param string/array $username	保护用户名
 * @return integer 1:成功    -1:失败
 */
function uc_user_deleteprotected($username) {
	return call_user_func ( UC_API_FUNC, 'user', 'deleteprotected', array ('username' => $username ) );
}

/**
 * 
 * 得到受保护的用户名列表
 * @return array 受保护的用户数据
 */
function uc_user_getprotected() {
	$return = call_user_func ( UC_API_FUNC, 'user', 'getprotected', array ('1' => 1 ) );
	return UC_CONNECT == 'mysql' ? $return : xml_unserialize ( $return );
}

/**
 * 
 * 获取用户数据接口
 * @param string $username	用户名
 * @param bool $isuid	是否使用用户ID获取	0:(默认值)使用用户名获取	1:使用用户ID获取
 * @return array
 * integer[0]	用户ID
 * string[1]	用户名
 * string[2]	Email
 * 
 */
function uc_get_user($username, $isuid = 0) {
	$return = call_user_func ( UC_API_FUNC, 'user', 'get_user', array ('username' => $username, 'isuid' => $isuid ) );
	return UC_CONNECT == 'mysql' ? $return : xml_unserialize ( $return );
}

/**
 * 
 * 合并重名用户接口
 * @param string $oldusername	老用户名
 * @param string $newusername	新用户名
 * @param integer $uid	用户ID
 * @param string $password	密码
 * @param string $email	电子邮件
 * @return integer 
 * 大于0:返回用户ID,表示合并成功
 * -1:用户名不合法
 * -2:包含不允许注册的词语
 * -3:用户名已经存在
 * 
 */
function uc_user_merge($oldusername, $newusername, $uid, $password, $email) {
	return call_user_func ( UC_API_FUNC, 'user', 'merge', array ('oldusername' => $oldusername, 'newusername' => $newusername, 'uid' => $uid, 'password' => $password, 'email' => $email ) );
}

/**
 * 
 * 移除重名用户记录接口
 * @param string $username	用户名
 */
function uc_user_merge_remove($username) {
	return call_user_func ( UC_API_FUNC, 'user', 'merge_remove', array ('username' => $username ) );
}

/**
 * 
 * 获取指定应用的指定用户积分
 * @param integer $appid	应用ID
 * @param integer $uid	用户ID
 * @param integer $credit	积分编号
 * @return integer 积分
 */
function uc_user_getcredit($appid, $uid, $credit) {
	return uc_api_post ( 'user', 'getcredit', array ('appid' => $appid, 'uid' => $uid, 'credit' => $credit ) );
}

/**
 * 
 * 进入短消息中心
 * @param integer $uid	用户ID
 * @param bool $newpm	是否直接查看未读短消息	0:(默认值)否	1:是
 */
function uc_pm_location($uid, $newpm = 0) {
	$apiurl = uc_api_url ( 'pm_client', 'ls', "uid=$uid", ($newpm ? '&folder=newbox' : '') );
	@header ( "Expires: 0" );
	@header ( "Cache-Control: private, post-check=0, pre-check=0, max-age=0", FALSE );
	@header ( "Pragma: no-cache" );
	@header ( "location: $apiurl" );
}

/**
 * 
 * 检查新的短消息
 * @param integer $uid	用户ID
 * @param integer $more	
 * 0:(默认值)只返回未读消息数
 * 1:返回私人消息数,未读消息数
 * 2:返回未读消息数,私人消息数,公共消息数,系统消息数
 * 3:返回未读消息数,私人消息数,公共消息数,系统消息数,最后消息时间,最后消息内容等
 * 
 * 当$more=0
 * @return integer 未读消息数
 * 
 * 其他情况
 * @return array
 * integer[newpm]	未读消息数
 * integer['newprivatepm']	私人消息数
 * integer['announcepm']	公共消息数
 * integer['systempm']	系统消息数
 * integer['lastdate']	最后消息时间
 * integer['lastmsgfromid']	最后消息发件人id
 * string['lastmsgfrom']	最后发件人用户名
 * string['lastmsg']	最后消息内容
 */
function uc_pm_checknew($uid, $more = 0) {
	$return = call_user_func ( UC_API_FUNC, 'pm', 'check_newpm', array ('uid' => $uid, 'more' => $more ) );
	return (! $more || UC_CONNECT == 'mysql') ? $return : xml_unserialize ( $return );
}

/**
 * 
 * 发送短消息
 * @param integer $fromuid	发件人用户ID,0为系统消息
 * @param string $msgto	收件人用户名,用户ID,多个用逗号分隔
 * @param string $subject	消息标题
 * @param string $message	消息内容
 * @param bool $instantly	是否直接发送	1(默认值):直接发送消息	0:进入发送短消息的界面
 * @param integer $replypmid	回复的消息ID	大于0:回复指定的短消息	0:(默认值)发送新的短消息
 * @param bool $isusername	msgto是否为用户名	0:(默认值)msgto参数为用户ID	1:msgto参数为用户名
 * @param integer $type	类别
 * @return integer 
 * 大于0:发送成功的最后一条消息ID
 * 0:发送失败
 * -1:超出24小时最大允许发送短消息数目
 * -2:不满足两次发送短消息最小间隔
 * -3:不能给非好友批量发送短消息
 * -4:目前还不能使用发送短消息功能(注册多少日后才可以使用发送短消息限制)
 * 
 */
function uc_pm_send($fromuid, $msgto, $subject, $message, $instantly = 1, $replypmid = 0, $isusername = 0, $type = 0) {
	if ($instantly) {
		$replypmid = @is_numeric ( $replypmid ) ? $replypmid : 0;
		return call_user_func ( UC_API_FUNC, 'pm', 'sendpm', array ('fromuid' => $fromuid, 'msgto' => $msgto, 'subject' => $subject, 'message' => $message, 'replypmid' => $replypmid, 'isusername' => $isusername, 'type' => $type ) );
	} else {
		$fromuid = intval ( $fromuid );
		$subject = rawurlencode ( $subject );
		$msgto = rawurlencode ( $msgto );
		$message = rawurlencode ( $message );
		$replypmid = @is_numeric ( $replypmid ) ? $replypmid : 0;
		$replyadd = $replypmid ? "&pmid=$replypmid&do=reply" : '';
		$apiurl = uc_api_url ( 'pm_client', 'send', "uid=$fromuid", "&msgto=$msgto&subject=$subject&message=$message$replyadd" );
		@header ( "Expires: 0" );
		@header ( "Cache-Control: private, post-check=0, pre-check=0, max-age=0", FALSE );
		@header ( "Pragma: no-cache" );
		@header ( "location: " . $apiurl );
	}
}

/**
 * 
 * 删除短消息接口1
 * @param integer $uid	用户ID
 * @param string $folder	短消息所在文件夹
 * @param array $pmids	消息ID数组
 * @return integer 被删除的短消息数
 */
function uc_pm_delete($uid, $folder, $pmids) {
	return call_user_func ( UC_API_FUNC, 'pm', 'delete', array ('uid' => $uid, 'pmids' => $pmids ) );
}

/**
 * 
 * 删除短消息接口2
 * @param integer $uid	用户ID
 * @param array $touids	对方用户ID数组
 * @return integer 被删除的短消息数
 */
function uc_pm_deleteuser($uid, $touids) {
	return call_user_func ( UC_API_FUNC, 'pm', 'deleteuser', array ('uid' => $uid, 'touids' => $touids ) );
}

/**
 * 
 * 用于群聊短消息的退出和删除。plids 参数是一个自由 Key 数组类型，如“ array(1, 2, 3, 4, 5 ...) ”，数组的每一个值均为 ID。
 * @param integer $uid	用户 ID
 * @param array $plids	要删除的消息会话 ID 数组
 * @param bool $type	类别	0:(默认值) 退出群聊	1:删除群聊
 */
function uc_pm_deletechat($uid, $plids, $type = 0) {
	return call_user_func ( UC_API_FUNC, 'pm', 'deletechat', array ('uid' => $uid, 'plids' => $plids, 'type' => $type ) );
}

/**
 * 
 * 标记短消息已读/未读状态
 * @param integer $uid	用户ID
 * @param array $uids	要标记的对方用户ID数组
 * @param array $plids	要标记的消息ID数组,默认值空数组
 * @param bool $status	要标记的状态	0:(默认值)标记为已读	1:标记为未读
 */
function uc_pm_readstatus($uid, $uids, $plids = array(), $status = 0) {
	return call_user_func ( UC_API_FUNC, 'pm', 'readstatus', array ('uid' => $uid, 'uids' => $uids, 'plids' => $plids, 'status' => $status ) );
}

/**
 * 
 * 获取短消息列表接口
 * @param integer $uid 用户ID
 * @param integer $page	当前页编号,默认值1
 * @param integer $pagesize	每页最大条目数,默认值10
 * @param string $folder	短消息所在文件夹	newbox:新件箱    inbox:(默认值)收件箱    outbox:发件箱
 * @param string $filter	过滤方式	newpm:(默认值)未读短消息    folder为inbox和outbox时使用    systempm为系统消息    announcepm为公共消息
 * @param integer $msglen	截取短消息内容文字的长度	0:(默认值)不截取
 * @return array 
 * integer['count']	消息总数
 * array['data'] 短消息列表数据
 */
function uc_pm_list($uid, $page = 1, $pagesize = 10, $folder = 'inbox', $filter = 'newpm', $msglen = 0) {
	$uid = intval ( $uid );
	$page = intval ( $page );
	$pagesize = intval ( $pagesize );
	$return = call_user_func ( UC_API_FUNC, 'pm', 'ls', array ('uid' => $uid, 'page' => $page, 'pagesize' => $pagesize, 'filter' => $filter, 'msglen' => $msglen ) );
	return UC_CONNECT == 'mysql' ? $return : xml_unserialize ( $return );
}

/**
 * 
 * 忽略未读消息提示
 * @param integer $uid	用户ID
 */
function uc_pm_ignore($uid) {
	$uid = intval ( $uid );
	return call_user_func ( UC_API_FUNC, 'pm', 'ignore', array ('uid' => $uid ) );
}

/**
 * 
 * 获取短消息内容
 * @param integer $uid	用户ID
 * @param integer $pmid	消息ID
 * @param integer $touid	消息对方用户ID
 * @param integer $daterange	日期范围	1:(默认值)今天    2:昨天    3:前天    4:上周    5:更早
 * @param integer $page	当前页码
 * @param integer $pagesize	每页最大条数
 * @param integer $type	消息类型	1:私人消息    2:群聊消息
 * @param integer $isplid	touid参数是回话id还是用户id
 * @return array 短消息内容数据
 */
function uc_pm_view($uid, $pmid = 0, $touid = 0, $daterange = 1, $page = 0, $pagesize = 10, $type = 0, $isplid = 0) {
	$uid = intval ( $uid );
	$touid = intval ( $touid );
	$page = intval ( $page );
	$pagesize = intval ( $pagesize );
	$pmid = @is_numeric ( $pmid ) ? $pmid : 0;
	$return = call_user_func ( UC_API_FUNC, 'pm', 'view', array ('uid' => $uid, 'pmid' => $pmid, 'touid' => $touid, 'daterange' => $daterange, 'page' => $page, 'pagesize' => $pagesize, 'type' => $type, 'isplid' => $isplid ) );
	return UC_CONNECT == 'mysql' ? $return : xml_unserialize ( $return );
}

/**
 * 
 * 返回指定会话的消息数量
 * @param integer $uid	用户ID
 * @param integer $touid	查找会话ID或者用户ID
 * @param bool $isplid	touid参数是回话ID还是用户ID
 * @return integer 指定会话的消息数量
 */
function uc_pm_view_num($uid, $touid, $isplid) {
	$uid = intval ( $uid );
	$touid = intval ( $touid );
	$isplid = intval ( $isplid );
	return call_user_func ( UC_API_FUNC, 'pm', 'viewnum', array ('uid' => $uid, 'touid' => $touid, 'isplid' => $isplid ) );
}

/**
 * 
 * 查询单条短消息内容
 * @param integer $uid	用户ID
 * @param integer $type	消息的类型
 * @param integer $pmid	消息ID
 * @return array 短消息内容数据
 */
function uc_pm_viewnode($uid, $type, $pmid) {
	$uid = intval ( $uid );
	$type = intval ( $type );
	$pmid = @is_numeric ( $pmid ) ? $pmid : 0;
	$return = call_user_func ( UC_API_FUNC, 'pm', 'viewnode', array ('uid' => $uid, 'type' => $type, 'pmid' => $pmid ) );
	return UC_CONNECT == 'mysql' ? $return : xml_unserialize ( $return );
}

/**
 * 
 * 群聊成员列表
 * @param integer $uid	用户ID
 * @param integer $plid	群聊会话ID
 * @return array 返回群聊成员列表信息
 */
function uc_pm_chatpmmemberlist($uid, $plid = 0) {
	$uid = intval ( $uid );
	$plid = intval ( $plid );
	$return = call_user_func ( UC_API_FUNC, 'pm', 'chatpmmemberlist', array ('uid' => $uid, 'plid' => $plid ) );
	return UC_CONNECT == 'mysql' ? $return : xml_unserialize ( $return );
}

/**
 * 
 * 将指定用户踢出群聊会话
 * @param integer $plid	会话ID
 * @param integer $uid	用户ID
 * @param integer $touid	踢出的用户ID
 * @return integer 1:踢出成功    2:踢出失败
 */
function uc_pm_kickchatpm($plid, $uid, $touid) {
	$uid = intval ( $uid );
	$plid = intval ( $plid );
	$touid = intval ( $touid );
	return call_user_func ( UC_API_FUNC, 'pm', 'kickchatpm', array ('uid' => $uid, 'plid' => $plid, 'touid' => $touid ) );
}

/**
 * 
 * 添加群聊成员
 * @param integer $plid	会话ID
 * @param integer $uid	用户ID
 * @param integer $touid	添加的用户ID
 * @return integer 1:添加成功    2:添加失败
 */
function uc_pm_appendchatpm($plid, $uid, $touid) {
	$uid = intval ( $uid );
	$plid = intval ( $plid );
	$touid = intval ( $touid );
	return call_user_func ( UC_API_FUNC, 'pm', 'appendchatpm', array ('uid' => $uid, 'plid' => $plid, 'touid' => $touid ) );
}

/**
 * 
 * 获取用户黑名单设置的内容
 * @param integer $uid	用户ID
 * @return string 黑名单内容数据
 */
function uc_pm_blackls_get($uid) {
	$uid = intval ( $uid );
	return call_user_func ( UC_API_FUNC, 'pm', 'blackls_get', array ('uid' => $uid ) );
}

/**
 * 
 * 更新用户的黑名单列表数据
 * @param integer $uid	用户ID
 * @param string $blackls	黑名单内容
 * @return bool 1:更新成功    0:更新失败
 */
function uc_pm_blackls_set($uid, $blackls) {
	$uid = intval ( $uid );
	return call_user_func ( UC_API_FUNC, 'pm', 'blackls_set', array ('uid' => $uid, 'blackls' => $blackls ) );
}

/**
 * 
 * 添加用户的黑名单项目
 * @param integer $uid	用户ID
 * @param array $username	用户名数组
 * @return 1:添加成功    0:添加失败
 */
function uc_pm_blackls_add($uid, $username) {
	$uid = intval ( $uid );
	return call_user_func ( UC_API_FUNC, 'pm', 'blackls_add', array ('uid' => $uid, 'username' => $username ) );
}

/**
 * 
 * 删除用户的黑名单项目
 * @param integer $uid	用户ID
 * @param string $username	用户名数组
 */
function uc_pm_blackls_delete($uid, $username) {
	$uid = intval ( $uid );
	return call_user_func ( UC_API_FUNC, 'pm', 'blackls_delete', array ('uid' => $uid, 'username' => $username ) );
}

/**
 * 
 * Enter description here ...
 */
function uc_domain_ls() {
	$return = call_user_func ( UC_API_FUNC, 'domain', 'ls', array ('1' => 1 ) );
	return UC_CONNECT == 'mysql' ? $return : xml_unserialize ( $return );
}

/**
 * 
 * 积分兑换处理接口
 * @param integer $uid	用户ID
 * @param integer $from	原积分
 * @param integer $to	目标积分
 * @param integer $toappid	目标应用ID
 * @param integer $amount	积分数额
 * @return bool 1:请求成功	0:请求失败
 */
function uc_credit_exchange_request($uid, $from, $to, $toappid, $amount) {
	$uid = intval ( $uid );
	$from = intval ( $from );
	$toappid = intval ( $toappid );
	$to = intval ( $to );
	$amount = intval ( $amount );
	return uc_api_post ( 'credit', 'request', array ('uid' => $uid, 'from' => $from, 'to' => $to, 'toappid' => $toappid, 'amount' => $amount ) );
}

/**
 * 
 * 本接口会向用户中心发起一个请求，要求获取用户中心中所有应用的标签数据。如果指定了 totalnum，那么将按照用户中心标签设置中的数字按比例返回相应条目数，如果未指定每个应用将返回所有的 10 条数据。
 * @param string $tagname	标签名称
 * @param array $nums	指定每个应用返回多少条数据	数组结构如array('1' => '10', '2' => '15')，键为应用ID, 值为返回的数据条数
 * @return 返回标签列表数据    
		单条标签数组结构
			key	类型	value
			data	 array	 标签内容数组，请参看下方 单条标签内容数组结构
			type	 integer	 应用类型，值为 DISCUZ、SUPESITE、XSPACE、SUPEV、ECSHOP、OTHER
		单条标签内容数组结构
			key	类型	value
			url	 string	 URL
			subject	 string	 标题
			extra	 string	 扩展数据
 * 
 */
function uc_tag_get($tagname, $nums = 0) {
	$return = call_user_func ( UC_API_FUNC, 'tag', 'gettag', array ('tagname' => $tagname, 'nums' => $nums ) );
	return UC_CONNECT == 'mysql' ? $return : xml_unserialize ( $return );
}

/**
 * 
 * 返回设置用户头像的 HTML 代码，HTML 会输出一个 Flash，生成的显示效果类似如下效果。	Avatar fK4AXnJgvbTB.jpg
 * @param integer $uid	用户 ID
 * @param string $type	头像类型	real:真实头像	virtual:(默认值) 虚拟头像
 * @param bool $returnhtml	是否返回 HTML代码	1:(默认值) 是，返回设置头像的 HTML代码	0:否，返回设置头像的 Flash调用数组
 */
function uc_avatar($uid, $type = 'virtual', $returnhtml = 1) {
	$uid = intval ( $uid );
	$uc_input = uc_api_input ( "uid=$uid" );
	$uc_avatarflash = UC_API . '/images/camera.swf?inajax=1&appid=' . UC_APPID . '&input=' . $uc_input . '&agent=' . md5 ( $_SERVER ['HTTP_USER_AGENT'] ) . '&ucapi=' . urlencode ( str_replace ( 'http://', '', UC_API ) ) . '&avatartype=' . $type . '&uploadSize=2048';
	if ($returnhtml) {
		return '<object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=9,0,0,0" width="450" height="253" id="mycamera" align="middle">
			<param name="allowScriptAccess" value="always" />
			<param name="scale" value="exactfit" />
			<param name="wmode" value="transparent" />
			<param name="quality" value="high" />
			<param name="bgcolor" value="#ffffff" />
			<param name="movie" value="' . $uc_avatarflash . '" />
			<param name="menu" value="false" />
			<embed src="' . $uc_avatarflash . '" quality="high" bgcolor="#ffffff" width="450" height="253" name="mycamera" align="middle" allowScriptAccess="always" allowFullScreen="false" scale="exactfit"  wmode="transparent" type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/go/getflashplayer" />
		</object>';
	} else {
		return array ('width', '450', 'height', '253', 'scale', 'exactfit', 'src', $uc_avatarflash, 'id', 'mycamera', 'name', 'mycamera', 'quality', 'high', 'bgcolor', '#ffffff', 'menu', 'false', 'swLiveConnect', 'true', 'allowScriptAccess', 'always' );
	}
}

/**
 * 
 * 将邮件加入到邮件队列，或者直接发送邮件（当level为0时）。
 * @param string $uids	用户 ID 多个用逗号(,)隔开
 * @param string $emails	目标email，多个用逗号(,)隔开
 * @param string $subject	邮件标题
 * @param string $message	邮件内容
 * @param mail $frommail	发信人，可选参数，默认为空，uc后台设置的邮件来源作为发信人地址
 * @param string $charset	邮件字符集，可选参数，默认为gbk
 * @param bool $htmlon	是否是html格式的邮件，可选参数，默认为FALSE，即文本邮件
 * @param integer $level	邮件级别，可选参数，默认为1，数字大的优先发送，取值为0的时候立即发送，邮件不入队列
 * @return mixed	false:失败：进入队列失败，或者发送失败			integer:成功：进入队列的邮件的id，当level为0，则返回1
 */
function uc_mail_queue($uids, $emails, $subject, $message, $frommail = '', $charset = 'gbk', $htmlon = FALSE, $level = 1) {
	return call_user_func ( UC_API_FUNC, 'mail', 'add', array ('uids' => $uids, 'emails' => $emails, 'subject' => $subject, 'message' => $message, 'frommail' => $frommail, 'charset' => $charset, 'htmlon' => $htmlon, 'level' => $level ) );
}

/**
 * 
 * 用于检测指定用户的头像是否存在
 * @param integer $uid	用户 ID
 * @param string $size	头像大小	big:大头像（200 x 250）		middle:(默认值) 中头像（120 x 120），默认值为此		small:小头像（48 x 48）
 * @param string $type	头像类型	real:真实头像		virtual:(默认值) 虚拟头像，默认值为此
 * @return bool 1:头像存在	0:头像不存在
 */
function uc_check_avatar($uid, $size = 'middle', $type = 'virtual') {
	$url = UC_API . "/avatar.php?uid=$uid&size=$size&type=$type&check_file_exists=1";
	$res = uc_fopen2 ( $url, 500000, '', '', TRUE, UC_IP, 20 );
	if ($res == 1) {
		return 1;
	} else {
		return 0;
	}
}

/**
 * 
 * 取得UCenter服务端的版本信息和数据库的版本信息	
 * 检测uc_server的数据库版本和程序版本
 * @return mixd
 * array('db' => 'xxx', 'file' => 'xxx');
 * null 无法调用到接口
 * string 文件版本低于1.5
 */
function uc_check_version() {
	//version:文件名	check:方法名	array():参数数组
	$return = uc_api_post ( 'version', 'check', array () );
	$data = xml_unserialize ( $return );
	return is_array ( $data ) ? $data : $return;
}
function uc_bbs_threadnum($post = 0) {
	return uc_api_post ( 'bbs', 'threadnum', array ('post' => $post ) );
}