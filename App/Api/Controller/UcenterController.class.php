<?php
/**
 * Ucenter接收通知接口
 * ===============================================
 * @copyright 深圳市顶速网络科技有限公司  http://fasttop.top
 * ===============================================
 * @author: 小陈先生 <945146147@qq.com>
 * @date: 2015-5-17
 */
namespace Api\Controller;
use Common\Controller\ApiBaseController;
class UcenterController extends ApiBaseController {
	
	public function _initialize() {
		parent::_initialize ();
		
		define ( 'API_DELETEUSER', 1 ); //note 用户删除 API 接口开关
		define ( 'API_RENAMEUSER', 1 ); //note 用户改名 API 接口开关
		define ( 'API_GETTAG', 1 ); //note 获取标签 API 接口开关
		define ( 'API_SYNLOGIN', 1 ); //note 同步登录 API 接口开关
		define ( 'API_SYNLOGOUT', 1 ); //note 同步登出 API 接口开关
		define ( 'API_UPDATEPW', 1 ); //note 更改用户密码 开关
		define ( 'API_UPDATEBADWORDS', 1 ); //note 更新关键字列表 开关
		define ( 'API_UPDATEHOSTS', 1 ); //note 更新域名解析缓存 开关
		define ( 'API_UPDATEAPPS', 1 ); //note 更新应用列表 开关
		define ( 'API_UPDATECLIENT', 1 ); //note 更新客户端缓存 开关
		define ( 'API_UPDATECREDIT', 1 ); //note 更新用户积分 开关
		define ( 'API_GETCREDITSETTINGS', 1 ); //note 向 UCenter 提供积分设置 开关
		define ( 'API_GETCREDIT', 1 ); //note 获取用户的某项积分 开关
		define ( 'API_UPDATECREDITSETTINGS', 1 ); //note 更新应用积分设置 开关
		

		define ( 'API_RETURN_SUCCEED', '1' ); // 返回成功
		define ( 'API_RETURN_FAILED', '-1' ); // 返回失败
		define ( 'API_RETURN_FORBIDDEN', '-2' );
		
		// 两个用不着的常量,待定
		//define ( 'ROOT_PATH', '../' );
		//define ( 'UC_PATH', ROOT_PATH . 'Ucenter/' );
		define ( 'UC_DB_DSN', 'mysql://root:root@127.0.0.1:3306/uc' ); // 数据库连接，使用Model方式调用API必须配置此项
		define ( 'UC_TABLE_PREFIX', 'uc_' ); // 数据表前缀，使用Model方式调用API必须配置此项
	}
	
	/**
	 * 入口方法
	 */
	public function index() {
		error_reporting ( 0 );
		set_magic_quotes_runtime ( 0 );
		
		$_DCACHE = $get = $post = array ();
		
		$code = @$_GET ['code'];
		
		parse_str ( _authcode ( $code, 'DECODE', UC_KEY ), $get );
		if (MAGIC_QUOTES_GPC) {
			$get = _stripslashes ( $get );
		}
		$timestamp = time ();
		
		if ($timestamp - $get ['time'] > 3600) {
			exit ( 'Authracation has expiried' );
		}
		if (empty ( $get )) {
			exit ( 'Invalid Request' );
		}
		$action = $get ['action'];
		
		//require_once UC_PATH . 'lib/xml.class.php';
		$xml = new \Org\Util\Xml ();
		$post = $xml->xml_unserialize ( file_get_contents ( 'php://input' ) );
		
		if (in_array ( $get ['action'], array ('test', 'deleteuser', 'renameuser', 'gettag', 'synlogin', 'synlogout', 'updatepw', 'updatebadwords', 'updatehosts', 'updateapps', 'updateclient', 'updatecredit', 'getcreditsettings', 'updatecreditsettings' ) )) {
			exit ( $this->$get ['action'] ( $get, $post ) );
		} else {
			exit ( API_RETURN_FAILED );
		}
	
	}
	
	/**
	 * 通信测试
	 */
	protected function test() {
		return API_RETURN_SUCCEED;
	}
	
	/**
	 * 删除用户
	 */
	protected function deleteuser($get, $post) {
		$uids = $get ['ids'];
		! API_DELETEUSER && exit ( API_RETURN_FORBIDDEN );
		// 执行删除用户
		return D ( 'User' )->deleteUser ( $uids ) ? API_RETURN_SUCCEED : API_RETURN_FAILED;
	}

}