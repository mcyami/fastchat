<?php
/**
 * 附件管理控制器
 * ===============================================
 * @copyright 深圳市顶速网络科技有限公司  http://fasttop.top
 * ===============================================
 * @author: Ketity <970564173@qq.com>
 * @date: 2015-5-23
 */
namespace Admin\Controller;
class AttachController extends BaseController {
	
	public function _initialize() {
		parent::_initialize ();
	}
	
	/**
	 * 载入upload空间
	 * @param array $file
	 */
	public function load($file) {
		$config = array ();
		$config ['rootPath'] = UPLOAD_PATH;
		$upload = new \Think\Upload ( $config ); //使用框架内置的Upload控件
		$uploadList = $upload->upload ( $file );
		return $uploadList;
	}
	
	/**
	 * 保存附件
	 * @param string $pic_name 附件名
	 * @param array $uploadList
	 */
	public function addAttach($pic_name, $uploadList = array()) {
		// TODO： 加水印
		$attach ['userid'] = UID;
		$attach ['file_name'] = $uploadList [$pic_name] ['name'];
		$attach ['file_path'] = substr ( './Uploads/' . $uploadList [$pic_name] ['savepath'] . strtolower ( $uploadList [$pic_name] ['savename'] ), 1 );
		$attach ['file_size'] = $uploadList [$pic_name] ['size'];
		$attach ['file_ext'] = strtolower ( $uploadList [$pic_name] ['ext'] );
		$attachData = D ( 'Attach' )->create ( $attach );
		if ($attachData === false) {
			$this->error ( D ( 'Attach' )->getError () );
		}
		if (! D ( 'Attach' )->add ( $attachData )) {
			$this->error ( D ( 'Attach' )->getError () );
		}
	}
}