<?php
/**
 * 菜单管理控制器
 * ===============================================
 * @copyright 深圳市顶速网络科技有限公司  http://fasttop.top
 * ===============================================
 * @author: Ketity <970564173@qq.com>
 * @date: 2015-5-18
 */
namespace Admin\Controller;
class MenuController extends BaseController {
	
	/**
	 * 初始化
	 * @see Admin\Controller.BaseController::_initialize()
	 */
	public function _initialize() {
		parent::_initialize ();
		$menus = D ( 'Menu' )->getList ( '*', array ('status=1' ) ); //获取所有允许显示的菜单
		$menuArr = array (); //定义菜单空数组
		foreach ( $menus as $menu ) {
			if (isset ( $_GET ['parentid'] ) && $menu ['id'] == $_GET ['parentid']) {
				$menu ['selected'] = 'selected';
			} else {
				$menu ['selected'] = '';
			}
			$menuArr [] = $menu;
		}
		$str = "<option value='\$id' {\$selected}>{\$spacer} {\$title}</option>";
		$tree = new \Org\Util\Tree ( $menuArr );
		$menuData = $tree->get_tree ( 0, $str ); //获取树形菜单结构
		$this->assign ( 'memudata', $menuData );
	}
	
	/**
	 * 菜单列表
	 */
	public function index() {
		$parentid = I ( 'get.parentid', 0 );
		$title = I ( 'post.title' );
		if ($title) {
			$where ['title'] = array ('like', "%{$title}%" ); //按菜单名称模糊查询,此时不用考虑菜单父级ID
			$this->assign ( 'title', $title );
		} else {
			$where ['parentid'] = $parentid;
		}
		$count = D ( 'Menu' )->getCount ( $where, 'sort_order desc,id asc' );
		$limit = $this->paging ( $count ); //分页
		$menus = D ( 'Menu' )->getList ( '*', $where, 'sort_order desc,id asc', $limit );
		$menulist = D ( 'Menu' )->getField ( 'id,title' );
		foreach ( $menus as $k => $v ) {
			$menus [$k] ['parent'] = $menulist [$v ['parentid']] ? $menulist [$v ['parentid']] : L ( 'text_menu_top' );
		}
		$this->assign ( 'list', $menus );
		$this->display ();
	}

}