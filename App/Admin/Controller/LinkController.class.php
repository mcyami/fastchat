<?php
/**
 * 友情链接管理控制器
 * ===============================================
 * @copyright 深圳市顶速网络科技有限公司  http://fasttop.top
 * ===============================================
 * @author: Ketity <970564173@qq.com>
 * @date: 2015-5-19
 */
namespace Admin\Controller;
class LinkController extends BaseController {
	
	public function _initialize() {
		parent::_initialize ();
	}
	
	/**
	 * 友情链接列表
	 */
	public function index() {
		$where = array ();
		$link_name = I ( 'post.link_name' );
		if ($link_name) {
			$where ['link_name'] = array ('like', '%' . $link_name . '%' );
		}
		$count = D ( 'Link' )->getCount ( $where );
		$limit = $this->paging ( $count );
		$list = D ( 'Link' )->getList ( $where, 'sort_order desc,id asc', $limit );
		$this->assign ( 'list', $list );
		$this->display ();
	}
	
	/**
	 * 添加友情链接插入操作
	 */
	public function addLink() {
		if (IS_POST && isset ( $_POST ['dosubmit'] )) {
			$postdata = I ( 'post.' );
			if ($_FILES ['link_logo'] ['name']) {
				$load = new AttachController ();
				$uploadList = $load->load ( $_FILES );
				if (! $uploadList) {
					$this->error ( $uploadList );
				} else {
					$load->addAttach ( 'link_logo', $uploadList );
				}
				$link_logo = './Uploads/' . $uploadList ['link_logo'] ['savepath'] . $uploadList ['link_logo'] ['savename'];
			}
			$id = D ( 'Link' )->addLink ( $postdata ['link_name'], $postdata ['link_url'], $link_logo, $postdata ['status'] );
			if ($id > 0) {
				$this->success ( L ( 'success_insert' ), U ( 'index' ) );
			} else {
				$this->error ( $id );
			}
		} else {
			$this->error ( L ( 'error_illegal_operation' ) );
		}
	}
	
	/**
	 * 更新友情链接信息
	 */
	public function updateLink() {
		if (IS_POST && isset ( $_POST ['dosubmit'] )) {
			$postdata = I ( 'post.' );
			if ($_FILES ['link_logo'] ['name']) {
				$load = new AttachController ();
				$uploadList = $load->load ( $_FILES );
				if (! $uploadList) {
					$this->error ( $uploadList );
				} else {
					$load->addAttach ( 'link_logo', $uploadList );
				}
				$postdata ['link_logo'] = './Uploads/' . $uploadList ['link_logo'] ['savepath'] . $uploadList ['link_logo'] ['savename'];
			}
			$result = D ( 'Link' )->editLink ( $postdata ['id'], $postdata );
			if ($result) {
				$this->success ( L ( 'success_edit' ), U ( 'index' ) );
			} else {
				$this->error ( D ( 'Link' )->getError () );
			}
		} else {
			$this->error ( L ( 'error_illegal_operation' ) );
		}
	}
}