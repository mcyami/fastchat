<?php
/**
* 数据库管理控制器
* ===============================================
* @copyright 深圳市顶速网络科技有限公司  http://fasttop.top
* ===============================================
* @author: Ketity <970564173@qq.com>
* @date: 2015-5-19
*/
namespace Admin\Controller;
use Common\Controller\AdminBaseController;
class DatabaseController extends AdminBaseController {
	
	public function _initialize() {
		parent::_initialize ();
	}
}