<?php
/**
 * 后台通用管理控制器
 * ===============================================
 * @copyright 深圳市顶速网络科技有限公司  http://fasttop.top
 * ===============================================
 * @author: Ketity <970564173@qq.com>
 * @date: 2015-5-21
 */
namespace Admin\Controller;
use Common\Controller\AdminBaseController;
class BaseController extends AdminBaseController {
	
	public function _initialize() {
		parent::_initialize ();
		$square = '<span class="label label-sm label-success">' . L ( 'text_show' ) . '</span>'; //显示
		$anti = '<span class="label label-sm label-warning">' . L ( 'text_hidden' ) . '</span>'; //隐藏
		$this->assign ( 'showornot', array (1 => $square, 0 => $anti ) );
		
		$enabled = '<span class="label label-sm label-success">' . L ( 'button_enable' ) . '</span>'; //启用
		$disabled = '<span class="label label-sm label-warning">' . L ( 'button_disable' ) . '</span>'; //禁用
		$this->assign ( 'enableornot', array (1 => $enabled, 0 => $disabled ) );
		//根据访问节点获取菜单信息作为SEO相关信息
		$condition ['url'] = array ('like', '%' . CONTROLLER_NAME . '/' . ACTION_NAME . '%' );
		$current = D ( 'Menu' )->getMenuField ( 'title,description', $condition );
		$this->assign ( 'seo_title', $current ['title'] . '_顶速网络' );
		$this->assign ( 'seo_description', '顶速网络_' . $current ['description'] );
		$this->assign ( 'author', 'FastTop' );
	}
	
	/**
	 * TODO 全局搜索
	 */
	public function globalSearch() {
		$this->error ( "此功能正在开发中！" );
	}
}