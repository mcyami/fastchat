<?php
/**
 * 聊天室管理控制器
 * ===============================================
 * @copyright 深圳市顶速网络科技有限公司  http://fasttop.top
 * ===============================================
 * @author: Ketity <970564173@qq.com>
 * @date: 2015-6-9
 */
namespace Admin\Controller;
class ChatRoomController extends BaseController {
	
	/**
	 * 初始化
	 * @see Admin\Controller.BaseController::_initialize()
	 */
	public function _initialize() {
		parent::_initialize ();
		$where ['status'] = 1;
		$classify = D ( 'Classify' )->getClassify ( 'id,classify_name,sort_order', $where );
		$this->assign ( 'classify', $classify );
		$chat_room_types = array ('1' => L ( 'text_chatroom_chat' ), '2' => L ( 'text_chatroom_live' ), '3' => L ( 'text_chatroom_shout' ) );
		$this->assign ( 'chat_room_types', $chat_room_types );
	}
	
	/**
	 * 聊天室列表
	 */
	public function index() {
		$where = array ();
		$classifyid = I ( 'get.classifyid' );
		if ($classifyid) {
			$where ['classifyid'] = $classifyid;
			$this->assign ( 'classifyid', $classifyid );
		}
		$chat_room_name = I ( 'post.chat_room_name' );
		if ($chat_room_name) {
			$where ['chat_room_name'] = array ('like', '%' . $chat_room_name . '%' );
			$this->assign ( 'chat_room_name', $chat_room_name );
		}
		$count = D ( 'ChatRoom' )->getCount ( $where );
		$limit = $this->paging ( $count );
		$list = D ( 'ChatRoom' )->getList ( $where, 'sort_order desc,id asc', $limit );
		$this->assign ( 'list', $list );
		$this->display ();
	}
	
	/**
	 * 编辑聊天室页面
	 */
	public function editRoom() {
		$id = I ( 'get.id' );
		if (empty ( $id )) {
			$this->error ( L ( 'error_illegal_operation' ) );
		}
		$detail = D ( 'ChatRoom' )->getById ( $id );
		if ($detail ['chat_room_type']) {
			$detail ['chat_room_type'] = explode ( ',', $detail ['chat_room_type'] );
		}
		$this->assign ( $detail );
		$this->display ( 'edit' );
	}
	
	/**
	 * 新增聊天室处理操作
	 */
	public function addRoom() {
		if (IS_POST && isset ( $_POST ['dosubmit'] )) {
			$postdata = I ( 'post.' );
			if ($_FILES ['chat_room_thumb'] ['name']) {
				$load = new AttachController ();
				$uploadList = $load->load ( $_FILES );
				if (! $uploadList) {
					$this->error ( $uploadList );
				} else {
					$load->addAttach ( 'chat_room_thumb', $uploadList );
				}
				$chat_room_thumb = './Uploads/' . $uploadList ['chat_room_thumb'] ['savepath'] . $uploadList ['chat_room_thumb'] ['savename'];
			}
			if ($postdata ['chat_room_type']) {
				$postdata ['chat_room_type'] = implode ( ',', $postdata ['chat_room_type'] );
				$chat_room_type = '1,' . $postdata ['chat_room_type'];
			}
			$detail = array ('chat_room_description' => $postdata ['chat_room_description'], 'chat_room_thumb' => $chat_room_thumb, 'status' => $postdata ['status'], 'chat_room_type' => $chat_room_type );
			$result = D ( 'ChatRoom' )->addRoom ( $postdata ['chat_room_name'], $postdata ['classifyid'], $detail );
			if (is_numeric ( $result )) {
				$this->success ( L ( 'success_insert' ), U ( 'index' ) );
			} else {
				$this->error ( $result );
			}
		} else {
			$this->error ( L ( 'error_illegal_operation' ) );
		}
	}
	
	/**
	 * 更新聊天室信息
	 */
	public function updateRoom() {
		if (IS_POST && isset ( $_POST ['dosubmit'] )) {
			$postdata = I ( 'post.' );
			if ($_FILES ['chat_room_thumb'] ['name']) {
				$load = new AttachController ();
				$uploadList = $load->load ( $_FILES );
				if (! $uploadList) {
					$this->error ( $uploadList );
				} else {
					$load->addAttach ( 'chat_room_thumb', $uploadList );
				}
				$postdata ['chat_room_thumb'] = './Uploads/' . $uploadList ['chat_room_thumb'] ['savepath'] . $uploadList ['chat_room_thumb'] ['savename'];
			}
			if ($postdata ['chat_room_type']) {
				$postdata ['chat_room_type'] = implode ( ',', $postdata ['chat_room_type'] );
				$postdata ['chat_room_type'] = '1,' . $postdata ['chat_room_type'];
			} else {
				$postdata ['chat_room_type'] = 1;
			}
			$result = D ( 'ChatRoom' )->editRoom ( $postdata ['id'], $postdata );
			if (! empty ( $result ) && is_numeric ( $result )) {
				$this->success ( L ( 'success_edit' ), U ( 'index' ) );
			} elseif (is_numeric($result) && $result == 0) {
				$this->error ( L ( 'error_edit_none' ) );
			} else {
				$this->error ( $result );
			}
		} else {
			$this->error ( L ( 'error_illegal_operation' ) );
		}
	}
}