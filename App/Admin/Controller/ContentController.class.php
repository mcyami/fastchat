<?php
/**
 * 内容管理控制器
 * ===============================================
 * @copyright 深圳市顶速网络科技有限公司  http://fasttop.top
 * ===============================================
 * @author: Ketity <970564173@qq.com>
 * @date: 2015-6-9
 */
namespace Admin\Controller;
class ContentController extends BaseController {
	
	public function _initialize() {
		parent::_initialize ();
	}
	
	public function index() {
		$this->display ();
	}

}