/*
Navicat MySQL Data Transfer

Source Server         : connect
Source Server Version : 50528
Source Host           : localhost:3306
Source Database       : chatlive

Target Server Type    : MYSQL
Target Server Version : 50528
File Encoding         : 65001

Date: 2015-06-29 18:48:32
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `ft_area`
-- ----------------------------
DROP TABLE IF EXISTS `ft_area`;
CREATE TABLE `ft_area` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT COMMENT '地区ID',
  `parentid` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT '父级ID',
  `region_name` varchar(100) NOT NULL DEFAULT '' COMMENT '地区名称',
  `region_type` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '地区类型，0：国家  1：省  2：市  3：区',
  `sort_order` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT '排序',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=855 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ft_area
-- ----------------------------
INSERT INTO `ft_area` VALUES ('1', '0', '中国', '0', '0');
INSERT INTO `ft_area` VALUES ('2', '1', '北京市', '1', '0');
INSERT INTO `ft_area` VALUES ('3', '1', '河北省', '1', '0');
INSERT INTO `ft_area` VALUES ('4', '1', '山东省', '1', '0');
INSERT INTO `ft_area` VALUES ('5', '1', '辽宁省', '1', '0');
INSERT INTO `ft_area` VALUES ('6', '1', '黑龙江省', '1', '0');
INSERT INTO `ft_area` VALUES ('7', '1', '吉林省', '1', '0');
INSERT INTO `ft_area` VALUES ('8', '1', '甘肃省', '1', '0');
INSERT INTO `ft_area` VALUES ('9', '1', '青海省', '1', '0');
INSERT INTO `ft_area` VALUES ('10', '1', '河南省', '1', '0');
INSERT INTO `ft_area` VALUES ('11', '1', '江苏省', '1', '0');
INSERT INTO `ft_area` VALUES ('12', '1', '上海市', '1', '0');
INSERT INTO `ft_area` VALUES ('13', '1', '湖北省', '1', '0');
INSERT INTO `ft_area` VALUES ('14', '1', '湖南省', '1', '0');
INSERT INTO `ft_area` VALUES ('15', '1', '江西省', '1', '0');
INSERT INTO `ft_area` VALUES ('16', '1', '浙江省', '1', '0');
INSERT INTO `ft_area` VALUES ('17', '1', '广东省', '1', '0');
INSERT INTO `ft_area` VALUES ('18', '1', '云南省', '1', '0');
INSERT INTO `ft_area` VALUES ('19', '1', '福建省', '1', '0');
INSERT INTO `ft_area` VALUES ('20', '1', '海南省', '1', '0');
INSERT INTO `ft_area` VALUES ('21', '1', '山西省', '1', '0');
INSERT INTO `ft_area` VALUES ('22', '1', '四川省', '1', '0');
INSERT INTO `ft_area` VALUES ('23', '1', '重庆市', '1', '0');
INSERT INTO `ft_area` VALUES ('24', '1', '天津市', '1', '0');
INSERT INTO `ft_area` VALUES ('25', '1', '陕西省', '1', '0');
INSERT INTO `ft_area` VALUES ('26', '1', '贵州省', '1', '0');
INSERT INTO `ft_area` VALUES ('27', '1', '安徽省', '1', '0');
INSERT INTO `ft_area` VALUES ('28', '1', '内蒙古自治区', '1', '0');
INSERT INTO `ft_area` VALUES ('29', '1', '宁夏回族自治区', '1', '0');
INSERT INTO `ft_area` VALUES ('30', '1', '新疆维吾尔自治区', '1', '0');
INSERT INTO `ft_area` VALUES ('31', '1', '西藏自治区', '1', '0');
INSERT INTO `ft_area` VALUES ('32', '1', '广西壮族自治区', '1', '0');
INSERT INTO `ft_area` VALUES ('33', '1', '香港特别行政区', '1', '0');
INSERT INTO `ft_area` VALUES ('34', '1', '澳门特别行政区', '1', '0');
INSERT INTO `ft_area` VALUES ('35', '1', '台湾省', '1', '0');
INSERT INTO `ft_area` VALUES ('36', '2', '东城区', '2', '0');
INSERT INTO `ft_area` VALUES ('37', '2', '西城区', '2', '0');
INSERT INTO `ft_area` VALUES ('38', '2', '朝阳区', '2', '0');
INSERT INTO `ft_area` VALUES ('39', '2', '海淀区', '2', '0');
INSERT INTO `ft_area` VALUES ('40', '2', '丰台区', '2', '0');
INSERT INTO `ft_area` VALUES ('41', '2', '石景山区', '2', '0');
INSERT INTO `ft_area` VALUES ('42', '2', '门头沟区', '2', '0');
INSERT INTO `ft_area` VALUES ('43', '2', '房山区', '2', '0');
INSERT INTO `ft_area` VALUES ('44', '2', '大兴区', '2', '0');
INSERT INTO `ft_area` VALUES ('45', '2', '通州区', '2', '0');
INSERT INTO `ft_area` VALUES ('46', '2', '顺义区', '2', '0');
INSERT INTO `ft_area` VALUES ('47', '2', '昌平区', '2', '0');
INSERT INTO `ft_area` VALUES ('48', '2', '平谷区', '2', '0');
INSERT INTO `ft_area` VALUES ('49', '2', '怀柔区', '2', '0');
INSERT INTO `ft_area` VALUES ('50', '2', '密云县', '2', '0');
INSERT INTO `ft_area` VALUES ('51', '2', '延庆县', '2', '0');
INSERT INTO `ft_area` VALUES ('52', '3', '石家庄市', '2', '0');
INSERT INTO `ft_area` VALUES ('53', '3', '唐山市', '2', '0');
INSERT INTO `ft_area` VALUES ('54', '3', '秦皇岛市', '2', '0');
INSERT INTO `ft_area` VALUES ('55', '3', '邯郸市', '2', '0');
INSERT INTO `ft_area` VALUES ('56', '3', '邢台市', '2', '0');
INSERT INTO `ft_area` VALUES ('57', '3', '保定市', '2', '0');
INSERT INTO `ft_area` VALUES ('58', '3', '张家口市', '2', '0');
INSERT INTO `ft_area` VALUES ('59', '3', '承德市', '2', '0');
INSERT INTO `ft_area` VALUES ('60', '3', '沧州市', '2', '0');
INSERT INTO `ft_area` VALUES ('61', '3', '廊坊市', '2', '0');
INSERT INTO `ft_area` VALUES ('62', '3', '衡水市', '2', '0');
INSERT INTO `ft_area` VALUES ('63', '4', '济南市', '2', '0');
INSERT INTO `ft_area` VALUES ('64', '4', '青岛市', '2', '0');
INSERT INTO `ft_area` VALUES ('65', '4', '淄博市', '2', '0');
INSERT INTO `ft_area` VALUES ('66', '4', '枣庄市', '2', '0');
INSERT INTO `ft_area` VALUES ('67', '4', '东营市', '2', '0');
INSERT INTO `ft_area` VALUES ('68', '4', '烟台市', '2', '0');
INSERT INTO `ft_area` VALUES ('69', '4', '潍坊市', '2', '0');
INSERT INTO `ft_area` VALUES ('70', '4', '济宁市', '2', '0');
INSERT INTO `ft_area` VALUES ('71', '4', '泰安市', '2', '0');
INSERT INTO `ft_area` VALUES ('72', '4', '威海市', '2', '0');
INSERT INTO `ft_area` VALUES ('73', '4', '日照市', '2', '0');
INSERT INTO `ft_area` VALUES ('74', '4', '滨州市', '2', '0');
INSERT INTO `ft_area` VALUES ('75', '4', '德州市', '2', '0');
INSERT INTO `ft_area` VALUES ('76', '4', '聊城市', '2', '0');
INSERT INTO `ft_area` VALUES ('77', '4', '临沂市', '2', '0');
INSERT INTO `ft_area` VALUES ('78', '4', '菏泽市', '2', '0');
INSERT INTO `ft_area` VALUES ('79', '4', '莱芜市', '2', '0');
INSERT INTO `ft_area` VALUES ('80', '5', '沈阳市', '2', '0');
INSERT INTO `ft_area` VALUES ('81', '5', '大连市', '2', '0');
INSERT INTO `ft_area` VALUES ('82', '5', '鞍山市', '2', '0');
INSERT INTO `ft_area` VALUES ('83', '5', '抚顺市', '2', '0');
INSERT INTO `ft_area` VALUES ('84', '5', '本溪市', '2', '0');
INSERT INTO `ft_area` VALUES ('85', '5', '丹东市', '2', '0');
INSERT INTO `ft_area` VALUES ('86', '5', '锦州市', '2', '0');
INSERT INTO `ft_area` VALUES ('87', '5', '营口市', '2', '0');
INSERT INTO `ft_area` VALUES ('88', '5', '阜新市', '2', '0');
INSERT INTO `ft_area` VALUES ('89', '5', '辽阳市', '2', '0');
INSERT INTO `ft_area` VALUES ('90', '5', '盘锦市', '2', '0');
INSERT INTO `ft_area` VALUES ('91', '5', '铁岭市', '2', '0');
INSERT INTO `ft_area` VALUES ('92', '5', '朝阳市', '2', '0');
INSERT INTO `ft_area` VALUES ('93', '5', '葫芦岛', '2', '0');
INSERT INTO `ft_area` VALUES ('94', '6', '哈尔滨市', '2', '0');
INSERT INTO `ft_area` VALUES ('95', '6', '齐齐哈尔市', '2', '0');
INSERT INTO `ft_area` VALUES ('96', '6', '牡丹江市', '2', '0');
INSERT INTO `ft_area` VALUES ('97', '6', '佳木斯市', '2', '0');
INSERT INTO `ft_area` VALUES ('98', '6', '大庆市', '2', '0');
INSERT INTO `ft_area` VALUES ('99', '6', '伊春市', '2', '0');
INSERT INTO `ft_area` VALUES ('100', '6', '鸡西市', '2', '0');
INSERT INTO `ft_area` VALUES ('101', '6', '鹤岗市', '2', '0');
INSERT INTO `ft_area` VALUES ('102', '6', '双鸭山市', '2', '0');
INSERT INTO `ft_area` VALUES ('103', '6', '七台河市', '2', '0');
INSERT INTO `ft_area` VALUES ('104', '6', '绥化市', '2', '0');
INSERT INTO `ft_area` VALUES ('105', '6', '黑河市', '2', '0');
INSERT INTO `ft_area` VALUES ('106', '6', '大兴安岭', '2', '0');
INSERT INTO `ft_area` VALUES ('107', '7', '长春市', '2', '0');
INSERT INTO `ft_area` VALUES ('108', '7', '吉林市', '2', '0');
INSERT INTO `ft_area` VALUES ('109', '7', '四平市', '2', '0');
INSERT INTO `ft_area` VALUES ('110', '7', '辽源市', '2', '0');
INSERT INTO `ft_area` VALUES ('111', '7', '通化市', '2', '0');
INSERT INTO `ft_area` VALUES ('112', '7', '白山市', '2', '0');
INSERT INTO `ft_area` VALUES ('113', '7', '白城市', '2', '0');
INSERT INTO `ft_area` VALUES ('114', '7', '松原市', '2', '0');
INSERT INTO `ft_area` VALUES ('115', '7', '延边朝鲜族自治州', '2', '0');
INSERT INTO `ft_area` VALUES ('116', '7', '长白山管委会', '2', '0');
INSERT INTO `ft_area` VALUES ('117', '7', ' 梅河口', '2', '0');
INSERT INTO `ft_area` VALUES ('118', '7', '公主岭', '2', '0');
INSERT INTO `ft_area` VALUES ('119', '8', '兰州市', '2', '0');
INSERT INTO `ft_area` VALUES ('120', '8', '酒泉市', '2', '0');
INSERT INTO `ft_area` VALUES ('121', '8', '金昌市', '2', '0');
INSERT INTO `ft_area` VALUES ('122', '8', '天水市', '2', '0');
INSERT INTO `ft_area` VALUES ('123', '8', '嘉峪关', '2', '0');
INSERT INTO `ft_area` VALUES ('124', '8', '武威市', '2', '0');
INSERT INTO `ft_area` VALUES ('125', '8', '张掖市', '2', '0');
INSERT INTO `ft_area` VALUES ('126', '8', '白银市', '2', '0');
INSERT INTO `ft_area` VALUES ('127', '8', '平凉市', '2', '0');
INSERT INTO `ft_area` VALUES ('128', '8', '庆阳市', '2', '0');
INSERT INTO `ft_area` VALUES ('129', '8', '定西市', '2', '0');
INSERT INTO `ft_area` VALUES ('130', '8', '陇南市', '2', '0');
INSERT INTO `ft_area` VALUES ('131', '8', '临夏', '2', '0');
INSERT INTO `ft_area` VALUES ('132', '8', '甘南', '2', '0');
INSERT INTO `ft_area` VALUES ('133', '9', '西宁市', '2', '0');
INSERT INTO `ft_area` VALUES ('134', '9', '海东市', '2', '0');
INSERT INTO `ft_area` VALUES ('135', '9', '海北藏族自治州', '2', '0');
INSERT INTO `ft_area` VALUES ('136', '9', '黄南藏族自治州', '2', '0');
INSERT INTO `ft_area` VALUES ('137', '9', '海南藏族自治州', '2', '0');
INSERT INTO `ft_area` VALUES ('138', '9', '果洛藏族自治州', '2', '0');
INSERT INTO `ft_area` VALUES ('139', '9', '玉树藏族自治州', '2', '0');
INSERT INTO `ft_area` VALUES ('140', '9', '海西蒙古族藏族自治州', '2', '0');
INSERT INTO `ft_area` VALUES ('141', '10', '郑州市', '2', '0');
INSERT INTO `ft_area` VALUES ('142', '10', '开封市', '2', '0');
INSERT INTO `ft_area` VALUES ('143', '10', '洛阳市', '2', '0');
INSERT INTO `ft_area` VALUES ('144', '10', '平顶山市', '2', '0');
INSERT INTO `ft_area` VALUES ('145', '10', '安阳市', '2', '0');
INSERT INTO `ft_area` VALUES ('146', '10', '鹤壁市', '2', '0');
INSERT INTO `ft_area` VALUES ('147', '10', '新乡市', '2', '0');
INSERT INTO `ft_area` VALUES ('148', '10', '焦作市', '2', '0');
INSERT INTO `ft_area` VALUES ('149', '10', '濮阳市', '2', '0');
INSERT INTO `ft_area` VALUES ('150', '10', '许昌市', '2', '0');
INSERT INTO `ft_area` VALUES ('151', '10', '漯河市', '2', '0');
INSERT INTO `ft_area` VALUES ('152', '10', '三门峡', '2', '0');
INSERT INTO `ft_area` VALUES ('153', '10', '商丘市', '2', '0');
INSERT INTO `ft_area` VALUES ('154', '10', '周口市', '2', '0');
INSERT INTO `ft_area` VALUES ('155', '10', '驻马店', '2', '0');
INSERT INTO `ft_area` VALUES ('156', '10', '南阳市', '2', '0');
INSERT INTO `ft_area` VALUES ('157', '10', '信阳市', '2', '0');
INSERT INTO `ft_area` VALUES ('158', '10', '济源市', '2', '0');
INSERT INTO `ft_area` VALUES ('159', '11', '南京市', '2', '0');
INSERT INTO `ft_area` VALUES ('160', '11', '无锡市', '2', '0');
INSERT INTO `ft_area` VALUES ('161', '11', '徐州市', '2', '0');
INSERT INTO `ft_area` VALUES ('162', '11', '常州市', '2', '0');
INSERT INTO `ft_area` VALUES ('163', '11', '苏州市', '2', '0');
INSERT INTO `ft_area` VALUES ('164', '11', '南通市', '2', '0');
INSERT INTO `ft_area` VALUES ('165', '11', '连云港', '2', '0');
INSERT INTO `ft_area` VALUES ('166', '11', '淮安市', '2', '0');
INSERT INTO `ft_area` VALUES ('167', '11', '盐城市', '2', '0');
INSERT INTO `ft_area` VALUES ('168', '11', ' 扬州市', '2', '0');
INSERT INTO `ft_area` VALUES ('169', '11', '镇江市', '2', '0');
INSERT INTO `ft_area` VALUES ('170', '11', '泰州市', '2', '0');
INSERT INTO `ft_area` VALUES ('171', '11', '宿迁市', '2', '0');
INSERT INTO `ft_area` VALUES ('172', '12', '黄浦区', '2', '0');
INSERT INTO `ft_area` VALUES ('173', '12', '浦东新区', '2', '0');
INSERT INTO `ft_area` VALUES ('174', '12', '徐汇区', '2', '0');
INSERT INTO `ft_area` VALUES ('175', '12', '长宁区', '2', '0');
INSERT INTO `ft_area` VALUES ('176', '12', '静安区', '2', '0');
INSERT INTO `ft_area` VALUES ('177', '12', '普陀区', '2', '0');
INSERT INTO `ft_area` VALUES ('178', '12', '闸北区', '2', '0');
INSERT INTO `ft_area` VALUES ('179', '12', '虹口区', '2', '0');
INSERT INTO `ft_area` VALUES ('180', '12', '杨浦区', '2', '0');
INSERT INTO `ft_area` VALUES ('181', '12', '闵行区', '2', '0');
INSERT INTO `ft_area` VALUES ('182', '12', '宝山区', '2', '0');
INSERT INTO `ft_area` VALUES ('183', '12', '嘉定区', '2', '0');
INSERT INTO `ft_area` VALUES ('184', '12', '金山区', '2', '0');
INSERT INTO `ft_area` VALUES ('185', '12', '松江区', '2', '0');
INSERT INTO `ft_area` VALUES ('186', '12', '青浦区', '2', '0');
INSERT INTO `ft_area` VALUES ('187', '12', '奉贤区', '2', '0');
INSERT INTO `ft_area` VALUES ('188', '12', '崇明县', '2', '0');
INSERT INTO `ft_area` VALUES ('189', '13', '武汉市', '2', '0');
INSERT INTO `ft_area` VALUES ('190', '13', '黄石市', '2', '0');
INSERT INTO `ft_area` VALUES ('191', '13', '十堰市', '2', '0');
INSERT INTO `ft_area` VALUES ('192', '13', '荆州市', '2', '0');
INSERT INTO `ft_area` VALUES ('193', '13', '宜昌市', '2', '0');
INSERT INTO `ft_area` VALUES ('194', '13', '襄阳市', '2', '0');
INSERT INTO `ft_area` VALUES ('195', '13', '鄂州市', '2', '0');
INSERT INTO `ft_area` VALUES ('196', '13', '荆门市', '2', '0');
INSERT INTO `ft_area` VALUES ('197', '13', '黄冈市', '2', '0');
INSERT INTO `ft_area` VALUES ('198', '13', '孝感市', '2', '0');
INSERT INTO `ft_area` VALUES ('199', '13', '咸宁市', '2', '0');
INSERT INTO `ft_area` VALUES ('200', '13', '仙桃市', '2', '0');
INSERT INTO `ft_area` VALUES ('201', '13', '潜江市', '2', '0');
INSERT INTO `ft_area` VALUES ('202', '13', '神农架', '2', '0');
INSERT INTO `ft_area` VALUES ('203', '13', '恩施市', '2', '0');
INSERT INTO `ft_area` VALUES ('204', '13', '天门市', '2', '0');
INSERT INTO `ft_area` VALUES ('205', '13', '随州市', '2', '0');
INSERT INTO `ft_area` VALUES ('206', '14', '长沙市', '2', '0');
INSERT INTO `ft_area` VALUES ('207', '14', '株洲市', '2', '0');
INSERT INTO `ft_area` VALUES ('208', '14', '湘潭市', '2', '0');
INSERT INTO `ft_area` VALUES ('209', '14', '衡阳市', '2', '0');
INSERT INTO `ft_area` VALUES ('210', '14', '邵阳市', '2', '0');
INSERT INTO `ft_area` VALUES ('211', '14', '岳阳市', '2', '0');
INSERT INTO `ft_area` VALUES ('212', '14', '常德市', '2', '0');
INSERT INTO `ft_area` VALUES ('213', '14', '张家界市', '2', '0');
INSERT INTO `ft_area` VALUES ('214', '14', '益阳市', '2', '0');
INSERT INTO `ft_area` VALUES ('215', '14', '娄底市', '2', '0');
INSERT INTO `ft_area` VALUES ('216', '14', '郴州市', '2', '0');
INSERT INTO `ft_area` VALUES ('217', '14', '永州市', '2', '0');
INSERT INTO `ft_area` VALUES ('218', '14', '怀化市', '2', '0');
INSERT INTO `ft_area` VALUES ('219', '14', '湘西土家族苗族自治州', '2', '0');
INSERT INTO `ft_area` VALUES ('220', '15', '南昌市', '2', '0');
INSERT INTO `ft_area` VALUES ('221', '15', '九江市', '2', '0');
INSERT INTO `ft_area` VALUES ('222', '15', '上饶市', '2', '0');
INSERT INTO `ft_area` VALUES ('223', '15', '抚州市', '2', '0');
INSERT INTO `ft_area` VALUES ('224', '15', '宜春市', '2', '0');
INSERT INTO `ft_area` VALUES ('225', '15', '吉安市', '2', '0');
INSERT INTO `ft_area` VALUES ('226', '15', '赣州市', '2', '0');
INSERT INTO `ft_area` VALUES ('227', '15', '景德镇市', '2', '0');
INSERT INTO `ft_area` VALUES ('228', '15', '萍乡市', '2', '0');
INSERT INTO `ft_area` VALUES ('229', '15', '新余市', '2', '0');
INSERT INTO `ft_area` VALUES ('230', '15', '鹰潭市', '2', '0');
INSERT INTO `ft_area` VALUES ('231', '16', '杭州市', '2', '0');
INSERT INTO `ft_area` VALUES ('232', '16', '宁波市', '2', '0');
INSERT INTO `ft_area` VALUES ('233', '16', '温州市', '2', '0');
INSERT INTO `ft_area` VALUES ('234', '16', '绍兴市', '2', '0');
INSERT INTO `ft_area` VALUES ('235', '16', '湖州市', '2', '0');
INSERT INTO `ft_area` VALUES ('236', '16', '嘉兴市', '2', '0');
INSERT INTO `ft_area` VALUES ('237', '16', '金华市', '2', '0');
INSERT INTO `ft_area` VALUES ('238', '16', '衢州市', '2', '0');
INSERT INTO `ft_area` VALUES ('239', '16', '舟山市', '2', '0');
INSERT INTO `ft_area` VALUES ('240', '16', '台州市', '2', '0');
INSERT INTO `ft_area` VALUES ('241', '16', '丽水市', '2', '0');
INSERT INTO `ft_area` VALUES ('242', '17', '广州市', '2', '0');
INSERT INTO `ft_area` VALUES ('243', '17', '深圳市', '2', '0');
INSERT INTO `ft_area` VALUES ('244', '17', '珠海市', '2', '0');
INSERT INTO `ft_area` VALUES ('245', '17', '汕头市', '2', '0');
INSERT INTO `ft_area` VALUES ('246', '17', '佛山市', '2', '0');
INSERT INTO `ft_area` VALUES ('247', '17', '韶关市', '2', '0');
INSERT INTO `ft_area` VALUES ('248', '17', '湛江市', '2', '0');
INSERT INTO `ft_area` VALUES ('249', '17', '肇庆市', '2', '0');
INSERT INTO `ft_area` VALUES ('250', '17', '江门市', '2', '0');
INSERT INTO `ft_area` VALUES ('251', '17', '茂名市', '2', '0');
INSERT INTO `ft_area` VALUES ('252', '17', '惠州市', '2', '0');
INSERT INTO `ft_area` VALUES ('253', '17', ' 梅州市', '2', '0');
INSERT INTO `ft_area` VALUES ('254', '17', '汕尾市', '2', '0');
INSERT INTO `ft_area` VALUES ('255', '17', '河源市', '2', '0');
INSERT INTO `ft_area` VALUES ('256', '17', '阳江市', '2', '0');
INSERT INTO `ft_area` VALUES ('257', '17', '清远市', '2', '0');
INSERT INTO `ft_area` VALUES ('258', '17', '东莞市', '2', '0');
INSERT INTO `ft_area` VALUES ('259', '17', '中山市', '2', '0');
INSERT INTO `ft_area` VALUES ('260', '17', '潮州市', '2', '0');
INSERT INTO `ft_area` VALUES ('261', '17', '揭阳市', '2', '0');
INSERT INTO `ft_area` VALUES ('262', '17', '云浮市', '2', '0');
INSERT INTO `ft_area` VALUES ('263', '18', '昆明市', '2', '0');
INSERT INTO `ft_area` VALUES ('264', '18', '曲靖市', '2', '0');
INSERT INTO `ft_area` VALUES ('265', '18', '玉溪市', '2', '0');
INSERT INTO `ft_area` VALUES ('266', '18', '保山市', '2', '0');
INSERT INTO `ft_area` VALUES ('267', '18', '昭通市', '2', '0');
INSERT INTO `ft_area` VALUES ('268', '18', '丽江市', '2', '0');
INSERT INTO `ft_area` VALUES ('269', '18', '普洱市', '2', '0');
INSERT INTO `ft_area` VALUES ('270', '18', '临沧市', '2', '0');
INSERT INTO `ft_area` VALUES ('271', '18', '德宏傣族景颇族自治州', '2', '0');
INSERT INTO `ft_area` VALUES ('272', '18', '怒江僳僳族自治州', '2', '0');
INSERT INTO `ft_area` VALUES ('273', '18', '迪庆藏族自治州', '2', '0');
INSERT INTO `ft_area` VALUES ('274', '18', '大理白族自治州', '2', '0');
INSERT INTO `ft_area` VALUES ('275', '18', '楚雄彝族自治州', '2', '0');
INSERT INTO `ft_area` VALUES ('276', '18', '红河哈尼族彝族自治州', '2', '0');
INSERT INTO `ft_area` VALUES ('277', '18', '文山壮族苗族自治州', '2', '0');
INSERT INTO `ft_area` VALUES ('278', '18', '西双版纳傣族自治州', '2', '0');
INSERT INTO `ft_area` VALUES ('279', '19', '福州市', '2', '0');
INSERT INTO `ft_area` VALUES ('280', '19', '厦门市', '2', '0');
INSERT INTO `ft_area` VALUES ('281', '19', '漳州市', '2', '0');
INSERT INTO `ft_area` VALUES ('282', '19', '泉州市', '2', '0');
INSERT INTO `ft_area` VALUES ('283', '19', '三明市', '2', '0');
INSERT INTO `ft_area` VALUES ('284', '19', '莆田市', '2', '0');
INSERT INTO `ft_area` VALUES ('285', '19', '南平市', '2', '0');
INSERT INTO `ft_area` VALUES ('286', '19', '龙岩市', '2', '0');
INSERT INTO `ft_area` VALUES ('287', '19', '宁德市', '2', '0');
INSERT INTO `ft_area` VALUES ('288', '19', ' 平潭市', '2', '0');
INSERT INTO `ft_area` VALUES ('289', '20', '海口市', '2', '0');
INSERT INTO `ft_area` VALUES ('290', '20', '三亚市', '2', '0');
INSERT INTO `ft_area` VALUES ('291', '20', '三沙市', '2', '0');
INSERT INTO `ft_area` VALUES ('292', '20', '儋州市', '2', '0');
INSERT INTO `ft_area` VALUES ('293', '20', '五指山市', '2', '0');
INSERT INTO `ft_area` VALUES ('294', '20', '文昌市', '2', '0');
INSERT INTO `ft_area` VALUES ('295', '20', '琼海市', '2', '0');
INSERT INTO `ft_area` VALUES ('296', '20', '万宁市', '2', '0');
INSERT INTO `ft_area` VALUES ('297', '20', '东方市', '2', '0');
INSERT INTO `ft_area` VALUES ('298', '21', '太原市', '2', '0');
INSERT INTO `ft_area` VALUES ('299', '21', '大同市', '2', '0');
INSERT INTO `ft_area` VALUES ('300', '21', '阳泉市', '2', '0');
INSERT INTO `ft_area` VALUES ('301', '21', '长治市', '2', '0');
INSERT INTO `ft_area` VALUES ('302', '21', '晋城市', '2', '0');
INSERT INTO `ft_area` VALUES ('303', '21', ' 朔州市', '2', '0');
INSERT INTO `ft_area` VALUES ('304', '21', '晋中市', '2', '0');
INSERT INTO `ft_area` VALUES ('305', '21', '运城市', '2', '0');
INSERT INTO `ft_area` VALUES ('306', '21', ' 忻州市', '2', '0');
INSERT INTO `ft_area` VALUES ('307', '21', '临汾市', '2', '0');
INSERT INTO `ft_area` VALUES ('308', '21', '吕梁市', '2', '0');
INSERT INTO `ft_area` VALUES ('309', '22', '成都市', '2', '0');
INSERT INTO `ft_area` VALUES ('310', '22', '绵阳市', '2', '0');
INSERT INTO `ft_area` VALUES ('311', '22', '自贡市', '2', '0');
INSERT INTO `ft_area` VALUES ('312', '22', '攀枝花市', '2', '0');
INSERT INTO `ft_area` VALUES ('313', '22', '泸州市', '2', '0');
INSERT INTO `ft_area` VALUES ('314', '22', '德阳市', '2', '0');
INSERT INTO `ft_area` VALUES ('315', '22', '广元市', '2', '0');
INSERT INTO `ft_area` VALUES ('316', '22', '遂宁市', '2', '0');
INSERT INTO `ft_area` VALUES ('317', '22', '内江市', '2', '0');
INSERT INTO `ft_area` VALUES ('318', '22', '乐山市', '2', '0');
INSERT INTO `ft_area` VALUES ('319', '22', '资阳市', '2', '0');
INSERT INTO `ft_area` VALUES ('320', '22', '宜宾市', '2', '0');
INSERT INTO `ft_area` VALUES ('321', '22', '南充市', '2', '0');
INSERT INTO `ft_area` VALUES ('322', '22', '达州市', '2', '0');
INSERT INTO `ft_area` VALUES ('323', '22', '阿坝藏族羌族自治州', '2', '0');
INSERT INTO `ft_area` VALUES ('324', '22', '甘孜藏族自治州', '2', '0');
INSERT INTO `ft_area` VALUES ('325', '22', '凉山彝族自治州', '2', '0');
INSERT INTO `ft_area` VALUES ('326', '22', '广安市', '2', '0');
INSERT INTO `ft_area` VALUES ('327', '22', '巴中市', '2', '0');
INSERT INTO `ft_area` VALUES ('328', '22', '眉山市', '2', '0');
INSERT INTO `ft_area` VALUES ('329', '23', '渝中区', '2', '0');
INSERT INTO `ft_area` VALUES ('330', '23', '大渡口区', '2', '0');
INSERT INTO `ft_area` VALUES ('331', '23', '江北区', '2', '0');
INSERT INTO `ft_area` VALUES ('332', '23', '沙坪坝区', '2', '0');
INSERT INTO `ft_area` VALUES ('333', '23', '九龙坡区', '2', '0');
INSERT INTO `ft_area` VALUES ('334', '23', '南岸区', '2', '0');
INSERT INTO `ft_area` VALUES ('335', '23', '北碚区', '2', '0');
INSERT INTO `ft_area` VALUES ('336', '23', '渝北区', '2', '0');
INSERT INTO `ft_area` VALUES ('337', '23', '巴南区', '2', '0');
INSERT INTO `ft_area` VALUES ('338', '23', '涪陵区', '2', '0');
INSERT INTO `ft_area` VALUES ('339', '23', '綦江区', '2', '0');
INSERT INTO `ft_area` VALUES ('340', '23', '大足区', '2', '0');
INSERT INTO `ft_area` VALUES ('341', '23', '长寿区', '2', '0');
INSERT INTO `ft_area` VALUES ('342', '23', '江津区', '2', '0');
INSERT INTO `ft_area` VALUES ('343', '23', '合川区', '2', '0');
INSERT INTO `ft_area` VALUES ('344', '23', '永川区', '2', '0');
INSERT INTO `ft_area` VALUES ('345', '23', '南川区', '2', '0');
INSERT INTO `ft_area` VALUES ('346', '23', '铜梁区', '2', '0');
INSERT INTO `ft_area` VALUES ('347', '23', '璧山区', '2', '0');
INSERT INTO `ft_area` VALUES ('348', '23', '潼南区', '2', '0');
INSERT INTO `ft_area` VALUES ('349', '23', '荣昌区', '2', '0');
INSERT INTO `ft_area` VALUES ('350', '23', '万州区', '2', '0');
INSERT INTO `ft_area` VALUES ('351', '23', '梁平县', '2', '0');
INSERT INTO `ft_area` VALUES ('352', '23', '城口县', '2', '0');
INSERT INTO `ft_area` VALUES ('353', '23', '丰都县', '2', '0');
INSERT INTO `ft_area` VALUES ('354', '23', '垫江县', '2', '0');
INSERT INTO `ft_area` VALUES ('355', '23', '忠县', '2', '0');
INSERT INTO `ft_area` VALUES ('356', '23', '开县', '2', '0');
INSERT INTO `ft_area` VALUES ('357', '23', '云阳县', '2', '0');
INSERT INTO `ft_area` VALUES ('358', '23', '奉节县', '2', '0');
INSERT INTO `ft_area` VALUES ('359', '23', '巫山县', '2', '0');
INSERT INTO `ft_area` VALUES ('360', '23', '巫溪县', '2', '0');
INSERT INTO `ft_area` VALUES ('361', '23', '黔江区', '2', '0');
INSERT INTO `ft_area` VALUES ('362', '23', '武隆县', '2', '0');
INSERT INTO `ft_area` VALUES ('363', '23', '石柱土家族自治县', '2', '0');
INSERT INTO `ft_area` VALUES ('364', '23', '秀山土家族苗族自治县', '2', '0');
INSERT INTO `ft_area` VALUES ('365', '23', '酉阳土家族苗族自治县', '2', '0');
INSERT INTO `ft_area` VALUES ('366', '23', '彭水苗族土家族自治县', '2', '0');
INSERT INTO `ft_area` VALUES ('367', '24', '和平区', '2', '0');
INSERT INTO `ft_area` VALUES ('368', '24', '河西区', '2', '0');
INSERT INTO `ft_area` VALUES ('369', '24', '南开区', '2', '0');
INSERT INTO `ft_area` VALUES ('370', '24', '河东区', '2', '0');
INSERT INTO `ft_area` VALUES ('371', '24', '河北区', '2', '0');
INSERT INTO `ft_area` VALUES ('372', '24', '红桥区', '2', '0');
INSERT INTO `ft_area` VALUES ('373', '24', '东丽区', '2', '0');
INSERT INTO `ft_area` VALUES ('374', '24', '津南区', '2', '0');
INSERT INTO `ft_area` VALUES ('375', '24', '西青区', '2', '0');
INSERT INTO `ft_area` VALUES ('376', '24', '北辰区', '2', '0');
INSERT INTO `ft_area` VALUES ('377', '24', '滨海新区', '2', '0');
INSERT INTO `ft_area` VALUES ('378', '24', '武清区', '2', '0');
INSERT INTO `ft_area` VALUES ('379', '24', '宝坻区', '2', '0');
INSERT INTO `ft_area` VALUES ('380', '24', '蓟县', '2', '0');
INSERT INTO `ft_area` VALUES ('381', '24', '宁河县', '2', '0');
INSERT INTO `ft_area` VALUES ('382', '24', '静海县', '2', '0');
INSERT INTO `ft_area` VALUES ('383', '25', '西安市', '2', '0');
INSERT INTO `ft_area` VALUES ('384', '25', '宝鸡市', '2', '0');
INSERT INTO `ft_area` VALUES ('385', '25', '咸阳市', '2', '0');
INSERT INTO `ft_area` VALUES ('386', '25', '渭南市', '2', '0');
INSERT INTO `ft_area` VALUES ('387', '25', '铜川市', '2', '0');
INSERT INTO `ft_area` VALUES ('388', '25', '延安市', '2', '0');
INSERT INTO `ft_area` VALUES ('389', '25', '榆林市', '2', '0');
INSERT INTO `ft_area` VALUES ('390', '25', '安康市', '2', '0');
INSERT INTO `ft_area` VALUES ('391', '25', '汉中市', '2', '0');
INSERT INTO `ft_area` VALUES ('392', '25', '商洛市', '2', '0');
INSERT INTO `ft_area` VALUES ('393', '25', '杨凌示范区', '2', '0');
INSERT INTO `ft_area` VALUES ('394', '26', '贵阳市', '2', '0');
INSERT INTO `ft_area` VALUES ('395', '26', '六盘水市', '2', '0');
INSERT INTO `ft_area` VALUES ('396', '26', '遵义市', '2', '0');
INSERT INTO `ft_area` VALUES ('397', '26', '铜仁市', '2', '0');
INSERT INTO `ft_area` VALUES ('398', '26', '黔西南布依族苗族自治州', '2', '0');
INSERT INTO `ft_area` VALUES ('399', '26', '毕节市', '2', '0');
INSERT INTO `ft_area` VALUES ('400', '26', '安顺市', '2', '0');
INSERT INTO `ft_area` VALUES ('401', '26', '黔东南苗族侗族自治州', '2', '0');
INSERT INTO `ft_area` VALUES ('402', '26', '黔南布依族苗族自治州', '2', '0');
INSERT INTO `ft_area` VALUES ('403', '27', '合肥市', '2', '0');
INSERT INTO `ft_area` VALUES ('404', '27', '芜湖市', '2', '0');
INSERT INTO `ft_area` VALUES ('405', '27', '安庆市', '2', '0');
INSERT INTO `ft_area` VALUES ('406', '27', '马鞍山市', '2', '0');
INSERT INTO `ft_area` VALUES ('407', '27', '滁州市', '2', '0');
INSERT INTO `ft_area` VALUES ('408', '27', '阜阳市', '2', '0');
INSERT INTO `ft_area` VALUES ('409', '27', '宿州市', '2', '0');
INSERT INTO `ft_area` VALUES ('410', '27', '蚌埠市', '2', '0');
INSERT INTO `ft_area` VALUES ('411', '27', '六安市', '2', '0');
INSERT INTO `ft_area` VALUES ('412', '27', '宣城市', '2', '0');
INSERT INTO `ft_area` VALUES ('413', '27', '亳州市', '2', '0');
INSERT INTO `ft_area` VALUES ('414', '27', '淮南市', '2', '0');
INSERT INTO `ft_area` VALUES ('415', '27', '淮北市', '2', '0');
INSERT INTO `ft_area` VALUES ('416', '27', '铜陵市', '2', '0');
INSERT INTO `ft_area` VALUES ('417', '27', '黄山市', '2', '0');
INSERT INTO `ft_area` VALUES ('418', '27', '池州市', '2', '0');
INSERT INTO `ft_area` VALUES ('419', '28', '呼和浩特市', '2', '0');
INSERT INTO `ft_area` VALUES ('420', '28', '包头市', '2', '0');
INSERT INTO `ft_area` VALUES ('421', '28', '乌海市', '2', '0');
INSERT INTO `ft_area` VALUES ('422', '28', '赤峰市', '2', '0');
INSERT INTO `ft_area` VALUES ('423', '28', '通辽市', '2', '0');
INSERT INTO `ft_area` VALUES ('424', '28', '鄂尔多斯市', '2', '0');
INSERT INTO `ft_area` VALUES ('425', '28', '呼伦贝尔市', '2', '0');
INSERT INTO `ft_area` VALUES ('426', '28', '巴彦淖尔市', '2', '0');
INSERT INTO `ft_area` VALUES ('427', '28', '乌兰察布市', '2', '0');
INSERT INTO `ft_area` VALUES ('428', '28', '兴安盟', '2', '0');
INSERT INTO `ft_area` VALUES ('429', '28', '锡林郭勒盟', '2', '0');
INSERT INTO `ft_area` VALUES ('430', '28', '阿拉善盟', '2', '0');
INSERT INTO `ft_area` VALUES ('431', '29', '银川市', '2', '0');
INSERT INTO `ft_area` VALUES ('432', '29', '石嘴山市', '2', '0');
INSERT INTO `ft_area` VALUES ('433', '29', '吴忠市', '2', '0');
INSERT INTO `ft_area` VALUES ('434', '29', '固原市', '2', '0');
INSERT INTO `ft_area` VALUES ('435', '29', '中卫市', '2', '0');
INSERT INTO `ft_area` VALUES ('436', '30', '乌鲁木齐市', '2', '0');
INSERT INTO `ft_area` VALUES ('437', '30', '克拉玛依市', '2', '0');
INSERT INTO `ft_area` VALUES ('438', '30', '吐鲁番市', '2', '0');
INSERT INTO `ft_area` VALUES ('439', '30', '哈密地区', '2', '0');
INSERT INTO `ft_area` VALUES ('440', '30', '昌吉回族自治州', '2', '0');
INSERT INTO `ft_area` VALUES ('441', '30', '博尔塔拉蒙古自治州', '2', '0');
INSERT INTO `ft_area` VALUES ('442', '30', '巴音郭楞蒙古自治州', '2', '0');
INSERT INTO `ft_area` VALUES ('443', '30', '阿克苏地区', '2', '0');
INSERT INTO `ft_area` VALUES ('444', '30', '克孜勒苏柯尔克孜自治州', '2', '0');
INSERT INTO `ft_area` VALUES ('445', '30', '喀什地区', '2', '0');
INSERT INTO `ft_area` VALUES ('446', '30', '和田地区', '2', '0');
INSERT INTO `ft_area` VALUES ('447', '30', '伊犁哈萨克自治州', '2', '0');
INSERT INTO `ft_area` VALUES ('448', '30', '塔城地区', '2', '0');
INSERT INTO `ft_area` VALUES ('449', '30', '阿勒泰地区', '2', '0');
INSERT INTO `ft_area` VALUES ('450', '30', '石河子市', '2', '0');
INSERT INTO `ft_area` VALUES ('451', '30', '阿拉尔市', '2', '0');
INSERT INTO `ft_area` VALUES ('452', '30', '图木舒克市', '2', '0');
INSERT INTO `ft_area` VALUES ('453', '30', '五家渠市', '2', '0');
INSERT INTO `ft_area` VALUES ('454', '30', '北屯市', '2', '0');
INSERT INTO `ft_area` VALUES ('455', '30', '铁门关市', '2', '0');
INSERT INTO `ft_area` VALUES ('456', '30', '双河市', '2', '0');
INSERT INTO `ft_area` VALUES ('457', '30', '可克达拉市', '2', '0');
INSERT INTO `ft_area` VALUES ('458', '31', '拉萨市', '2', '0');
INSERT INTO `ft_area` VALUES ('459', '31', '昌都市', '2', '0');
INSERT INTO `ft_area` VALUES ('460', '31', '日喀则市', '2', '0');
INSERT INTO `ft_area` VALUES ('461', '31', '林芝市', '2', '0');
INSERT INTO `ft_area` VALUES ('462', '31', '山南地区', '2', '0');
INSERT INTO `ft_area` VALUES ('463', '31', '那曲地区', '2', '0');
INSERT INTO `ft_area` VALUES ('464', '31', '阿里地区', '2', '0');
INSERT INTO `ft_area` VALUES ('465', '32', '南宁市', '2', '0');
INSERT INTO `ft_area` VALUES ('466', '32', '柳州市', '2', '0');
INSERT INTO `ft_area` VALUES ('467', '32', '桂林市', '2', '0');
INSERT INTO `ft_area` VALUES ('468', '32', '梧州市', '2', '0');
INSERT INTO `ft_area` VALUES ('469', '32', '北海市', '2', '0');
INSERT INTO `ft_area` VALUES ('470', '32', '防城港市', '2', '0');
INSERT INTO `ft_area` VALUES ('471', '32', '钦州市', '2', '0');
INSERT INTO `ft_area` VALUES ('472', '32', '贵港市', '2', '0');
INSERT INTO `ft_area` VALUES ('473', '32', '玉林市', '2', '0');
INSERT INTO `ft_area` VALUES ('474', '32', '百色市', '2', '0');
INSERT INTO `ft_area` VALUES ('475', '32', '贺州市', '2', '0');
INSERT INTO `ft_area` VALUES ('476', '32', '河池市', '2', '0');
INSERT INTO `ft_area` VALUES ('477', '32', '来宾市', '2', '0');
INSERT INTO `ft_area` VALUES ('478', '32', '崇左市', '2', '0');
INSERT INTO `ft_area` VALUES ('479', '33', '香港岛', '2', '0');
INSERT INTO `ft_area` VALUES ('480', '33', '九龙半岛', '2', '0');
INSERT INTO `ft_area` VALUES ('481', '33', '新界', '2', '0');
INSERT INTO `ft_area` VALUES ('482', '34', '花地玛堂区', '2', '0');
INSERT INTO `ft_area` VALUES ('483', '34', '圣安多尼堂区', '2', '0');
INSERT INTO `ft_area` VALUES ('484', '34', '大堂区', '2', '0');
INSERT INTO `ft_area` VALUES ('485', '34', '望德堂区', '2', '0');
INSERT INTO `ft_area` VALUES ('486', '34', '风顺堂区', '2', '0');
INSERT INTO `ft_area` VALUES ('487', '34', '嘉模堂区', '2', '0');
INSERT INTO `ft_area` VALUES ('488', '34', '圣方济各堂区', '2', '0');
INSERT INTO `ft_area` VALUES ('489', '34', '路氹城', '2', '0');
INSERT INTO `ft_area` VALUES ('490', '35', '台北市', '2', '0');
INSERT INTO `ft_area` VALUES ('491', '35', '新北市', '2', '0');
INSERT INTO `ft_area` VALUES ('492', '35', '桃园市', '2', '0');
INSERT INTO `ft_area` VALUES ('493', '35', '台中市', '2', '0');
INSERT INTO `ft_area` VALUES ('494', '35', '台南市', '2', '0');
INSERT INTO `ft_area` VALUES ('495', '35', '高雄市', '2', '0');
INSERT INTO `ft_area` VALUES ('496', '52', '长安区', '3', '0');
INSERT INTO `ft_area` VALUES ('497', '52', '桥西区', '3', '0');
INSERT INTO `ft_area` VALUES ('498', '52', '新华区', '3', '0');
INSERT INTO `ft_area` VALUES ('499', '52', '井陉矿区', '3', '0');
INSERT INTO `ft_area` VALUES ('500', '52', '裕华区', '3', '0');
INSERT INTO `ft_area` VALUES ('501', '52', '藁城区', '3', '0');
INSERT INTO `ft_area` VALUES ('502', '52', '鹿泉区', '3', '0');
INSERT INTO `ft_area` VALUES ('503', '52', '栾城区', '3', '0');
INSERT INTO `ft_area` VALUES ('504', '52', '井陉县', '3', '0');
INSERT INTO `ft_area` VALUES ('505', '52', '正定县', '3', '0');
INSERT INTO `ft_area` VALUES ('506', '52', '行唐县', '3', '0');
INSERT INTO `ft_area` VALUES ('507', '52', '灵寿县', '3', '0');
INSERT INTO `ft_area` VALUES ('508', '52', '高邑县', '3', '0');
INSERT INTO `ft_area` VALUES ('509', '52', '深泽县', '3', '0');
INSERT INTO `ft_area` VALUES ('510', '52', '赞皇县', '3', '0');
INSERT INTO `ft_area` VALUES ('511', '52', '无极县', '3', '0');
INSERT INTO `ft_area` VALUES ('512', '52', '平山县', '3', '0');
INSERT INTO `ft_area` VALUES ('513', '52', '元氏县', '3', '0');
INSERT INTO `ft_area` VALUES ('514', '52', '赵县', '3', '0');
INSERT INTO `ft_area` VALUES ('515', '52', '晋州市', '3', '0');
INSERT INTO `ft_area` VALUES ('516', '52', '新乐市', '3', '0');
INSERT INTO `ft_area` VALUES ('517', '52', '辛集市', '3', '0');
INSERT INTO `ft_area` VALUES ('518', '63', '历下区', '3', '0');
INSERT INTO `ft_area` VALUES ('519', '63', '市中区', '3', '0');
INSERT INTO `ft_area` VALUES ('520', '63', '槐荫区', '3', '0');
INSERT INTO `ft_area` VALUES ('521', '63', '天桥区', '3', '0');
INSERT INTO `ft_area` VALUES ('522', '63', '历城区', '3', '0');
INSERT INTO `ft_area` VALUES ('523', '63', '长清区', '3', '0');
INSERT INTO `ft_area` VALUES ('524', '63', '章丘区', '3', '0');
INSERT INTO `ft_area` VALUES ('525', '63', '平阴县', '3', '0');
INSERT INTO `ft_area` VALUES ('526', '63', '济阳县', '3', '0');
INSERT INTO `ft_area` VALUES ('527', '63', '商河县', '3', '0');
INSERT INTO `ft_area` VALUES ('528', '80', '和平区', '3', '0');
INSERT INTO `ft_area` VALUES ('529', '80', '沈河区', '3', '0');
INSERT INTO `ft_area` VALUES ('530', '80', '大东区', '3', '0');
INSERT INTO `ft_area` VALUES ('531', '80', '皇姑区', '3', '0');
INSERT INTO `ft_area` VALUES ('532', '80', '铁西区', '3', '0');
INSERT INTO `ft_area` VALUES ('533', '80', '苏家屯区', '3', '0');
INSERT INTO `ft_area` VALUES ('534', '80', '浑南区', '3', '0');
INSERT INTO `ft_area` VALUES ('535', '80', '沈北新区', '3', '0');
INSERT INTO `ft_area` VALUES ('536', '80', '于洪区', '3', '0');
INSERT INTO `ft_area` VALUES ('537', '80', '新民市', '3', '0');
INSERT INTO `ft_area` VALUES ('538', '80', '辽中县', '3', '0');
INSERT INTO `ft_area` VALUES ('539', '80', '康平县', '3', '0');
INSERT INTO `ft_area` VALUES ('540', '80', '法库县', '3', '0');
INSERT INTO `ft_area` VALUES ('541', '94', '松北区', '3', '0');
INSERT INTO `ft_area` VALUES ('542', '94', '南岗区', '3', '0');
INSERT INTO `ft_area` VALUES ('543', '94', '道里区', '3', '0');
INSERT INTO `ft_area` VALUES ('544', '94', '道外区', '3', '0');
INSERT INTO `ft_area` VALUES ('545', '94', '香坊区', '3', '0');
INSERT INTO `ft_area` VALUES ('546', '94', '平房区', '3', '0');
INSERT INTO `ft_area` VALUES ('547', '94', '呼兰区', '3', '0');
INSERT INTO `ft_area` VALUES ('548', '94', '阿城区', '3', '0');
INSERT INTO `ft_area` VALUES ('549', '94', '双城区', '3', '0');
INSERT INTO `ft_area` VALUES ('550', '94', '五常市', '3', '0');
INSERT INTO `ft_area` VALUES ('551', '94', '尚志市', '3', '0');
INSERT INTO `ft_area` VALUES ('552', '94', '宾县', '3', '0');
INSERT INTO `ft_area` VALUES ('553', '94', '巴彦县', '3', '0');
INSERT INTO `ft_area` VALUES ('554', '94', '延寿县', '3', '0');
INSERT INTO `ft_area` VALUES ('555', '94', '木兰县', '3', '0');
INSERT INTO `ft_area` VALUES ('556', '94', '通河县', '3', '0');
INSERT INTO `ft_area` VALUES ('557', '94', '方正县', '3', '0');
INSERT INTO `ft_area` VALUES ('558', '94', '依兰县', '3', '0');
INSERT INTO `ft_area` VALUES ('559', '107', '南关区', '3', '0');
INSERT INTO `ft_area` VALUES ('560', '107', '朝阳区', '3', '0');
INSERT INTO `ft_area` VALUES ('561', '107', '绿园区', '3', '0');
INSERT INTO `ft_area` VALUES ('562', '107', '二道区', '3', '0');
INSERT INTO `ft_area` VALUES ('563', '107', '双阳区', '3', '0');
INSERT INTO `ft_area` VALUES ('564', '107', '宽城区', '3', '0');
INSERT INTO `ft_area` VALUES ('565', '107', '九台区', '3', '0');
INSERT INTO `ft_area` VALUES ('566', '107', '榆树市', '3', '0');
INSERT INTO `ft_area` VALUES ('567', '107', '德惠市', '3', '0');
INSERT INTO `ft_area` VALUES ('568', '107', '农安县', '3', '0');
INSERT INTO `ft_area` VALUES ('569', '119', '城关区', '3', '0');
INSERT INTO `ft_area` VALUES ('570', '119', '七里河区', '3', '0');
INSERT INTO `ft_area` VALUES ('571', '119', '西固区', '3', '0');
INSERT INTO `ft_area` VALUES ('572', '119', '安宁区', '3', '0');
INSERT INTO `ft_area` VALUES ('573', '119', '红古区', '3', '0');
INSERT INTO `ft_area` VALUES ('574', '119', '兰州新区', '3', '0');
INSERT INTO `ft_area` VALUES ('575', '119', '榆中县', '3', '0');
INSERT INTO `ft_area` VALUES ('576', '119', '皋兰县', '3', '0');
INSERT INTO `ft_area` VALUES ('577', '119', '永登县', '3', '0');
INSERT INTO `ft_area` VALUES ('578', '133', '城中区', '3', '0');
INSERT INTO `ft_area` VALUES ('579', '133', '城东区', '3', '0');
INSERT INTO `ft_area` VALUES ('580', '133', '城西区', '3', '0');
INSERT INTO `ft_area` VALUES ('581', '133', '城北区', '3', '0');
INSERT INTO `ft_area` VALUES ('582', '133', '大通回族土族自治县', '3', '0');
INSERT INTO `ft_area` VALUES ('583', '133', '湟中县', '3', '0');
INSERT INTO `ft_area` VALUES ('584', '133', '湟源县', '3', '0');
INSERT INTO `ft_area` VALUES ('585', '141', '中原区', '3', '0');
INSERT INTO `ft_area` VALUES ('586', '141', '二七区', '3', '0');
INSERT INTO `ft_area` VALUES ('587', '141', '管城回族区', '3', '0');
INSERT INTO `ft_area` VALUES ('588', '141', '金水区', '3', '0');
INSERT INTO `ft_area` VALUES ('589', '141', '上街区', '3', '0');
INSERT INTO `ft_area` VALUES ('590', '141', '惠济区', '3', '0');
INSERT INTO `ft_area` VALUES ('591', '141', '中牟县', '3', '0');
INSERT INTO `ft_area` VALUES ('592', '141', '巩义市', '3', '0');
INSERT INTO `ft_area` VALUES ('593', '141', '荥阳市', '3', '0');
INSERT INTO `ft_area` VALUES ('594', '141', '新密市', '3', '0');
INSERT INTO `ft_area` VALUES ('595', '141', '新郑市', '3', '0');
INSERT INTO `ft_area` VALUES ('596', '141', '登封市', '3', '0');
INSERT INTO `ft_area` VALUES ('597', '159', '玄武区', '3', '0');
INSERT INTO `ft_area` VALUES ('598', '159', '秦淮区', '3', '0');
INSERT INTO `ft_area` VALUES ('599', '159', '鼓楼区', '3', '0');
INSERT INTO `ft_area` VALUES ('600', '159', '建邺区', '3', '0');
INSERT INTO `ft_area` VALUES ('601', '159', '栖霞区', '3', '0');
INSERT INTO `ft_area` VALUES ('602', '159', '雨花台区', '3', '0');
INSERT INTO `ft_area` VALUES ('603', '159', '江宁区', '3', '0');
INSERT INTO `ft_area` VALUES ('604', '159', '浦口区', '3', '0');
INSERT INTO `ft_area` VALUES ('605', '159', '六合区', '3', '0');
INSERT INTO `ft_area` VALUES ('606', '159', '溧水区', '3', '0');
INSERT INTO `ft_area` VALUES ('607', '159', '高淳区', '3', '0');
INSERT INTO `ft_area` VALUES ('608', '172', '南京东路街道', '3', '0');
INSERT INTO `ft_area` VALUES ('609', '172', '外滩街道', '3', '0');
INSERT INTO `ft_area` VALUES ('610', '172', '半淞园路街道', '3', '0');
INSERT INTO `ft_area` VALUES ('611', '172', '小东门街道', '3', '0');
INSERT INTO `ft_area` VALUES ('612', '172', '豫园街道', '3', '0');
INSERT INTO `ft_area` VALUES ('613', '172', '老西门街道', '3', '0');
INSERT INTO `ft_area` VALUES ('614', '172', '瑞金二路街道', '3', '0');
INSERT INTO `ft_area` VALUES ('615', '172', '淮海中路街道', '3', '0');
INSERT INTO `ft_area` VALUES ('616', '172', '打浦桥街道', '3', '0');
INSERT INTO `ft_area` VALUES ('617', '172', '五里桥街道', '3', '0');
INSERT INTO `ft_area` VALUES ('618', '189', '硚口区', '3', '0');
INSERT INTO `ft_area` VALUES ('619', '189', '汉阳区', '3', '0');
INSERT INTO `ft_area` VALUES ('620', '189', '武昌区', '3', '0');
INSERT INTO `ft_area` VALUES ('621', '189', '青山区', '3', '0');
INSERT INTO `ft_area` VALUES ('622', '189', '洪山区', '3', '0');
INSERT INTO `ft_area` VALUES ('623', '189', '东西湖区', '3', '0');
INSERT INTO `ft_area` VALUES ('624', '189', '黄陂区', '3', '0');
INSERT INTO `ft_area` VALUES ('625', '189', '江夏区', '3', '0');
INSERT INTO `ft_area` VALUES ('626', '189', '蔡甸区', '3', '0');
INSERT INTO `ft_area` VALUES ('627', '189', '汉南区', '3', '0');
INSERT INTO `ft_area` VALUES ('628', '189', '新洲区', '3', '0');
INSERT INTO `ft_area` VALUES ('629', '206', '芙蓉区', '3', '0');
INSERT INTO `ft_area` VALUES ('630', '206', '天心区', '3', '0');
INSERT INTO `ft_area` VALUES ('631', '206', '岳麓区', '3', '0');
INSERT INTO `ft_area` VALUES ('632', '206', '开福区', '3', '0');
INSERT INTO `ft_area` VALUES ('633', '206', '雨花区', '3', '0');
INSERT INTO `ft_area` VALUES ('634', '206', '望城区', '3', '0');
INSERT INTO `ft_area` VALUES ('635', '206', '长沙县', '3', '0');
INSERT INTO `ft_area` VALUES ('636', '206', '宁乡县', '3', '0');
INSERT INTO `ft_area` VALUES ('637', '206', '浏阳市', '3', '0');
INSERT INTO `ft_area` VALUES ('638', '220', '东湖区', '3', '0');
INSERT INTO `ft_area` VALUES ('639', '220', '西湖区', '3', '0');
INSERT INTO `ft_area` VALUES ('640', '220', '青云谱区', '3', '0');
INSERT INTO `ft_area` VALUES ('641', '220', '湾里区', '3', '0');
INSERT INTO `ft_area` VALUES ('642', '220', '青山湖区', '3', '0');
INSERT INTO `ft_area` VALUES ('643', '220', '南昌县', '3', '0');
INSERT INTO `ft_area` VALUES ('644', '220', '新建县', '3', '0');
INSERT INTO `ft_area` VALUES ('645', '220', '安义县', '3', '0');
INSERT INTO `ft_area` VALUES ('646', '220', '进贤县', '3', '0');
INSERT INTO `ft_area` VALUES ('647', '231', '上城区', '3', '0');
INSERT INTO `ft_area` VALUES ('648', '231', '下城区', '3', '0');
INSERT INTO `ft_area` VALUES ('649', '231', '江干区', '3', '0');
INSERT INTO `ft_area` VALUES ('650', '231', '拱墅区', '3', '0');
INSERT INTO `ft_area` VALUES ('651', '231', '西湖区', '3', '0');
INSERT INTO `ft_area` VALUES ('652', '231', '滨江区', '3', '0');
INSERT INTO `ft_area` VALUES ('653', '231', '余杭区', '3', '0');
INSERT INTO `ft_area` VALUES ('654', '231', '萧山区', '3', '0');
INSERT INTO `ft_area` VALUES ('655', '231', '富阳区', '3', '0');
INSERT INTO `ft_area` VALUES ('656', '231', '建德市', '3', '0');
INSERT INTO `ft_area` VALUES ('657', '231', '临安市', '3', '0');
INSERT INTO `ft_area` VALUES ('658', '231', '桐庐县', '3', '0');
INSERT INTO `ft_area` VALUES ('659', '231', '淳安县', '3', '0');
INSERT INTO `ft_area` VALUES ('660', '242', '越秀区', '3', '0');
INSERT INTO `ft_area` VALUES ('661', '242', '荔湾区', '3', '0');
INSERT INTO `ft_area` VALUES ('662', '242', '海珠区', '3', '0');
INSERT INTO `ft_area` VALUES ('663', '242', '天河区', '3', '0');
INSERT INTO `ft_area` VALUES ('664', '242', '白云区', '3', '0');
INSERT INTO `ft_area` VALUES ('665', '242', '黄浦区', '3', '0');
INSERT INTO `ft_area` VALUES ('666', '242', '花都区', '3', '0');
INSERT INTO `ft_area` VALUES ('667', '242', '番禺区', '3', '0');
INSERT INTO `ft_area` VALUES ('668', '242', '南沙区', '3', '0');
INSERT INTO `ft_area` VALUES ('669', '242', '增城区', '3', '0');
INSERT INTO `ft_area` VALUES ('670', '242', '从化区', '3', '0');
INSERT INTO `ft_area` VALUES ('671', '263', '呈贡区', '3', '0');
INSERT INTO `ft_area` VALUES ('672', '263', '盘龙区', '3', '0');
INSERT INTO `ft_area` VALUES ('673', '263', '五华区', '3', '0');
INSERT INTO `ft_area` VALUES ('674', '263', '官渡区', '3', '0');
INSERT INTO `ft_area` VALUES ('675', '263', '西山区', '3', '0');
INSERT INTO `ft_area` VALUES ('676', '263', '东川区', '3', '0');
INSERT INTO `ft_area` VALUES ('677', '263', '安宁市', '3', '0');
INSERT INTO `ft_area` VALUES ('678', '263', '晋宁县', '3', '0');
INSERT INTO `ft_area` VALUES ('679', '263', '富民县', '3', '0');
INSERT INTO `ft_area` VALUES ('680', '263', '宜良县', '3', '0');
INSERT INTO `ft_area` VALUES ('681', '263', '嵩明县', '3', '0');
INSERT INTO `ft_area` VALUES ('682', '263', '石林彝族自治县', '3', '0');
INSERT INTO `ft_area` VALUES ('683', '263', '禄劝彝族苗族自治县', '3', '0');
INSERT INTO `ft_area` VALUES ('684', '263', '寻甸回族彝族自治县', '3', '0');
INSERT INTO `ft_area` VALUES ('685', '279', '鼓楼区', '3', '0');
INSERT INTO `ft_area` VALUES ('686', '279', '台江区', '3', '0');
INSERT INTO `ft_area` VALUES ('687', '279', '仓山区', '3', '0');
INSERT INTO `ft_area` VALUES ('688', '279', '马尾区', '3', '0');
INSERT INTO `ft_area` VALUES ('689', '279', '晋安区', '3', '0');
INSERT INTO `ft_area` VALUES ('690', '279', '福清市', '3', '0');
INSERT INTO `ft_area` VALUES ('691', '279', '长乐市', '3', '0');
INSERT INTO `ft_area` VALUES ('692', '279', '闽侯县', '3', '0');
INSERT INTO `ft_area` VALUES ('693', '279', '连江县', '3', '0');
INSERT INTO `ft_area` VALUES ('694', '279', '罗源县', '3', '0');
INSERT INTO `ft_area` VALUES ('695', '279', '闽清县', '3', '0');
INSERT INTO `ft_area` VALUES ('696', '279', '永泰县', '3', '0');
INSERT INTO `ft_area` VALUES ('697', '289', '秀英区', '3', '0');
INSERT INTO `ft_area` VALUES ('698', '289', '龙华区', '3', '0');
INSERT INTO `ft_area` VALUES ('699', '289', '琼山区', '3', '0');
INSERT INTO `ft_area` VALUES ('700', '289', '美兰区', '3', '0');
INSERT INTO `ft_area` VALUES ('701', '298', '迎泽区', '3', '0');
INSERT INTO `ft_area` VALUES ('702', '298', '杏花岭区', '3', '0');
INSERT INTO `ft_area` VALUES ('703', '298', '万柏林区', '3', '0');
INSERT INTO `ft_area` VALUES ('704', '298', '尖草坪区', '3', '0');
INSERT INTO `ft_area` VALUES ('705', '298', '小店区', '3', '0');
INSERT INTO `ft_area` VALUES ('706', '298', '晋源区', '3', '0');
INSERT INTO `ft_area` VALUES ('707', '298', '清徐县', '3', '0');
INSERT INTO `ft_area` VALUES ('708', '298', '阳曲县', '3', '0');
INSERT INTO `ft_area` VALUES ('709', '298', '娄烦县', '3', '0');
INSERT INTO `ft_area` VALUES ('710', '298', '古交市', '3', '0');
INSERT INTO `ft_area` VALUES ('711', '309', '锦江区', '3', '0');
INSERT INTO `ft_area` VALUES ('712', '309', '青羊区', '3', '0');
INSERT INTO `ft_area` VALUES ('713', '309', '金牛区', '3', '0');
INSERT INTO `ft_area` VALUES ('714', '309', '武侯区', '3', '0');
INSERT INTO `ft_area` VALUES ('715', '309', '成华区', '3', '0');
INSERT INTO `ft_area` VALUES ('716', '309', '青白江区', '3', '0');
INSERT INTO `ft_area` VALUES ('717', '309', '龙泉驿区', '3', '0');
INSERT INTO `ft_area` VALUES ('718', '309', '新都区', '3', '0');
INSERT INTO `ft_area` VALUES ('719', '309', '温江区', '3', '0');
INSERT INTO `ft_area` VALUES ('720', '309', '金堂县', '3', '0');
INSERT INTO `ft_area` VALUES ('721', '309', '双流县', '3', '0');
INSERT INTO `ft_area` VALUES ('722', '309', '郫县', '3', '0');
INSERT INTO `ft_area` VALUES ('723', '309', '大邑县', '3', '0');
INSERT INTO `ft_area` VALUES ('724', '309', '蒲江县', '3', '0');
INSERT INTO `ft_area` VALUES ('725', '309', '新津县', '3', '0');
INSERT INTO `ft_area` VALUES ('726', '309', '都江堰市', '3', '0');
INSERT INTO `ft_area` VALUES ('727', '309', '彭州市', '3', '0');
INSERT INTO `ft_area` VALUES ('728', '309', '崇州市', '3', '0');
INSERT INTO `ft_area` VALUES ('729', '309', '邛崃市', '3', '0');
INSERT INTO `ft_area` VALUES ('730', '329', '朝天门街道', '3', '0');
INSERT INTO `ft_area` VALUES ('731', '329', '南纪门街道', '3', '0');
INSERT INTO `ft_area` VALUES ('732', '329', '解放碑街道', '3', '0');
INSERT INTO `ft_area` VALUES ('733', '367', '浑河湾街道', '3', '0');
INSERT INTO `ft_area` VALUES ('734', '367', '新华街道', '3', '0');
INSERT INTO `ft_area` VALUES ('735', '367', '西塔街道', '3', '0');
INSERT INTO `ft_area` VALUES ('736', '367', '八斤街道', '3', '0');
INSERT INTO `ft_area` VALUES ('737', '367', '马路湾街道', '3', '0');
INSERT INTO `ft_area` VALUES ('738', '367', '集贤街道', '3', '0');
INSERT INTO `ft_area` VALUES ('739', '367', '南湖街道', '3', '0');
INSERT INTO `ft_area` VALUES ('740', '367', '长白街道', '3', '0');
INSERT INTO `ft_area` VALUES ('741', '367', '太原街街道', '3', '0');
INSERT INTO `ft_area` VALUES ('742', '367', '北市场街道', '3', '0');
INSERT INTO `ft_area` VALUES ('743', '367', '南市场街道', '3', '0');
INSERT INTO `ft_area` VALUES ('744', '367', '沈水湾街道', '3', '0');
INSERT INTO `ft_area` VALUES ('745', '367', '浑河站西街道', '3', '0');
INSERT INTO `ft_area` VALUES ('746', '383', '新城区', '3', '0');
INSERT INTO `ft_area` VALUES ('747', '383', '碑林区', '3', '0');
INSERT INTO `ft_area` VALUES ('748', '383', '莲湖区', '3', '0');
INSERT INTO `ft_area` VALUES ('749', '383', '灞桥区', '3', '0');
INSERT INTO `ft_area` VALUES ('750', '383', '未央区', '3', '0');
INSERT INTO `ft_area` VALUES ('751', '383', '雁塔区', '3', '0');
INSERT INTO `ft_area` VALUES ('752', '383', '阎良区', '3', '0');
INSERT INTO `ft_area` VALUES ('753', '383', '临潼区', '3', '0');
INSERT INTO `ft_area` VALUES ('754', '383', '长安区', '3', '0');
INSERT INTO `ft_area` VALUES ('755', '383', '高陵区', '3', '0');
INSERT INTO `ft_area` VALUES ('756', '383', '蓝田县', '3', '0');
INSERT INTO `ft_area` VALUES ('757', '383', '周至县', '3', '0');
INSERT INTO `ft_area` VALUES ('758', '383', '户县', '3', '0');
INSERT INTO `ft_area` VALUES ('759', '394', '云岩区', '3', '0');
INSERT INTO `ft_area` VALUES ('760', '394', '南明区', '3', '0');
INSERT INTO `ft_area` VALUES ('761', '394', '观山湖区', '3', '0');
INSERT INTO `ft_area` VALUES ('762', '394', '花溪区', '3', '0');
INSERT INTO `ft_area` VALUES ('763', '394', '乌当区', '3', '0');
INSERT INTO `ft_area` VALUES ('764', '394', '白云区', '3', '0');
INSERT INTO `ft_area` VALUES ('765', '394', '清镇市', '3', '0');
INSERT INTO `ft_area` VALUES ('766', '394', '修文县', '3', '0');
INSERT INTO `ft_area` VALUES ('767', '394', '息烽县', '3', '0');
INSERT INTO `ft_area` VALUES ('768', '394', '开阳县', '3', '0');
INSERT INTO `ft_area` VALUES ('769', '403', '瑶海区', '3', '0');
INSERT INTO `ft_area` VALUES ('770', '403', '庐阳区', '3', '0');
INSERT INTO `ft_area` VALUES ('771', '403', '蜀山区', '3', '0');
INSERT INTO `ft_area` VALUES ('772', '403', '包河区', '3', '0');
INSERT INTO `ft_area` VALUES ('773', '403', '肥东县', '3', '0');
INSERT INTO `ft_area` VALUES ('774', '403', '肥西县', '3', '0');
INSERT INTO `ft_area` VALUES ('775', '403', '长丰县', '3', '0');
INSERT INTO `ft_area` VALUES ('776', '403', '庐江县', '3', '0');
INSERT INTO `ft_area` VALUES ('777', '403', '巢湖市', '3', '0');
INSERT INTO `ft_area` VALUES ('778', '419', '回民区', '3', '0');
INSERT INTO `ft_area` VALUES ('779', '419', '新城区', '3', '0');
INSERT INTO `ft_area` VALUES ('780', '419', '玉泉区', '3', '0');
INSERT INTO `ft_area` VALUES ('781', '419', '赛罕区', '3', '0');
INSERT INTO `ft_area` VALUES ('782', '419', '土默特左旗', '3', '0');
INSERT INTO `ft_area` VALUES ('783', '419', '托克托县', '3', '0');
INSERT INTO `ft_area` VALUES ('784', '419', '和林格尔县', '3', '0');
INSERT INTO `ft_area` VALUES ('785', '419', '武川县', '3', '0');
INSERT INTO `ft_area` VALUES ('786', '419', '清水河县', '3', '0');
INSERT INTO `ft_area` VALUES ('787', '431', '兴庆区金凤区西夏区灵武市', '3', '0');
INSERT INTO `ft_area` VALUES ('788', '431', '永宁县', '3', '0');
INSERT INTO `ft_area` VALUES ('789', '431', '贺兰县', '3', '0');
INSERT INTO `ft_area` VALUES ('790', '436', '天山区', '3', '0');
INSERT INTO `ft_area` VALUES ('791', '436', '沙依巴克区', '3', '0');
INSERT INTO `ft_area` VALUES ('792', '436', '新市区', '3', '0');
INSERT INTO `ft_area` VALUES ('793', '436', '水磨沟区', '3', '0');
INSERT INTO `ft_area` VALUES ('794', '436', '头屯河区', '3', '0');
INSERT INTO `ft_area` VALUES ('795', '436', '达坂城区', '3', '0');
INSERT INTO `ft_area` VALUES ('796', '436', '米东区乌鲁木齐县', '3', '0');
INSERT INTO `ft_area` VALUES ('797', '458', '城关区', '3', '0');
INSERT INTO `ft_area` VALUES ('798', '458', '林周县', '3', '0');
INSERT INTO `ft_area` VALUES ('799', '458', '达孜县', '3', '0');
INSERT INTO `ft_area` VALUES ('800', '458', '堆龙德庆县', '3', '0');
INSERT INTO `ft_area` VALUES ('801', '458', '尼木县', '3', '0');
INSERT INTO `ft_area` VALUES ('802', '458', '当雄县', '3', '0');
INSERT INTO `ft_area` VALUES ('803', '458', '曲水县', '3', '0');
INSERT INTO `ft_area` VALUES ('804', '458', '墨竹工卡县', '3', '0');
INSERT INTO `ft_area` VALUES ('805', '465', '青秀区', '3', '0');
INSERT INTO `ft_area` VALUES ('806', '465', '兴宁区', '3', '0');
INSERT INTO `ft_area` VALUES ('807', '465', '西乡塘区', '3', '0');
INSERT INTO `ft_area` VALUES ('808', '465', '江南区', '3', '0');
INSERT INTO `ft_area` VALUES ('809', '465', '良庆区', '3', '0');
INSERT INTO `ft_area` VALUES ('810', '465', '邕宁区', '3', '0');
INSERT INTO `ft_area` VALUES ('811', '465', '武鸣县', '3', '0');
INSERT INTO `ft_area` VALUES ('812', '465', '隆安县', '3', '0');
INSERT INTO `ft_area` VALUES ('813', '465', '马山县', '3', '0');
INSERT INTO `ft_area` VALUES ('814', '465', '上林县', '3', '0');
INSERT INTO `ft_area` VALUES ('815', '465', '宾阳县', '3', '0');
INSERT INTO `ft_area` VALUES ('816', '465', '横县', '3', '0');
INSERT INTO `ft_area` VALUES ('817', '479', '中西区', '3', '0');
INSERT INTO `ft_area` VALUES ('818', '479', '湾仔区', '3', '0');
INSERT INTO `ft_area` VALUES ('819', '479', '东区', '3', '0');
INSERT INTO `ft_area` VALUES ('820', '479', '南区', '3', '0');
INSERT INTO `ft_area` VALUES ('821', '482', '青州', '3', '0');
INSERT INTO `ft_area` VALUES ('822', '482', '台山', '3', '0');
INSERT INTO `ft_area` VALUES ('823', '482', '黑沙环', '3', '0');
INSERT INTO `ft_area` VALUES ('824', '482', '筷子基', '3', '0');
INSERT INTO `ft_area` VALUES ('825', '482', '水塘', '3', '0');
INSERT INTO `ft_area` VALUES ('826', '490', '中正区', '3', '0');
INSERT INTO `ft_area` VALUES ('827', '490', '大同区', '3', '0');
INSERT INTO `ft_area` VALUES ('828', '490', '中山区', '3', '0');
INSERT INTO `ft_area` VALUES ('829', '490', '万华区', '3', '0');
INSERT INTO `ft_area` VALUES ('830', '490', '信义区', '3', '0');
INSERT INTO `ft_area` VALUES ('831', '490', '松山区', '3', '0');
INSERT INTO `ft_area` VALUES ('832', '490', '大安区', '3', '0');
INSERT INTO `ft_area` VALUES ('833', '490', '南港区', '3', '0');
INSERT INTO `ft_area` VALUES ('834', '490', '北投区', '3', '0');
INSERT INTO `ft_area` VALUES ('835', '490', '内湖区', '3', '0');
INSERT INTO `ft_area` VALUES ('836', '490', '士林区', '3', '0');
INSERT INTO `ft_area` VALUES ('837', '490', '文山区', '3', '0');
INSERT INTO `ft_area` VALUES ('838', '36', '安定门街道', '3', '0');
INSERT INTO `ft_area` VALUES ('839', '36', '建国门街道', '3', '0');
INSERT INTO `ft_area` VALUES ('840', '36', '朝阳门街道', '3', '0');
INSERT INTO `ft_area` VALUES ('841', '36', '东直门街道', '3', '0');
INSERT INTO `ft_area` VALUES ('842', '36', '东华门街道', '3', '0');
INSERT INTO `ft_area` VALUES ('843', '36', '和平里街道', '3', '0');
INSERT INTO `ft_area` VALUES ('844', '36', '北新桥街道', '3', '0');
INSERT INTO `ft_area` VALUES ('845', '36', '交道口街道', '3', '0');
INSERT INTO `ft_area` VALUES ('846', '36', '景山街道', '3', '0');
INSERT INTO `ft_area` VALUES ('847', '36', '东四街道', '3', '0');
INSERT INTO `ft_area` VALUES ('848', '36', '天坛街道', '3', '0');
INSERT INTO `ft_area` VALUES ('849', '36', '东花市街道', '3', '0');
INSERT INTO `ft_area` VALUES ('850', '36', '前门街道', '3', '0');
INSERT INTO `ft_area` VALUES ('851', '36', '龙潭街道', '3', '0');
INSERT INTO `ft_area` VALUES ('852', '36', '永定门外街道', '3', '0');
INSERT INTO `ft_area` VALUES ('853', '36', '崇文门外街道', '3', '0');
INSERT INTO `ft_area` VALUES ('854', '36', '体育馆街道', '3', '0');

-- ----------------------------
-- Table structure for `ft_attach`
-- ----------------------------
DROP TABLE IF EXISTS `ft_attach`;
CREATE TABLE `ft_attach` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '附件ID',
  `userid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '用户ID',
  `file_name` varchar(50) NOT NULL DEFAULT '' COMMENT '附件名称',
  `file_path` varchar(100) NOT NULL DEFAULT '' COMMENT '文件路径',
  `file_size` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '文件大小',
  `file_ext` char(10) NOT NULL DEFAULT '' COMMENT '文件名后缀',
  `createtime` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '上传时间',
  `upload_ip` varchar(15) NOT NULL DEFAULT '' COMMENT '上传的IP',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ft_attach
-- ----------------------------
INSERT INTO `ft_attach` VALUES ('1', '1', 'fasttop 160_60.png', '/Uploads/2015-06-06/5572c6f7d369a.png', '13506', 'png', '1433585399', '127.0.0.1');
INSERT INTO `ft_attach` VALUES ('2', '1', 'bd_logo1.png', '/Uploads/2015-06-06/5572c9b4454b2.png', '7877', 'png', '1433586100', '127.0.0.1');
INSERT INTO `ft_attach` VALUES ('3', '1', '117934795.jpg', '/Uploads/2015-06-10/5577a9891581e.jpg', '53332', 'jpg', '1433905544', '127.0.0.1');
INSERT INTO `ft_attach` VALUES ('4', '1', '117934795.jpg', '/Uploads/2015-06-10/5577af5cc3489.jpg', '53332', 'jpg', '1433907036', '127.0.0.1');
INSERT INTO `ft_attach` VALUES ('5', '1', '0050040043.jpg', '/Uploads/2015-06-10/5577b15c71a2a.jpg', '163238', 'jpg', '1433907548', '127.0.0.1');
INSERT INTO `ft_attach` VALUES ('6', '1', '117934795.jpg', '/Uploads/2015-06-10/5577b182711a4.jpg', '53332', 'jpg', '1433907586', '127.0.0.1');

-- ----------------------------
-- Table structure for `ft_auth_group`
-- ----------------------------
DROP TABLE IF EXISTS `ft_auth_group`;
CREATE TABLE `ft_auth_group` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '用户组ID',
  `module` varchar(20) NOT NULL DEFAULT '' COMMENT '用户组所属组别',
  `group_type` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '用户所属类型',
  `title` varchar(30) NOT NULL DEFAULT '' COMMENT '用户组名称',
  `group_desc` varchar(80) NOT NULL DEFAULT '' COMMENT '角色描述',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '用户组状态',
  `rules` text NOT NULL COMMENT '用户组遵循的规则',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ft_auth_group
-- ----------------------------
INSERT INTO `ft_auth_group` VALUES ('1', 'Admin', '1', '用户管理员', '用户管理员，主要负责对用户信息进行管理。', '1', '1,3,5,6,18,19,20,21,22,23,38,39,42,45,48');
INSERT INTO `ft_auth_group` VALUES ('2', 'Admin', '1', '系统管理员', '系统管理员，主要对系统部分进行管理。', '1', '1,4,7,8,18,19,20,21,22');
INSERT INTO `ft_auth_group` VALUES ('3', 'Admin', '1', '编辑人员', '网站编辑人员', '1', '1,3,5,18,19,20,21,22,23,45');
INSERT INTO `ft_auth_group` VALUES ('4', 'Admin', '1', '终端用户', '终端用户，只能在前台登录', '1', '');

-- ----------------------------
-- Table structure for `ft_auth_group_access`
-- ----------------------------
DROP TABLE IF EXISTS `ft_auth_group_access`;
CREATE TABLE `ft_auth_group_access` (
  `uid` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT '用户ID',
  `group_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '用户所属组别ID'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ft_auth_group_access
-- ----------------------------
INSERT INTO `ft_auth_group_access` VALUES ('9', '4');

-- ----------------------------
-- Table structure for `ft_auth_rule`
-- ----------------------------
DROP TABLE IF EXISTS `ft_auth_rule`;
CREATE TABLE `ft_auth_rule` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT COMMENT '规则ID',
  `name` varchar(100) NOT NULL DEFAULT '' COMMENT '规则名称',
  `title` varchar(50) NOT NULL DEFAULT '' COMMENT '规则标题',
  `module` varchar(20) NOT NULL DEFAULT '' COMMENT '规则所属模块',
  `type` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '规则类型',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '规则状态',
  `condition` varchar(255) NOT NULL DEFAULT '' COMMENT '规则附加条件',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=84 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ft_auth_rule
-- ----------------------------
INSERT INTO `ft_auth_rule` VALUES ('1', 'Admin/Index/index', '首页', 'Admin', '2', '1', '');
INSERT INTO `ft_auth_rule` VALUES ('2', 'Admin/Content/list', '内容', 'Admin', '2', '1', '');
INSERT INTO `ft_auth_rule` VALUES ('3', 'Admin/User/list', '用户', 'Admin', '2', '1', '');
INSERT INTO `ft_auth_rule` VALUES ('4', 'Admin/Config/system', '系统', 'Admin', '2', '1', '');
INSERT INTO `ft_auth_rule` VALUES ('5', 'Admin/User/index', '用户信息', 'Admin', '1', '1', '');
INSERT INTO `ft_auth_rule` VALUES ('6', 'Admin/Role/index', '用户角色管理', 'Admin', '1', '1', '');
INSERT INTO `ft_auth_rule` VALUES ('7', 'Admin/Config/group', '网站设置', 'Admin', '1', '1', '');
INSERT INTO `ft_auth_rule` VALUES ('8', 'Admin/Config/index', '配置管理', 'Admin', '1', '1', '');
INSERT INTO `ft_auth_rule` VALUES ('9', 'Admin/Menu/index', '菜单管理', 'Admin', '1', '1', '');
INSERT INTO `ft_auth_rule` VALUES ('10', 'Admin/Link/index', '友情链接', 'Admin', '1', '1', '');
INSERT INTO `ft_auth_rule` VALUES ('11', 'Admin/Area/index', '地区管理', 'Admin', '1', '1', '');
INSERT INTO `ft_auth_rule` VALUES ('12', 'Admin/Menu/add', '新增菜单', 'Admin', '1', '1', '');
INSERT INTO `ft_auth_rule` VALUES ('13', 'Admin/Menu/edit', '编辑菜单', 'Admin', '1', '1', '');
INSERT INTO `ft_auth_rule` VALUES ('14', 'Admin/Menu/listorder', '菜单排序', 'Admin', '1', '1', '');
INSERT INTO `ft_auth_rule` VALUES ('15', 'Admin/Menu/delete', '删除菜单', 'Admin', '1', '1', '');
INSERT INTO `ft_auth_rule` VALUES ('16', 'Admin/Menu/delBatch', '批量删除菜单', 'Admin', '1', '1', '');
INSERT INTO `ft_auth_rule` VALUES ('17', 'Admin/Link/add', '新增链接', 'Admin', '1', '1', '');
INSERT INTO `ft_auth_rule` VALUES ('18', 'Admin/Index/profile', '个人资料管理', 'Admin', '1', '1', '');
INSERT INTO `ft_auth_rule` VALUES ('19', 'Admin/Index/editProfile', '修改个人资料', 'Admin', '1', '1', '');
INSERT INTO `ft_auth_rule` VALUES ('20', 'Admin/Index/editPassword', '修改密码', 'Admin', '1', '1', '');
INSERT INTO `ft_auth_rule` VALUES ('21', 'Admin/Index/lock', '锁屏', 'Admin', '1', '1', '');
INSERT INTO `ft_auth_rule` VALUES ('22', 'Admin/Index/unlock', '屏幕解锁', 'Admin', '1', '1', '');
INSERT INTO `ft_auth_rule` VALUES ('23', 'Admin/User/add', '新增用户', 'Admin', '1', '1', '');
INSERT INTO `ft_auth_rule` VALUES ('24', 'Admin/Menu/status', '更改菜单状态', 'Admin', '1', '1', '');
INSERT INTO `ft_auth_rule` VALUES ('25', 'Admin/Area/add', '新增地区', 'Admin', '1', '1', '');
INSERT INTO `ft_auth_rule` VALUES ('26', 'Admin/Menu/insert', '插入菜单', 'Admin', '1', '1', '');
INSERT INTO `ft_auth_rule` VALUES ('27', 'Admin/Menu/update', '更新菜单', 'Admin', '1', '1', '');
INSERT INTO `ft_auth_rule` VALUES ('28', 'Admin/Area/editArea', '更新地区', 'Admin', '1', '1', '');
INSERT INTO `ft_auth_rule` VALUES ('29', 'Admin/Config/add', '新增配置', 'Admin', '1', '1', '');
INSERT INTO `ft_auth_rule` VALUES ('30', 'Admin/Config/edit', '编辑配置', 'Admin', '1', '1', '');
INSERT INTO `ft_auth_rule` VALUES ('31', 'Admin/Config/insert', '插入配置', 'Admin', '1', '1', '');
INSERT INTO `ft_auth_rule` VALUES ('32', 'Admin/Config/update', '更新配置', 'Admin', '1', '1', '');
INSERT INTO `ft_auth_rule` VALUES ('33', 'Admin/Config/save', '更新网站设置', 'Admin', '1', '1', '');
INSERT INTO `ft_auth_rule` VALUES ('34', 'Admin/Config/status', '更改配置状态', 'Admin', '1', '1', '');
INSERT INTO `ft_auth_rule` VALUES ('35', 'Admin/Link/edit', '编辑链接', 'Admin', '1', '1', '');
INSERT INTO `ft_auth_rule` VALUES ('36', 'Admin/Link/addLink', '插入友情链接', 'Admin', '1', '1', '');
INSERT INTO `ft_auth_rule` VALUES ('37', 'Admin/Link/editLink', '更新友情链接', 'Admin', '1', '1', '');
INSERT INTO `ft_auth_rule` VALUES ('38', 'Admin/Role/add', '添加角色', 'Admin', '1', '1', '');
INSERT INTO `ft_auth_rule` VALUES ('39', 'Admin/Role/editGroup', '编辑角色', 'Admin', '1', '1', '');
INSERT INTO `ft_auth_rule` VALUES ('40', 'Admin/Role/createGroup', '插入角色', 'Admin', '1', '1', '');
INSERT INTO `ft_auth_rule` VALUES ('41', 'Admin/Role/updateGroup', '更新角色', 'Admin', '1', '1', '');
INSERT INTO `ft_auth_rule` VALUES ('42', 'Admin/Role/setStatus', '更改角色状态', 'Admin', '1', '1', '');
INSERT INTO `ft_auth_rule` VALUES ('43', 'Admin/Role/access', '访问授权', 'Admin', '1', '1', '');
INSERT INTO `ft_auth_rule` VALUES ('44', 'Admin/Link/status', '更改链接状态', 'Admin', '1', '1', '');
INSERT INTO `ft_auth_rule` VALUES ('45', 'Admin/User/editUser', '编辑用户', 'Admin', '1', '1', '');
INSERT INTO `ft_auth_rule` VALUES ('46', 'Admin/User/addUser', '插入用户', 'Admin', '1', '1', '');
INSERT INTO `ft_auth_rule` VALUES ('47', 'Admin/User/updateUser', '更新用户', 'Admin', '1', '1', '');
INSERT INTO `ft_auth_rule` VALUES ('48', 'Admin/User/setStatus', '更改用户状态', 'Admin', '1', '1', '');
INSERT INTO `ft_auth_rule` VALUES ('49', 'Admin/Area/delete', '删除地区', 'Admin', '1', '1', '');
INSERT INTO `ft_auth_rule` VALUES ('50', 'Admin/Area/listorder', '地区排序', 'Admin', '1', '1', '');
INSERT INTO `ft_auth_rule` VALUES ('51', 'Admin/Link/delete', '删除友情链接', 'Admin', '1', '1', '');
INSERT INTO `ft_auth_rule` VALUES ('52', 'Admin/Link/listorder', '友情链接排序', 'Admin', '1', '1', '');
INSERT INTO `ft_auth_rule` VALUES ('53', 'Admin/Link/delBatch', '批量删除链接', 'Admin', '1', '1', '');
INSERT INTO `ft_auth_rule` VALUES ('54', 'Admin/Config/listorder', '配置排序', 'Admin', '2', '0', '');
INSERT INTO `ft_auth_rule` VALUES ('55', 'Admin/Config/listorder', '配置排序', 'Admin', '1', '1', '');
INSERT INTO `ft_auth_rule` VALUES ('56', 'Admin/Config/delete', '删除配置', 'Admin', '1', '1', '');
INSERT INTO `ft_auth_rule` VALUES ('57', 'Admin/Config/delConfig', '批量删除配置', 'Admin', '1', '1', '');
INSERT INTO `ft_auth_rule` VALUES ('58', 'Admin/Role/deleteGroup', '删除角色', 'Admin', '1', '1', '');
INSERT INTO `ft_auth_rule` VALUES ('59', 'Admin/Role/delGroup', '批量删除角色', 'Admin', '1', '1', '');
INSERT INTO `ft_auth_rule` VALUES ('60', 'Admin/User/deleteUser', '单个删除用户', 'Admin', '1', '1', '');
INSERT INTO `ft_auth_rule` VALUES ('61', 'Admin/User/delUser', '批量删除用户', 'Admin', '1', '1', '');
INSERT INTO `ft_auth_rule` VALUES ('62', 'Admin/Content/index', '管理内容', 'Admin', '1', '1', '');
INSERT INTO `ft_auth_rule` VALUES ('63', 'Admin/Navigator/index', '导航管理', 'Admin', '1', '1', '');
INSERT INTO `ft_auth_rule` VALUES ('64', 'Admin/ChatRoom/index', '聊天室管理', 'Admin', '1', '1', '');
INSERT INTO `ft_auth_rule` VALUES ('65', 'Admin/classify/index', '分类管理', 'Admin', '1', '1', '');
INSERT INTO `ft_auth_rule` VALUES ('66', 'Admin/Course/index', '课程管理', 'Admin', '1', '1', '');
INSERT INTO `ft_auth_rule` VALUES ('67', 'Admin/Shout/index', '喊单管理', 'Admin', '1', '1', '');
INSERT INTO `ft_auth_rule` VALUES ('68', 'Admin/ChatRoom/add', '添加聊天室', 'Admin', '1', '1', '');
INSERT INTO `ft_auth_rule` VALUES ('69', 'Admin/ChatRoom/editRoom', '编辑聊天室', 'Admin', '1', '1', '');
INSERT INTO `ft_auth_rule` VALUES ('70', 'Admin/ChatRoom/status', '更改聊天室状态', 'Admin', '1', '1', '');
INSERT INTO `ft_auth_rule` VALUES ('71', 'Admin/ChatRoom/delete', '删除聊天室', 'Admin', '1', '1', '');
INSERT INTO `ft_auth_rule` VALUES ('72', 'Admin/ChatRoom/delBatch', '批量删除聊天室', 'Admin', '1', '1', '');
INSERT INTO `ft_auth_rule` VALUES ('73', 'Admin/ChatRoom/addRoom', '插入聊天室', 'Admin', '1', '1', '');
INSERT INTO `ft_auth_rule` VALUES ('74', 'Admin/ChatRoom/updateRoom', '更新聊天室', 'Admin', '1', '1', '');
INSERT INTO `ft_auth_rule` VALUES ('75', 'Admin/Classify/add', '添加分类', 'Admin', '1', '1', '');
INSERT INTO `ft_auth_rule` VALUES ('76', 'Admin/Classify/edit', '编辑分类', 'Admin', '1', '1', '');
INSERT INTO `ft_auth_rule` VALUES ('77', 'Admin/Classify/status', '更改分类状态', 'Admin', '1', '1', '');
INSERT INTO `ft_auth_rule` VALUES ('78', 'Admin/Classify/insert', '插入分类', 'Admin', '1', '1', '');
INSERT INTO `ft_auth_rule` VALUES ('79', 'Admin/Classify/update', '更新分类', 'Admin', '1', '1', '');
INSERT INTO `ft_auth_rule` VALUES ('80', 'Admin/ChatRoom/listorder', '聊天室排序', 'Admin', '1', '1', '');
INSERT INTO `ft_auth_rule` VALUES ('81', 'Admin/Classify/listorder', '分类排序', 'Admin', '1', '1', '');
INSERT INTO `ft_auth_rule` VALUES ('82', 'Admin/Classify/delete', '删除分类', 'Admin', '1', '1', '');
INSERT INTO `ft_auth_rule` VALUES ('83', 'Admin/Classify/delBatch', '批量删除分类', 'Admin', '1', '1', '');

-- ----------------------------
-- Table structure for `ft_chat_content`
-- ----------------------------
DROP TABLE IF EXISTS `ft_chat_content`;
CREATE TABLE `ft_chat_content` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT COMMENT '聊天内容ID',
  `roomid` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '聊天室ID',
  `chat_content` text NOT NULL COMMENT '聊天内容',
  `chat_content_userid` varchar(8) NOT NULL DEFAULT '' COMMENT '用户ID',
  `chat_content_username` varchar(30) NOT NULL DEFAULT '' COMMENT '用户名',
  `createtime` int(10) NOT NULL DEFAULT '0' COMMENT '聊天时间',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '用户状态，1：放行 0：禁言',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=117 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ft_chat_content
-- ----------------------------
INSERT INTO `ft_chat_content` VALUES ('1', '1', '哈哈', '1', 'aadadadad', '1434532897', '1');
INSERT INTO `ft_chat_content` VALUES ('2', '1', '555，终于成功了', '1', 'admin', '1434528583', '1');
INSERT INTO `ft_chat_content` VALUES ('3', '1', '试试看', '58XWbfQe', '游客58XWbfQe', '1434529536', '1');
INSERT INTO `ft_chat_content` VALUES ('4', '1', '试试看', '58XWbfQe', '游客58XWbfQe', '1434529536', '1');
INSERT INTO `ft_chat_content` VALUES ('5', '1', 'dff', '45reMcC6', '游客45reMcC6', '1434537609', '1');
INSERT INTO `ft_chat_content` VALUES ('6', '1', 'grgrgrg', '45reMcC6', '游客45reMcC6', '1434537655', '1');
INSERT INTO `ft_chat_content` VALUES ('7', '1', 'ffffff', '05r3AUhb', '游客05r3AUhb', '1434546622', '1');
INSERT INTO `ft_chat_content` VALUES ('8', '1', '666', '559XMCAf', '游客559XMCAf', '1434603060', '1');
INSERT INTO `ft_chat_content` VALUES ('9', '1', 'hhhh', '559XMCAf', '游客559XMCAf', '1434625395', '1');
INSERT INTO `ft_chat_content` VALUES ('10', '1', 'rrrrr', '1', 'admin', '1434711146', '1');
INSERT INTO `ft_chat_content` VALUES ('11', '1', 'hhhh', '1', 'admin', '1434712296', '1');
INSERT INTO `ft_chat_content` VALUES ('12', '1', '试试', '1', 'admin', '1434712383', '1');
INSERT INTO `ft_chat_content` VALUES ('13', '1', '截至午间收盘，沪指跌4.06%报4344.06点；深成指跌6.08%报14738.91点。分析人士表示，牛市行情并没有结束，短期做空力量宣泄完毕之后，股指仍会继续上行。', '13', 'ketity1', '1435291268', '1');
INSERT INTO `ft_chat_content` VALUES ('14', '1', '试试能不能滚动', '13', 'ketity1', '1435302670', '1');
INSERT INTO `ft_chat_content` VALUES ('15', '1', '再试试', '13', 'ketity1', '1435303290', '1');
INSERT INTO `ft_chat_content` VALUES ('16', '1', '出问题了', '13', 'ketity1', '1435306698', '1');
INSERT INTO `ft_chat_content` VALUES ('17', '1', '呼呼呼呼呼呼呼呼呼呼呼呼呼呼', '13', 'ketity1', '1435310456', '1');
INSERT INTO `ft_chat_content` VALUES ('18', '1', '好无语', '93bCm3VT', '游客93bCm3VT', '1435310523', '1');
INSERT INTO `ft_chat_content` VALUES ('19', '1', '怎么啦', '13', 'ketity1', '1435310550', '1');
INSERT INTO `ft_chat_content` VALUES ('20', '1', '又降了', '93bCm3VT', '游客93bCm3VT', '1435310631', '1');
INSERT INTO `ft_chat_content` VALUES ('21', '1', '很正常啦~~~', '13', 'ketity1', '1435310660', '1');
INSERT INTO `ft_chat_content` VALUES ('22', '1', '伤心ing', '93bCm3VT', '游客93bCm3VT', '1435311198', '1');
INSERT INTO `ft_chat_content` VALUES ('23', '1', '哈哈 吸取教训哇', '13', 'ketity1', '1435311223', '1');
INSERT INTO `ft_chat_content` VALUES ('24', '1', '对的 ', '93bCm3VT', '游客93bCm3VT', '1435311260', '1');
INSERT INTO `ft_chat_content` VALUES ('25', '1', '加油', '13', 'ketity1', '1435311273', '1');
INSERT INTO `ft_chat_content` VALUES ('26', '1', '哈哈哈哈哈哈哈哈哈哈哈', '13', 'ketity1', '1435311840', '1');
INSERT INTO `ft_chat_content` VALUES ('27', '1', '拜托拜托', '93bCm3VT', '游客93bCm3VT', '1435312846', '1');
INSERT INTO `ft_chat_content` VALUES ('28', '1', '顶顶顶顶顶顶顶顶顶', '13', 'ketity1', '1435312981', '1');
INSERT INTO `ft_chat_content` VALUES ('29', '1', '11111111111111111111111111', '93bCm3VT', '游客93bCm3VT', '1435313513', '1');
INSERT INTO `ft_chat_content` VALUES ('30', '1', '哈哈哈哈 是不是可以了呢', '2636vVa9', '游客2636vVa9', '1435393322', '1');
INSERT INTO `ft_chat_content` VALUES ('31', '1', '悲催', '2636vVa9', '游客2636vVa9', '1435393430', '1');
INSERT INTO `ft_chat_content` VALUES ('32', '1', '怎么改呢', '2636vVa9', '游客2636vVa9', '1435393776', '1');
INSERT INTO `ft_chat_content` VALUES ('33', '1', '哎 为什么呢', '2636vVa9', '游客2636vVa9', '1435394279', '1');
INSERT INTO `ft_chat_content` VALUES ('34', '1', '哈哈  好像可以了哦', '2636vVa9', '游客2636vVa9', '1435402902', '1');
INSERT INTO `ft_chat_content` VALUES ('35', '1', '再试一次', '2636vVa9', '游客2636vVa9', '1435402953', '1');
INSERT INTO `ft_chat_content` VALUES ('36', '1', 'lalallallalllallllllallalal', '2636vVa9', '游客2636vVa9', '1435408716', '1');
INSERT INTO `ft_chat_content` VALUES ('37', '1', 'ddddddddddd', '2636vVa9', '游客2636vVa9', '1435408899', '1');
INSERT INTO `ft_chat_content` VALUES ('38', '1', 'lllllllllllllllllllllllllllllllll', '2636vVa9', '游客2636vVa9', '1435410248', '1');
INSERT INTO `ft_chat_content` VALUES ('39', '1', '汗滴滴', '21JKXQiB', '游客21JKXQiB', '1435497359', '1');
INSERT INTO `ft_chat_content` VALUES ('40', '1', 'sdgggg', '21JKXQiB', '游客21JKXQiB', '1435497485', '1');
INSERT INTO `ft_chat_content` VALUES ('41', '1', '哎  这可怎么半呢', '21JKXQiB', '游客21JKXQiB', '1435498362', '1');
INSERT INTO `ft_chat_content` VALUES ('42', '1', '4484848484848', '21JKXQiB', '游客21JKXQiB', '1435498451', '1');
INSERT INTO `ft_chat_content` VALUES ('43', '1', '555555555', '21JKXQiB', '游客21JKXQiB', '1435498531', '1');
INSERT INTO `ft_chat_content` VALUES ('44', '1', '22222', '21JKXQiB', '游客21JKXQiB', '1435499106', '1');
INSERT INTO `ft_chat_content` VALUES ('45', '1', '777777777777777777', '21JKXQiB', '游客21JKXQiB', '1435499675', '1');
INSERT INTO `ft_chat_content` VALUES ('46', '1', 'ffffffffffffffffff', '33pEMkhB', '游客33pEMkhB', '1435501297', '1');
INSERT INTO `ft_chat_content` VALUES ('47', '1', 'ffffffffffffffffff', '33pEMkhB', '游客33pEMkhB', '1435501821', '1');
INSERT INTO `ft_chat_content` VALUES ('48', '1', '111111', '33pEMkhB', '游客33pEMkhB', '1435501837', '1');
INSERT INTO `ft_chat_content` VALUES ('49', '1', '1111110', '33pEMkhB', '游客33pEMkhB', '1435502471', '1');
INSERT INTO `ft_chat_content` VALUES ('50', '1', '1111110858585888', '33pEMkhB', '游客33pEMkhB', '1435502485', '1');
INSERT INTO `ft_chat_content` VALUES ('51', '1', '1111110858585888', '33pEMkhB', '游客33pEMkhB', '1435502488', '1');
INSERT INTO `ft_chat_content` VALUES ('52', '1', '7787', '33pEMkhB', '游客33pEMkhB', '1435502538', '1');
INSERT INTO `ft_chat_content` VALUES ('53', '1', '7787', '33pEMkhB', '游客33pEMkhB', '1435502539', '1');
INSERT INTO `ft_chat_content` VALUES ('54', '1', '7787', '33pEMkhB', '游客33pEMkhB', '1435502542', '1');
INSERT INTO `ft_chat_content` VALUES ('55', '1', '7474747', '33pEMkhB', '游客33pEMkhB', '1435503091', '1');
INSERT INTO `ft_chat_content` VALUES ('56', '1', '4747474', '33pEMkhB', '游客33pEMkhB', '1435503209', '1');
INSERT INTO `ft_chat_content` VALUES ('57', '1', '4747474', '33pEMkhB', '游客33pEMkhB', '1435503211', '1');
INSERT INTO `ft_chat_content` VALUES ('58', '1', '4747474', '33pEMkhB', '游客33pEMkhB', '1435503218', '1');
INSERT INTO `ft_chat_content` VALUES ('59', '1', '4747474', '33pEMkhB', '游客33pEMkhB', '1435503221', '1');
INSERT INTO `ft_chat_content` VALUES ('60', '1', '123', '33pEMkhB', '游客33pEMkhB', '1435503284', '1');
INSERT INTO `ft_chat_content` VALUES ('61', '1', '123', '33pEMkhB', '游客33pEMkhB', '1435503286', '1');
INSERT INTO `ft_chat_content` VALUES ('62', '1', '123', '33pEMkhB', '游客33pEMkhB', '1435503289', '1');
INSERT INTO `ft_chat_content` VALUES ('63', '1', '456', '33pEMkhB', '游客33pEMkhB', '1435503387', '1');
INSERT INTO `ft_chat_content` VALUES ('64', '1', '456', '33pEMkhB', '游客33pEMkhB', '1435503410', '1');
INSERT INTO `ft_chat_content` VALUES ('65', '1', '4568', '33pEMkhB', '游客33pEMkhB', '1435503426', '1');
INSERT INTO `ft_chat_content` VALUES ('66', '1', '4747', '33pEMkhB', '游客33pEMkhB', '1435503452', '1');
INSERT INTO `ft_chat_content` VALUES ('67', '1', '47478', '33pEMkhB', '游客33pEMkhB', '1435503460', '1');
INSERT INTO `ft_chat_content` VALUES ('68', '1', '47478', '33pEMkhB', '游客33pEMkhB', '1435503464', '1');
INSERT INTO `ft_chat_content` VALUES ('69', '1', '47478', '33pEMkhB', '游客33pEMkhB', '1435503488', '1');
INSERT INTO `ft_chat_content` VALUES ('70', '1', '4521', '33pEMkhB', '游客33pEMkhB', '1435503505', '1');
INSERT INTO `ft_chat_content` VALUES ('71', '1', '78945', '33pEMkhB', '游客33pEMkhB', '1435503596', '1');
INSERT INTO `ft_chat_content` VALUES ('72', '1', '7478585', '33pEMkhB', '游客33pEMkhB', '1435503650', '1');
INSERT INTO `ft_chat_content` VALUES ('73', '1', '12132', '33pEMkhB', '游客33pEMkhB', '1435503776', '1');
INSERT INTO `ft_chat_content` VALUES ('74', '1', '7845', '33pEMkhB', '游客33pEMkhB', '1435503841', '1');
INSERT INTO `ft_chat_content` VALUES ('75', '1', '7845', '33pEMkhB', '游客33pEMkhB', '1435503916', '1');
INSERT INTO `ft_chat_content` VALUES ('76', '1', '4578', '33pEMkhB', '游客33pEMkhB', '1435503951', '1');
INSERT INTO `ft_chat_content` VALUES ('77', '1', '474', '33pEMkhB', '游客33pEMkhB', '1435504188', '1');
INSERT INTO `ft_chat_content` VALUES ('78', '1', 'dddddddddd', '33pEMkhB', '游客33pEMkhB', '1435504225', '1');
INSERT INTO `ft_chat_content` VALUES ('79', '1', 'uuuuuuuuuuuuuuuuu', '13', 'ketity1', '1435504323', '1');
INSERT INTO `ft_chat_content` VALUES ('80', '1', 'dddd', '13', 'ketity1', '1435504382', '1');
INSERT INTO `ft_chat_content` VALUES ('81', '1', '222', '13', 'ketity1', '1435504423', '1');
INSERT INTO `ft_chat_content` VALUES ('82', '1', '55', '13', 'ketity1', '1435504450', '1');
INSERT INTO `ft_chat_content` VALUES ('83', '1', '11', '13', 'ketity1', '1435504540', '1');
INSERT INTO `ft_chat_content` VALUES ('84', '1', '12233', '13', 'ketity1', '1435504643', '1');
INSERT INTO `ft_chat_content` VALUES ('85', '1', '567889', '13', 'ketity1', '1435504804', '1');
INSERT INTO `ft_chat_content` VALUES ('86', '1', '1234567', '13', 'ketity1', '1435505187', '1');
INSERT INTO `ft_chat_content` VALUES ('87', '1', '5667777', '13', 'ketity1', '1435505314', '1');
INSERT INTO `ft_chat_content` VALUES ('88', '1', '11111', '13', 'ketity1', '1435505376', '1');
INSERT INTO `ft_chat_content` VALUES ('89', '1', '888888888888888', '13', 'ketity1', '1435505481', '1');
INSERT INTO `ft_chat_content` VALUES ('90', '1', '888888888888888', '13', 'ketity1', '1435505494', '1');
INSERT INTO `ft_chat_content` VALUES ('91', '1', '888888888888888', '13', 'ketity1', '1435505508', '1');
INSERT INTO `ft_chat_content` VALUES ('92', '1', '5566', '13', 'ketity1', '1435505696', '1');
INSERT INTO `ft_chat_content` VALUES ('93', '1', '1234', '13', 'ketity1', '1435505768', '1');
INSERT INTO `ft_chat_content` VALUES ('94', '1', 'dddd', '13', 'ketity1', '1435505790', '1');
INSERT INTO `ft_chat_content` VALUES ('95', '1', 'yyy', '13', 'ketity1', '1435505974', '1');
INSERT INTO `ft_chat_content` VALUES ('96', '1', '56789', '13', 'ketity1', '1435506021', '1');
INSERT INTO `ft_chat_content` VALUES ('97', '1', '2345', '13', 'ketity1', '1435506120', '1');
INSERT INTO `ft_chat_content` VALUES ('98', '1', '90909090', '13', 'ketity1', '1435545622', '1');
INSERT INTO `ft_chat_content` VALUES ('99', '1', '4r4r4r4r4', '13', 'ketity1', '1435545674', '1');
INSERT INTO `ft_chat_content` VALUES ('100', '1', 'defeebvrferr', '13', 'ketity1', '1435545737', '1');
INSERT INTO `ft_chat_content` VALUES ('101', '1', '45678978', '13', 'ketity1', '1435545982', '1');
INSERT INTO `ft_chat_content` VALUES ('102', '1', '8iko97', '13', 'ketity1', '1435546048', '1');
INSERT INTO `ft_chat_content` VALUES ('103', '1', 'y6yjjmjuyy', '13', 'ketity1', '1435546086', '1');
INSERT INTO `ft_chat_content` VALUES ('104', '1', '69841', '13', 'ketity1', '1435546653', '1');
INSERT INTO `ft_chat_content` VALUES ('105', '1', '5djggygjnm', '13', 'ketity1', '1435546740', '1');
INSERT INTO `ft_chat_content` VALUES ('106', '1', '3566777', '13', 'ketity1', '1435547354', '1');
INSERT INTO `ft_chat_content` VALUES ('107', '1', 'yhhhjjjkk', '13', 'ketity1', '1435548075', '1');
INSERT INTO `ft_chat_content` VALUES ('108', '1', 'fbjuum,l', '13', 'ketity1', '1435548122', '1');
INSERT INTO `ft_chat_content` VALUES ('109', '1', 'yyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy', '13', 'ketity1', '1435548243', '1');
INSERT INTO `ft_chat_content` VALUES ('110', '1', 'feeererererere', '13', 'ketity1', '1435548274', '1');
INSERT INTO `ft_chat_content` VALUES ('111', '1', 'dfggg', '13', 'ketity1', '1435574200', '1');
INSERT INTO `ft_chat_content` VALUES ('112', '1', 'dfg ', '13', 'ketity1', '1435574207', '1');
INSERT INTO `ft_chat_content` VALUES ('113', '1', 'dfg', '24Pnm3tM', '游客24Pnm3tM', '1435574240', '1');
INSERT INTO `ft_chat_content` VALUES ('114', '1', 'hgf', '24Pnm3tM', '游客24Pnm3tM', '1435574243', '1');
INSERT INTO `ft_chat_content` VALUES ('115', '1', 'tytytytyt', '24Pnm3tM', '游客24Pnm3tM', '1435574248', '1');
INSERT INTO `ft_chat_content` VALUES ('116', '1', 'llll', '27sUa8Gk', '游客27sUa8Gk', '1435574467', '1');

-- ----------------------------
-- Table structure for `ft_chat_room`
-- ----------------------------
DROP TABLE IF EXISTS `ft_chat_room`;
CREATE TABLE `ft_chat_room` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT COMMENT '聊天室ID',
  `classifyid` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '聊天室所属分类ID',
  `chat_room_name` varchar(30) NOT NULL DEFAULT '' COMMENT '聊天室名称',
  `chat_room_description` varchar(80) NOT NULL DEFAULT '' COMMENT '聊天室描述',
  `chat_room_thumb` varchar(255) NOT NULL DEFAULT '' COMMENT '聊天室图标',
  `sort_order` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '排序',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '启禁用状态',
  `createtime` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `chat_room_type` varchar(50) NOT NULL DEFAULT '' COMMENT '聊天室功能类型，1：群聊 2：直播 3：喊单',
  `live_id` varchar(16) NOT NULL DEFAULT '' COMMENT '视频直播序列号',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ft_chat_room
-- ----------------------------
INSERT INTO `ft_chat_room` VALUES ('1', '2', '创享财经', '金融、财经聊天室', './Uploads/2015-06-10/5577b15c71a2a.jpg', '0', '1', '1433907548', '1,2', '28737700');
INSERT INTO `ft_chat_room` VALUES ('2', '1', '生活趣事', '闲谈各种生活趣事', './Uploads/2015-06-10/5577b182711a4.jpg', '0', '1', '1433907586', '1', '');

-- ----------------------------
-- Table structure for `ft_classify`
-- ----------------------------
DROP TABLE IF EXISTS `ft_classify`;
CREATE TABLE `ft_classify` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT COMMENT '分类ID',
  `classify_name` varchar(30) NOT NULL DEFAULT '' COMMENT '分类名称',
  `classify_description` varchar(80) NOT NULL DEFAULT '',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '分类启禁状态',
  `sort_order` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '排序',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ft_classify
-- ----------------------------
INSERT INTO `ft_classify` VALUES ('1', '星座', '按星座分类', '1', '0');
INSERT INTO `ft_classify` VALUES ('2', '地区', '按地区分类', '1', '0');
INSERT INTO `ft_classify` VALUES ('3', '性别', '按性别分类', '0', '0');

-- ----------------------------
-- Table structure for `ft_config`
-- ----------------------------
DROP TABLE IF EXISTS `ft_config`;
CREATE TABLE `ft_config` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '配置ID',
  `config_name` varchar(30) CHARACTER SET utf8 NOT NULL DEFAULT '' COMMENT '配置名称',
  `config_value` text CHARACTER SET utf8 NOT NULL COMMENT '配置值',
  `config_type` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '配置类型',
  `config_title` varchar(50) CHARACTER SET utf8 NOT NULL DEFAULT '' COMMENT '配置名称',
  `config_group` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '配置分组',
  `config_extra` varchar(255) CHARACTER SET utf8 NOT NULL DEFAULT '' COMMENT '配置值',
  `config_remark` varchar(100) CHARACTER SET utf8 NOT NULL DEFAULT '' COMMENT '配置说明',
  `create_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  `status` tinyint(4) unsigned NOT NULL DEFAULT '0' COMMENT '状态',
  `sort_order` smallint(3) unsigned NOT NULL DEFAULT '0' COMMENT '排序',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of ft_config
-- ----------------------------
INSERT INTO `ft_config` VALUES ('1', 'WEB_SITE_TITLE', '直播聊天室系统', '2', '网站标题', '1', '', '网站标题前台显示标题', '1433322925', '0', '1', '0');
INSERT INTO `ft_config` VALUES ('2', 'WEB_SITE_DESCRIPTION', '直播聊天室系统', '3', '网站描述', '1', '', '前台显示网站描述', '1433322930', '0', '1', '0');
INSERT INTO `ft_config` VALUES ('3', 'WEB_SITE_KEYWORD', '直播聊天室系统', '3', '网站关键字', '1', '', '网站搜索引擎关键字', '1433322935', '0', '1', '0');
INSERT INTO `ft_config` VALUES ('4', 'CONFIG_TYPE_LIST', '1:数字\r\n2:字符\r\n3:文本\r\n4:数组\r\n5:枚举', '4', '配置类型列表', '4', '', '主要用于数据解析和页面表单的生成', '1433322940', '0', '1', '0');
INSERT INTO `ft_config` VALUES ('5', 'CONFIG_GROUP_LIST', '1:基本\r\n2:内容\r\n3:用户\r\n4:系统', '4', '配置分组', '4', '', '配置分组', '1433322945', '0', '1', '0');
INSERT INTO `ft_config` VALUES ('6', 'USER_ALLOW_REGISTER', '0', '5', '是否允许用户注册', '3', '0:关闭\r\n1:允许', '是否开放用户注册', '1433322950', '0', '1', '0');
INSERT INTO `ft_config` VALUES ('7', 'WEB_SITE_ICP', '', '2', '网站备案号', '1', '', '设置在网站底部显示的备案号', '1433323140', '0', '1', '0');
INSERT INTO `ft_config` VALUES ('8', 'WEB_SITE_CLOSE', '1', '5', '关闭站点', '1', '0:关闭\r\n1:开启', '站点关闭后其他用户不能访问，管理员可以正常访问', '1433323213', '1433390691', '1', '0');
INSERT INTO `ft_config` VALUES ('9', 'UC_OPEN', '1', '5', '是否开启Ucenter', '3', '0:关闭\r\n1:开启', '是否开启Ucenter达到数据同步', '1433470641', '0', '1', '0');
INSERT INTO `ft_config` VALUES ('10', 'OFFLINE_TIME', '20', '2', '用户离线时间', '2', '', '设置用户离线时间（秒）', '1434423816', '1434423967', '1', '0');
INSERT INTO `ft_config` VALUES ('11', 'VISITOR_ACTIVE', '1', '5', '是否允许游客聊天', '2', '0:否\r\n1:是', '是否允许游客互动聊天', '1434448204', '1434552933', '1', '0');

-- ----------------------------
-- Table structure for `ft_link`
-- ----------------------------
DROP TABLE IF EXISTS `ft_link`;
CREATE TABLE `ft_link` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT COMMENT '友情链接ID',
  `link_name` varchar(150) NOT NULL DEFAULT '' COMMENT '链接名称',
  `linkurl` varchar(255) NOT NULL DEFAULT '' COMMENT '链接URL',
  `link_logo` varchar(255) NOT NULL DEFAULT '' COMMENT '链接logo',
  `sort_order` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '顺序',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '链接使用状态',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ft_link
-- ----------------------------
INSERT INTO `ft_link` VALUES ('1', 'Fasttop', 'http://fasttop.top', './Uploads/2015-06-06/5572c6f7d369a.png', '0', '1');
INSERT INTO `ft_link` VALUES ('2', '百度', 'http://www.baidu.com', './Uploads/2015-06-06/5572c9b4454b2.png', '0', '1');

-- ----------------------------
-- Table structure for `ft_login_times`
-- ----------------------------
DROP TABLE IF EXISTS `ft_login_times`;
CREATE TABLE `ft_login_times` (
  `username` varchar(20) NOT NULL DEFAULT '' COMMENT '用户名',
  `ip` varchar(15) NOT NULL DEFAULT '' COMMENT '登陆IP',
  `login_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '登陆时间',
  `is_admin` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否为管理员{0:否,1:是}',
  `times` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '重试次数',
  PRIMARY KEY (`username`,`is_admin`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='登陆次数限制表';

-- ----------------------------
-- Records of ft_login_times
-- ----------------------------

-- ----------------------------
-- Table structure for `ft_menu`
-- ----------------------------
DROP TABLE IF EXISTS `ft_menu`;
CREATE TABLE `ft_menu` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '菜单ID',
  `title` varchar(30) NOT NULL DEFAULT '' COMMENT '菜单名称',
  `parentid` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '上级菜单ID',
  `url` varchar(255) NOT NULL DEFAULT '' COMMENT '菜单链接URL',
  `module` varchar(50) NOT NULL DEFAULT '' COMMENT '所属模块',
  `sort_order` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '排序（同级有效）',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '菜单显示状态，0:隐藏 1:显示',
  `description` varchar(255) NOT NULL DEFAULT '' COMMENT '菜单说明',
  `data` text NOT NULL COMMENT '其它，待定',
  `is_main` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否是主菜单',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=83 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ft_menu
-- ----------------------------
INSERT INTO `ft_menu` VALUES ('1', '首页', '0', 'Index/index', 'Admin', '0', '1', '显示系统信息及团队信息', '', '1');
INSERT INTO `ft_menu` VALUES ('2', '内容', '0', 'Content/list', 'Admin', '0', '1', '内容管理', '', '1');
INSERT INTO `ft_menu` VALUES ('3', '用户', '0', 'User/list', 'Admin', '0', '1', '用户管理', '', '1');
INSERT INTO `ft_menu` VALUES ('4', '系统', '0', 'Config/system', 'Admin', '0', '1', '系统管理', '', '1');
INSERT INTO `ft_menu` VALUES ('5', '用户信息', '3', 'User/index', 'Admin', '0', '1', '用户列表', '', '1');
INSERT INTO `ft_menu` VALUES ('6', '用户角色管理', '3', 'Role/index', 'Admin', '0', '1', '用户角色管理', '', '1');
INSERT INTO `ft_menu` VALUES ('7', '网站设置', '4', 'Config/group', 'Admin', '0', '1', '网站相关设置管理', '', '1');
INSERT INTO `ft_menu` VALUES ('8', '配置管理', '4', 'Config/index', 'Admin', '0', '1', '配置管理', '', '1');
INSERT INTO `ft_menu` VALUES ('9', '菜单管理', '4', 'Menu/index', 'Admin', '0', '1', '菜单管理', '', '1');
INSERT INTO `ft_menu` VALUES ('10', '友情链接', '4', 'Link/index', 'Admin', '0', '1', '友情链接管理', '', '1');
INSERT INTO `ft_menu` VALUES ('11', '地区管理', '4', 'Area/index', 'Admin', '0', '1', '地区管理', '', '1');
INSERT INTO `ft_menu` VALUES ('12', '新增菜单', '9', 'Menu/add', 'Admin', '0', '1', '新增菜单页面显示', '', '0');
INSERT INTO `ft_menu` VALUES ('13', '编辑菜单', '9', 'Menu/edit', 'Admin', '0', '1', '编辑菜单页面', '', '0');
INSERT INTO `ft_menu` VALUES ('14', '菜单排序', '9', 'Menu/listorder', 'Admin', '0', '1', '对同级菜单进行排序', '', '0');
INSERT INTO `ft_menu` VALUES ('15', '删除菜单', '9', 'Menu/delete', 'Admin', '0', '1', '单个删除菜单', '', '0');
INSERT INTO `ft_menu` VALUES ('16', '批量删除菜单', '9', 'Menu/delBatch', 'Admin', '0', '1', '批量删除菜单', '', '0');
INSERT INTO `ft_menu` VALUES ('17', '新增链接', '10', 'Link/add', 'Admin', '0', '1', '友情链接', '', '0');
INSERT INTO `ft_menu` VALUES ('18', '个人资料管理', '1', 'Index/profile', 'Admin', '0', '1', '个人资料管理', '', '0');
INSERT INTO `ft_menu` VALUES ('19', '修改个人资料', '18', 'Index/editProfile', 'Admin', '0', '1', '修改个人资料，真实姓名、电子邮箱、手机号等', '', '0');
INSERT INTO `ft_menu` VALUES ('20', '修改密码', '18', 'Index/editPassword', 'Admin', '0', '1', '修改用户登录密码', '', '0');
INSERT INTO `ft_menu` VALUES ('21', '锁屏', '1', 'Index/lock', 'Admin', '0', '1', '锁屏', '', '0');
INSERT INTO `ft_menu` VALUES ('22', '屏幕解锁', '1', 'Index/unlock', 'Admin', '0', '1', '解锁', '', '0');
INSERT INTO `ft_menu` VALUES ('23', '新增用户', '5', 'User/add', 'Admin', '0', '1', '新增用户', '', '0');
INSERT INTO `ft_menu` VALUES ('24', '更改菜单状态', '9', 'Menu/status', 'Admin', '0', '1', '切换菜单状态，显示、隐藏。', '', '0');
INSERT INTO `ft_menu` VALUES ('25', '新增地区', '11', 'Area/add', 'Admin', '0', '1', '新增地区最多四级，国家（一级）、省（二级）、市（三级）、区（四级）', '', '0');
INSERT INTO `ft_menu` VALUES ('26', '插入菜单', '9', 'Menu/insert', 'Admin', '0', '1', '新增菜单的插入操作', '', '0');
INSERT INTO `ft_menu` VALUES ('27', '更新菜单', '9', 'Menu/update', 'Admin', '0', '1', '编辑菜单更新操作', '', '0');
INSERT INTO `ft_menu` VALUES ('28', '更新地区', '11', 'Area/editArea', 'Admin', '0', '1', '更新地区名称', '', '0');
INSERT INTO `ft_menu` VALUES ('29', '新增配置', '8', 'Config/add', 'Admin', '0', '1', '新增配置页面', '', '0');
INSERT INTO `ft_menu` VALUES ('30', '编辑配置', '8', 'Config/edit', 'Admin', '0', '1', '编辑配置页面', '', '0');
INSERT INTO `ft_menu` VALUES ('31', '插入配置', '8', 'Config/insert', 'Admin', '0', '1', '新增配置插入操作', '', '0');
INSERT INTO `ft_menu` VALUES ('32', '更新配置', '8', 'Config/update', 'Admin', '0', '1', '更新配置保存数据', '', '0');
INSERT INTO `ft_menu` VALUES ('33', '更新网站设置', '7', 'Config/save', 'Admin', '0', '1', '更新网站设置信息', '', '0');
INSERT INTO `ft_menu` VALUES ('34', '更改配置状态', '8', 'Config/status', 'Admin', '0', '1', '更改配置状态', '', '0');
INSERT INTO `ft_menu` VALUES ('35', '编辑链接', '10', 'Link/edit', 'Admin', '0', '1', '编辑链接页面', '', '0');
INSERT INTO `ft_menu` VALUES ('36', '插入友情链接', '10', 'Link/addLink', 'Admin', '0', '1', '添加友情链接保存操作', '', '0');
INSERT INTO `ft_menu` VALUES ('37', '更新友情链接', '10', 'Link/editLink', 'Admin', '0', '1', '更新友情链接信息', '', '0');
INSERT INTO `ft_menu` VALUES ('38', '添加角色', '6', 'Role/add', 'Admin', '0', '1', '添加用户角色页面', '', '0');
INSERT INTO `ft_menu` VALUES ('39', '编辑角色', '6', 'Role/editGroup', 'Admin', '0', '1', '编辑角色页面', '', '0');
INSERT INTO `ft_menu` VALUES ('40', '插入角色', '6', 'Role/createGroup', 'Admin', '0', '1', '新增用户角色保存操作', '', '0');
INSERT INTO `ft_menu` VALUES ('41', '更新角色', '6', 'Role/updateGroup', 'Admin', '0', '1', '更新用户角色信息', '', '0');
INSERT INTO `ft_menu` VALUES ('42', '更改角色状态', '6', 'Role/setStatus', 'Admin', '0', '1', '更改角色状态', '', '0');
INSERT INTO `ft_menu` VALUES ('43', '访问授权', '6', 'Role/access', 'Admin', '0', '1', '访问授权页面', '', '0');
INSERT INTO `ft_menu` VALUES ('44', '更改链接状态', '10', 'Link/status', 'Admin', '0', '1', '切换友情链接启禁用状态', '', '0');
INSERT INTO `ft_menu` VALUES ('45', '编辑用户', '5', 'User/editUser', 'Admin', '0', '1', '编辑用户页面', '', '0');
INSERT INTO `ft_menu` VALUES ('46', '插入用户', '5', 'User/addUser', 'Admin', '0', '1', '新增用户插入操作', '', '0');
INSERT INTO `ft_menu` VALUES ('47', '更新用户', '5', 'User/updateUser', 'Admin', '0', '1', '更改用户信息', '', '0');
INSERT INTO `ft_menu` VALUES ('48', '更改用户状态', '5', 'User/setStatus', 'Admin', '0', '1', '更改用户启禁用状态', '', '0');
INSERT INTO `ft_menu` VALUES ('49', '删除地区', '11', 'Area/delete', 'Admin', '0', '1', '删除地区', '', '0');
INSERT INTO `ft_menu` VALUES ('50', '地区排序', '11', 'Area/listorder', 'Admin', '0', '1', '同级地区排序', '', '0');
INSERT INTO `ft_menu` VALUES ('51', '删除友情链接', '10', 'Link/delete', 'Admin', '0', '1', '友情链接单个删除', '', '0');
INSERT INTO `ft_menu` VALUES ('52', '友情链接排序', '10', 'Link/listorder', 'Admin', '0', '1', '友情链接排序', '', '0');
INSERT INTO `ft_menu` VALUES ('53', '批量删除链接', '10', 'Link/delBatch', 'Admin', '0', '1', '友情链接批量删除', '', '0');
INSERT INTO `ft_menu` VALUES ('54', '配置排序', '8', 'Config/listorder', 'Admin', '0', '1', '配置项排序', '', '0');
INSERT INTO `ft_menu` VALUES ('55', '删除配置', '8', 'Config/delete', 'Admin', '0', '1', '单个删除配置项', '', '0');
INSERT INTO `ft_menu` VALUES ('56', '批量删除配置', '8', 'Config/delConfig', 'Admin', '0', '1', '批量删除配置项', '', '0');
INSERT INTO `ft_menu` VALUES ('57', '删除角色', '6', 'Role/deleteGroup', 'Admin', '0', '1', '单个删除用户角色组', '', '0');
INSERT INTO `ft_menu` VALUES ('58', '批量删除角色', '6', 'Role/delGroup', 'Admin', '0', '1', '批量删除用户角色组', '', '0');
INSERT INTO `ft_menu` VALUES ('59', '单个删除用户', '5', 'User/deleteUser', 'Admin', '0', '1', '单个删除用户', '', '0');
INSERT INTO `ft_menu` VALUES ('60', '批量删除用户', '5', 'User/delUser', 'Admin', '0', '1', '批量删除用户', '', '0');
INSERT INTO `ft_menu` VALUES ('61', '管理内容', '2', 'Content/index', 'Admin', '0', '1', '对文章、图片、产品等内容的管理', '', '1');
INSERT INTO `ft_menu` VALUES ('62', '导航管理', '4', 'Navigator/index', 'Admin', '0', '1', '前台导航的管理', '', '1');
INSERT INTO `ft_menu` VALUES ('63', '聊天室管理', '2', 'ChatRoom/index', 'Admin', '0', '1', '聊天室管理', '', '1');
INSERT INTO `ft_menu` VALUES ('64', '分类管理', '2', 'classify/index', 'Admin', '0', '1', '聊天室分类管理', '', '1');
INSERT INTO `ft_menu` VALUES ('65', '课程管理', '2', 'Course/index', 'Admin', '0', '1', '课程管理', '', '1');
INSERT INTO `ft_menu` VALUES ('66', '喊单管理', '2', 'Shout/index', 'Admin', '0', '1', '喊单管理', '', '1');
INSERT INTO `ft_menu` VALUES ('67', '添加聊天室', '63', 'ChatRoom/add', 'Admin', '0', '1', '添加聊天室页面', '', '0');
INSERT INTO `ft_menu` VALUES ('68', '编辑聊天室', '63', 'ChatRoom/editRoom', 'Admin', '0', '1', '编辑聊天室页面', '', '0');
INSERT INTO `ft_menu` VALUES ('69', '更改聊天室状态', '63', 'ChatRoom/status', 'Admin', '0', '1', '更改聊天室启禁用状态', '', '0');
INSERT INTO `ft_menu` VALUES ('70', '删除聊天室', '63', 'ChatRoom/delete', 'Admin', '0', '1', '单个删除聊天室', '', '0');
INSERT INTO `ft_menu` VALUES ('71', '批量删除聊天室', '63', 'ChatRoom/delBatch', 'Admin', '0', '1', '批量删除聊天室', '', '0');
INSERT INTO `ft_menu` VALUES ('72', '插入聊天室', '63', 'ChatRoom/addRoom', 'Admin', '0', '1', '新增聊天室处理操作', '', '0');
INSERT INTO `ft_menu` VALUES ('73', '更新聊天室', '63', 'ChatRoom/updateRoom', 'Admin', '0', '1', '更改聊天室信息处理操作', '', '0');
INSERT INTO `ft_menu` VALUES ('74', '添加分类', '64', 'Classify/add', 'Admin', '0', '1', '添加分类页面', '', '0');
INSERT INTO `ft_menu` VALUES ('75', '编辑分类', '64', 'Classify/edit', 'Admin', '0', '1', '编辑分类页面', '', '0');
INSERT INTO `ft_menu` VALUES ('76', '更改分类状态', '64', 'Classify/status', 'Admin', '0', '1', '更改分类启禁用状态', '', '0');
INSERT INTO `ft_menu` VALUES ('77', '插入分类', '64', 'Classify/insert', 'Admin', '0', '1', '新增分类处理操作', '', '0');
INSERT INTO `ft_menu` VALUES ('78', '更新分类', '64', 'Classify/update', 'Admin', '0', '1', '更新分类处理操作', '', '0');
INSERT INTO `ft_menu` VALUES ('79', '聊天室排序', '63', 'ChatRoom/listorder', 'Admin', '0', '1', '聊天室排序', '', '0');
INSERT INTO `ft_menu` VALUES ('80', '分类排序', '64', 'Classify/listorder', 'Admin', '0', '1', '分类排序', '', '0');
INSERT INTO `ft_menu` VALUES ('81', '删除分类', '64', 'Classify/delete', 'Admin', '0', '1', '单个删除分类', '', '0');
INSERT INTO `ft_menu` VALUES ('82', '批量删除分类', '64', 'Classify/delBatch', 'Admin', '0', '1', '批量删除分类', '', '0');

-- ----------------------------
-- Table structure for `ft_session`
-- ----------------------------
DROP TABLE IF EXISTS `ft_session`;
CREATE TABLE `ft_session` (
  `session_id` varchar(255) CHARACTER SET latin1 NOT NULL COMMENT 'session ID',
  `session_expire` int(11) NOT NULL COMMENT '有效期',
  `session_data` blob COMMENT 'session数据',
  UNIQUE KEY `session_id` (`session_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ft_session
-- ----------------------------
INSERT INTO `ft_session` VALUES ('49j83cbskgunjoio0k4fkkpt42', '1435575866', 0x75736572496E666F7C613A343A7B733A31343A22757365725F6F6E6C696E655F6964223B733A383A223236555A74346842223B733A31363A22757365725F6F6E6C696E655F6E616D65223B733A31343A22E6B8B8E5AEA23236555A74346842223B733A343A22726F6C65223B693A303B733A31303A2263726561746574696D65223B693A313433353537343432363B7D);
INSERT INTO `ft_session` VALUES ('hh7qft1f1j86eihes1de1e6hk6', '1435576220', 0x75736572496E666F7C613A343A7B733A31343A22757365725F6F6E6C696E655F6964223B693A313B733A31363A22757365725F6F6E6C696E655F6E616D65223B733A353A2261646D696E223B733A343A22726F6C65223B693A313B733A31303A2263726561746574696D65223B693A313433353537343735383B7D757365725F617574687C613A333A7B733A333A22756964223B693A313B733A353A22756E616D65223B733A353A2261646D696E223B733A363A22657870697265223B693A38363430303B7D757365725F617574685F7369676E7C733A34303A2261343137353131623766343465306335343331306635373163363933336332313064346135333965223B);
INSERT INTO `ft_session` VALUES ('u95n6rror3k4ffgf5e7dathdq0', '1435575663', 0x75736572496E666F7C613A343A7B733A31343A22757365725F6F6E6C696E655F6964223B693A31333B733A31363A22757365725F6F6E6C696E655F6E616D65223B733A373A226B657469747931223B733A343A22726F6C65223B693A313B733A31303A2263726561746574696D65223B693A313433353537343232333B7D757365725F617574687C613A333A7B733A333A22756964223B693A31333B733A353A22756E616D65223B733A373A226B657469747931223B733A363A22657870697265223B693A38363430303B7D757365725F617574685F7369676E7C733A34303A2261373662643839666264346132343935306238663539336237626536336465366433313365373937223B);

-- ----------------------------
-- Table structure for `ft_user`
-- ----------------------------
DROP TABLE IF EXISTS `ft_user`;
CREATE TABLE `ft_user` (
  `uid` mediumint(8) unsigned NOT NULL AUTO_INCREMENT COMMENT '用户ID',
  `ucid` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT 'Ucenter用户ID',
  `username` varchar(30) NOT NULL DEFAULT '' COMMENT '用户名',
  `password` varchar(32) NOT NULL DEFAULT '' COMMENT '用户密码',
  `realname` varchar(20) NOT NULL DEFAULT '' COMMENT '真实姓名',
  `email` varchar(40) NOT NULL DEFAULT '' COMMENT '用户email地址',
  `mobile` varchar(11) NOT NULL DEFAULT '' COMMENT '手机号',
  `qq` varchar(15) NOT NULL DEFAULT '' COMMENT 'QQ号',
  `credits` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT '用户积分',
  `reg_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '用户注册时间',
  `reg_ip` varchar(15) NOT NULL DEFAULT '' COMMENT '用户注册的IP',
  `update_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '用户信息更新时间',
  `last_login_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '最后登录的时间',
  `last_login_ip` varchar(15) NOT NULL DEFAULT '' COMMENT '最后登录的IP',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '用户状态',
  `encrypt` char(6) NOT NULL DEFAULT '' COMMENT '加密密钥',
  `avatar` varchar(255) NOT NULL DEFAULT '' COMMENT '用户头像',
  PRIMARY KEY (`uid`)
) ENGINE=MyISAM AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ft_user
-- ----------------------------
INSERT INTO `ft_user` VALUES ('1', '0', 'admin', '11dcb45d225228139432ac955fd58021', 'ketity', 'admin@admin.com', '15365874136', '', '0', '1433220107', '127.0.0.1', '1433220107', '1435574597', '127.0.0.1', '1', 'RC6dPK', '');
INSERT INTO `ft_user` VALUES ('2', '15', 'admin001', '90aa075bde52386a52e66cf77e33e6c2', 'admin001', 'admin001@qq.com', '15024785126', '', '0', '0', '', '0', '1433610756', '127.0.0.1', '1', 'EpaIhC', '');
INSERT INTO `ft_user` VALUES ('3', '16', 'admin002', '84ab89c5f034d49e7b57098b648292e2', 'admin002', 'admin002@qq.com', '15203684127', '', '0', '0', '', '0', '1433611969', '127.0.0.1', '1', 'xhdjsK', '');
INSERT INTO `ft_user` VALUES ('9', '20', 'test001', '298a764f8411355adbdac5863d197286', 'test001', 'test001@qq.com', '15036985214', '1023698745', '0', '1433818073', '127.0.0.1', '1433818073', '1433818094', '127.0.0.1', '1', '4UMhqz', '');
INSERT INTO `ft_user` VALUES ('13', '22', 'ketity1', '7c798463b9aef82c682fa19eacfd229b', 'ketity1', '970564173@qq.com', '15019211737', '970564173', '0', '1435496712', '127.0.0.1', '1435496712', '1435541252', '127.0.0.1', '1', 'WPCJS8', '');

-- ----------------------------
-- Table structure for `ft_user_online`
-- ----------------------------
DROP TABLE IF EXISTS `ft_user_online`;
CREATE TABLE `ft_user_online` (
  `user_online_id` varchar(16) NOT NULL DEFAULT '' COMMENT '用户ID',
  `user_online_name` varchar(30) NOT NULL DEFAULT '' COMMENT '用户名',
  `roomid` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT '聊天室ID',
  `role` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '用户角色标识，0：游客',
  `createtime` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '访问时间',
  PRIMARY KEY (`user_online_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ft_user_online
-- ----------------------------
INSERT INTO `ft_user_online` VALUES ('1', 'admin', '1', '1', '1435574758');
