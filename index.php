<?php
/**
 * index.php 入口文件
 */
if (version_compare ( PHP_VERSION, '5.3.0', '<' )) {
	die ( 'require PHP > 5.3.0 !' );
}

header ( 'Content-type:text/html;charset=utf-8' );

/* 系统定义 */
define ( 'APP_DEBUG', true ); // 调试模式
define ( 'APP_GZIP', false ); // 是否开启gzip压缩
define ( 'APP_LANG', true );
define ( 'BUILD_DIR_SECURE', false ); // 目录安全空白文件,默认关闭
/* 目录定义 */
define ( 'WEB_PATH', DIRECTORY_SEPARATOR );
define ( 'ROOT_PATH', dirname ( __FILE__ ) . DIRECTORY_SEPARATOR ); // 系统根目录绝对地址
define ( 'APP_PATH', ROOT_PATH . 'App' . DIRECTORY_SEPARATOR ); // 项目目录
define ( 'RUNTIME_PATH', ROOT_PATH . 'Runtime' . DIRECTORY_SEPARATOR ); // 运行时目录
define ( 'UPLOAD_PATH', ROOT_PATH . 'Uploads' . DIRECTORY_SEPARATOR ); // 上传目录
define ( 'PUBLIC_PATH', ROOT_PATH . 'Public' . DIRECTORY_SEPARATOR ); // 公共目录
define ( 'JS_PATH', PUBLIC_PATH . 'Js' . DIRECTORY_SEPARATOR ); // 公共Js目录
define ( 'CSS_PATH', PUBLIC_PATH . 'Css' . DIRECTORY_SEPARATOR ); // 公共Css目录
define ( 'IMG_PATH', PUBLIC_PATH . 'Img' . DIRECTORY_SEPARATOR ); // 公共Image目录
define ( 'VERSION', 'v1.0 Beta' ); //系统版本
define ( 'UPDATETIME', '2015040' );

// 主机协议
define ( 'SITE_PROTOCOL', isset ( $_SERVER ['SERVER_PORT'] ) && $_SERVER ['SERVER_PORT'] == '443' ? 'https://' : 'http://' ); //主机协议
// 当前访问的主机名
define ( 'SITE_URL', (isset ( $_SERVER ['HTTP_HOST'] ) ? $_SERVER ['HTTP_HOST'] : '') ); //当前访问的主机名
// 来源
define ( 'HTTP_REFERER', isset ( $_SERVER ['HTTP_REFERER'] ) ? $_SERVER ['HTTP_REFERER'] : '' ); //来源


/* 定义目录访问地址 */
define ( 'MAIN_URL', SITE_PROTOCOL . SITE_URL . '/' );
define ( 'PUBLIC_URL', MAIN_URL . 'Public/' );
define ( 'CSS_URL', PUBLIC_URL . 'Css/' );
define ( 'JS_URL', PUBLIC_URL . 'Js/' );
define ( 'IMG_URL', PUBLIC_URL . 'Img/' );

if (APP_GZIP && function_exists ( 'ob_gzhandler' )) {
	ob_start ( 'ob_gzhandler' );
} else {
	ob_start ();
}

/* 定义系统目录 */
require './ThinkPHP/ThinkPHP.php';